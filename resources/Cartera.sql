CREATE DATABASE Cartera
GO
USE [Cartera]
GO
/****** Object:  Table [dbo].[Clasificacion]    Script Date: 04/27/2014 10:27:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Clasificacion](
	[idClasificacion] [varchar](5) NOT NULL,
	[descripcion] [varchar](20) NOT NULL,
 CONSTRAINT [PK_Clasificacion] PRIMARY KEY CLUSTERED 
(
	[idClasificacion] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Identificador único de clasificación de clientes' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Clasificacion', @level2type=N'COLUMN',@level2name=N'idClasificacion'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Nombre o alias de la clasificación o categoría. Ej: Sector, Ruta, Tipo' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Clasificacion', @level2type=N'COLUMN',@level2name=N'descripcion'
GO
/****** Object:  Table [dbo].[Cliente]    Script Date: 04/27/2014 10:27:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Cliente](
	[idCliente] [varchar](10) NOT NULL,
	[nombre] [varchar](50) NOT NULL,
	[razonSocial] [varchar](20) NULL,
	[correo] [varchar](50) NULL,
 CONSTRAINT [PK_Clientes] PRIMARY KEY CLUSTERED 
(
	[idCliente] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Identificador único del cliente' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Cliente', @level2type=N'COLUMN',@level2name=N'idCliente'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Nombre del cliente' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Cliente', @level2type=N'COLUMN',@level2name=N'nombre'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Código de la razón social' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Cliente', @level2type=N'COLUMN',@level2name=N'razonSocial'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Dirección de correo electrónico' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Cliente', @level2type=N'COLUMN',@level2name=N'correo'
GO
/****** Object:  Table [dbo].[Rol]    Script Date: 04/27/2014 10:27:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Rol](
	[idRol] [int] NOT NULL,
	[descripcion] [varchar](30) NOT NULL,
 CONSTRAINT [PK_Rol] PRIMARY KEY CLUSTERED 
(
	[idRol] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Identificador del rol de usuario' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Rol', @level2type=N'COLUMN',@level2name=N'idRol'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Describe el rol del usuario (Administrador, gestor, etc)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Rol', @level2type=N'COLUMN',@level2name=N'descripcion'
GO
/****** Object:  Table [dbo].[Empresa]    Script Date: 04/27/2014 10:27:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Empresa](
	[idEmpresa] [varchar](5) NOT NULL,
	[nombre] [varchar](30) NOT NULL,
	[fechaInicioContrato] [date] NOT NULL,
 CONSTRAINT [PK_Empresa] PRIMARY KEY CLUSTERED 
(
	[idEmpresa] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Identificador de la empresa cliente del sistema de cartera y cobro' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Empresa', @level2type=N'COLUMN',@level2name=N'idEmpresa'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Nombre de la empresa' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Empresa', @level2type=N'COLUMN',@level2name=N'nombre'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Fecha en la que empieza a hacer uso del sistema' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Empresa', @level2type=N'COLUMN',@level2name=N'fechaInicioContrato'
GO
/****** Object:  Table [dbo].[TipoGestion]    Script Date: 04/27/2014 10:27:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TipoGestion](
	[idTipoGestion] [int] NOT NULL,
	[descripcion] [varchar](20) NOT NULL,
 CONSTRAINT [PK_TipoGestion] PRIMARY KEY CLUSTERED 
(
	[idTipoGestion] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Identificador único de cada tipo de gestión' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TipoGestion', @level2type=N'COLUMN',@level2name=N'idTipoGestion'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Nombre de la gestión' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TipoGestion', @level2type=N'COLUMN',@level2name=N'descripcion'
GO
/****** Object:  Table [dbo].[Tarea]    Script Date: 04/27/2014 10:27:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tarea](
	[idTarea] [varchar](6) NOT NULL,
	[idUsuario] [varchar](10) NOT NULL,
 CONSTRAINT [PK_Tarea] PRIMARY KEY CLUSTERED 
(
	[idTarea] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Identificador de tarea de usuario' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Tarea', @level2type=N'COLUMN',@level2name=N'idTarea'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Identificador único de usuario' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Tarea', @level2type=N'COLUMN',@level2name=N'idUsuario'
GO
/****** Object:  Table [dbo].[Vendedor]    Script Date: 04/27/2014 10:27:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Vendedor](
	[idVendedor] [varchar](10) NOT NULL,
	[nombre] [varchar](20) NOT NULL,
 CONSTRAINT [PK_Vendedor] PRIMARY KEY CLUSTERED 
(
	[idVendedor] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Identificador único de vendedor' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Vendedor', @level2type=N'COLUMN',@level2name=N'idVendedor'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Nombre del vendedor' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Vendedor', @level2type=N'COLUMN',@level2name=N'nombre'
GO
/****** Object:  Table [dbo].[Usuario]    Script Date: 04/27/2014 10:27:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Usuario](
	[idUsuario] [varchar](10) NOT NULL,
	[contrasena] [varchar](25) NOT NULL,
	[nombre] [varchar](25) NOT NULL,
	[rol] [int] NOT NULL,
 CONSTRAINT [PK_Usuario] PRIMARY KEY CLUSTERED 
(
	[idUsuario] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Identificador único de usuario' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Usuario', @level2type=N'COLUMN',@level2name=N'idUsuario'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Contraseña' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Usuario', @level2type=N'COLUMN',@level2name=N'contrasena'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Nombre de usuario' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Usuario', @level2type=N'COLUMN',@level2name=N'nombre'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Identificador del rol de usuario' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Usuario', @level2type=N'COLUMN',@level2name=N'rol'
GO
/****** Object:  Table [dbo].[Subclasificacion]    Script Date: 04/27/2014 10:27:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Subclasificacion](
	[idSubClasificacion] [varchar](5) NOT NULL,
	[descripcion] [varchar](15) NOT NULL,
	[clasificacion] [varchar](5) NOT NULL,
 CONSTRAINT [PK_Subclasificacion] PRIMARY KEY CLUSTERED 
(
	[idSubClasificacion] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Identificador único de la subcategoría' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Subclasificacion', @level2type=N'COLUMN',@level2name=N'idSubClasificacion'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Nombre o alias de la subcategoría. Ej: industrial, ferretero, occidente, oriente, mayorista, minorista' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Subclasificacion', @level2type=N'COLUMN',@level2name=N'descripcion'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Clasificación o categoría a la que pertenece' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Subclasificacion', @level2type=N'COLUMN',@level2name=N'clasificacion'
GO
/****** Object:  Table [dbo].[ClasificacionCliente]    Script Date: 04/27/2014 10:27:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ClasificacionCliente](
	[cliente] [varchar](10) NOT NULL,
	[subClasificacion] [varchar](5) NOT NULL,
 CONSTRAINT [PK_ClasificacionCliente] PRIMARY KEY CLUSTERED 
(
	[cliente] ASC,
	[subClasificacion] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Identificador único del cliente' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ClasificacionCliente', @level2type=N'COLUMN',@level2name=N'cliente'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Identificador único de la subcategoría' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ClasificacionCliente', @level2type=N'COLUMN',@level2name=N'subClasificacion'
GO
/****** Object:  Table [dbo].[Carga]    Script Date: 04/27/2014 10:27:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Carga](
	[idCarga] [varchar](10) NOT NULL,
	[descripcion] [varchar](50) NULL,
	[fecha] [datetime] NOT NULL,
	[usuario] [varchar](10) NOT NULL,
 CONSTRAINT [PK_Carga] PRIMARY KEY CLUSTERED 
(
	[idCarga] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Identificador único de carga masiva de documentos' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Carga', @level2type=N'COLUMN',@level2name=N'idCarga'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Nombre o descriptor de la carga (opcional)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Carga', @level2type=N'COLUMN',@level2name=N'descripcion'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Fecha en que se realizó la carga' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Carga', @level2type=N'COLUMN',@level2name=N'fecha'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Usuario que realizó la carga' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Carga', @level2type=N'COLUMN',@level2name=N'usuario'
GO
/****** Object:  Table [dbo].[Factura]    Script Date: 04/27/2014 10:27:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Factura](
	[idFactura] [varchar](10) NOT NULL,
	[cliente] [varchar](10) NOT NULL,
	[vendedor] [varchar](10) NULL,
	[fechaEmision] [date] NOT NULL,
	[fechaVencimiento] [date] NOT NULL,
	[monto] [float] NOT NULL,
	[carga] [varchar](10) NOT NULL,
	[estado] [varchar](10) NULL,
 CONSTRAINT [PK_Factura] PRIMARY KEY CLUSTERED 
(
	[idFactura] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Número de factura' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Factura', @level2type=N'COLUMN',@level2name=N'idFactura'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Cliente al que pertenece la factura' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Factura', @level2type=N'COLUMN',@level2name=N'cliente'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Vendedor que realizó la factura (si existe)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Factura', @level2type=N'COLUMN',@level2name=N'vendedor'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Fecha en que se emitió la factura' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Factura', @level2type=N'COLUMN',@level2name=N'fechaEmision'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Fecha de vencimiento de la factura' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Factura', @level2type=N'COLUMN',@level2name=N'fechaVencimiento'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Monto de la factura' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Factura', @level2type=N'COLUMN',@level2name=N'monto'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Carga que registró la factura' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Factura', @level2type=N'COLUMN',@level2name=N'carga'
GO
/****** Object:  Table [dbo].[Rechazo]    Script Date: 04/27/2014 10:27:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Rechazo](
	[idRechazo] [varchar](10) NOT NULL,
	[factura] [varchar](10) NULL,
	[cliente] [varchar](10) NULL,
	[carga] [varchar](10) NOT NULL,
	[vendedor] [varchar](10) NULL,
	[fechaEmision] [date] NULL,
	[fechaVencimiento] [date] NULL,
	[monto] [float] NULL,
	[observaciones] [text] NULL,
	[numFila] [int] NOT NULL,
 CONSTRAINT [PK_Rechazo] PRIMARY KEY CLUSTERED 
(
	[idRechazo] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Identificador único de registro rechazado por no cumplir con alguna validación requerida' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Rechazo', @level2type=N'COLUMN',@level2name=N'idRechazo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Número de factura a la que está ligado el registro (si existe)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Rechazo', @level2type=N'COLUMN',@level2name=N'factura'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Cliente al que está ligado el registro (si existe)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Rechazo', @level2type=N'COLUMN',@level2name=N'cliente'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Carga en la que se produjo el fallo de registro' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Rechazo', @level2type=N'COLUMN',@level2name=N'carga'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Vendedor que realizó la factura (si existe)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Rechazo', @level2type=N'COLUMN',@level2name=N'vendedor'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Fecha de emisión de la factura en el registro' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Rechazo', @level2type=N'COLUMN',@level2name=N'fechaEmision'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Fecha de vencimiento de la factura en el registro' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Rechazo', @level2type=N'COLUMN',@level2name=N'fechaVencimiento'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Monto del registro' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Rechazo', @level2type=N'COLUMN',@level2name=N'monto'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Un comentario sobre la causa del fallo de registro, indicando el/los campos que no fueron validados correctamente (opcional)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Rechazo', @level2type=N'COLUMN',@level2name=N'observaciones'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'El número de la fila de excel donde se encuentra el registro que produjo el fallo' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Rechazo', @level2type=N'COLUMN',@level2name=N'numFila'
GO
/****** Object:  Table [dbo].[Pago]    Script Date: 04/27/2014 10:27:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Pago](
	[idPago] [varchar](10) NOT NULL,
	[factura] [varchar](10) NOT NULL,
	[monto] [float] NOT NULL,
	[carga] [varchar](10) NOT NULL,
	[fechaEmision] [date] NOT NULL,
 CONSTRAINT [PK_Pago] PRIMARY KEY CLUSTERED 
(
	[idPago] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Identificador único de pago realizado' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Pago', @level2type=N'COLUMN',@level2name=N'idPago'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Factura a la que corresponde el pago' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Pago', @level2type=N'COLUMN',@level2name=N'factura'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Monto del pago registrado' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Pago', @level2type=N'COLUMN',@level2name=N'monto'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Carga que registró el pago' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Pago', @level2type=N'COLUMN',@level2name=N'carga'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Fecha en que se emitió el pago' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Pago', @level2type=N'COLUMN',@level2name=N'fechaEmision'
GO
/****** Object:  Table [dbo].[Nota]    Script Date: 04/27/2014 10:27:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Nota](
	[idNota] [varchar](15) NOT NULL,
	[factura] [varchar](10) NOT NULL,
	[fecha] [date] NOT NULL,
	[observaciones] [text] NULL,
	[tipoNota] [varchar](7) NOT NULL,
	[carga] [varchar](10) NOT NULL,
	[monto] [float] NOT NULL,
 CONSTRAINT [PK_Nota] PRIMARY KEY CLUSTERED 
(
	[idNota] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Identificador único de nota' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Nota', @level2type=N'COLUMN',@level2name=N'idNota'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Factura a la que está ligada la nota' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Nota', @level2type=N'COLUMN',@level2name=N'factura'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Fecha de emisión de la nota' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Nota', @level2type=N'COLUMN',@level2name=N'fecha'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Comentario sobre el concepto de la nota (opcional)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Nota', @level2type=N'COLUMN',@level2name=N'observaciones'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Especifica si la nota es de débito o crédito' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Nota', @level2type=N'COLUMN',@level2name=N'tipoNota'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Carga que registró la nota' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Nota', @level2type=N'COLUMN',@level2name=N'carga'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Saldo de la nota' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Nota', @level2type=N'COLUMN',@level2name=N'monto'
GO
/****** Object:  Table [dbo].[Gestion]    Script Date: 04/27/2014 10:27:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Gestion](
	[idGestion] [int] NOT NULL,
	[tipoGestion] [int] NOT NULL,
	[factura] [varchar](10) NOT NULL,
	[cliente] [varchar](10) NOT NULL,
	[fechaGestion] [date] NOT NULL,
	[observaciones] [text] NOT NULL,
	[fechaLimiteGestion] [date] NOT NULL,
	[usuario] [varchar](10) NOT NULL,
	[estado] [int] NOT NULL,
 CONSTRAINT [PK_Gestion] PRIMARY KEY CLUSTERED 
(
	[idGestion] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Identificador único de la tarea o gestión' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Gestion', @level2type=N'COLUMN',@level2name=N'idGestion'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tipo de gestión que se realiza' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Gestion', @level2type=N'COLUMN',@level2name=N'tipoGestion'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Factura a la que está ligada la gestión o tarea' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Gestion', @level2type=N'COLUMN',@level2name=N'factura'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Cliente al que está ligada la gestión' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Gestion', @level2type=N'COLUMN',@level2name=N'cliente'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Fecha en que se realiza una gestión' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Gestion', @level2type=N'COLUMN',@level2name=N'fechaGestion'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Comentario con las observaciones de la gestión realizada' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Gestion', @level2type=N'COLUMN',@level2name=N'observaciones'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Fecha límite de plazo para el cumplimiento de los acuerdos de la gestión o tarea' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Gestion', @level2type=N'COLUMN',@level2name=N'fechaLimiteGestion'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Identificador único de usuario' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Gestion', @level2type=N'COLUMN',@level2name=N'usuario'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Estado de la tarea (Pendiente, completa, vencida)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Gestion', @level2type=N'COLUMN',@level2name=N'estado'
GO
/****** Object:  View [dbo].[SumaPagos]    Script Date: 04/27/2014 10:27:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[SumaPagos]
AS
SELECT     TOP (100) PERCENT dbo.Factura.idFactura AS factura, dbo.Factura.cliente, dbo.Factura.monto, COALESCE (SUM(dbo.Pago.monto), 0) AS debito, 
                      COUNT(dbo.Pago.monto) AS pagosRealizados, dbo.Factura.fechaVencimiento, dbo.Factura.estado
FROM         dbo.Pago RIGHT OUTER JOIN
                      dbo.Factura ON dbo.Pago.factura = dbo.Factura.idFactura
GROUP BY dbo.Factura.idFactura, dbo.Factura.cliente, dbo.Factura.fechaVencimiento, dbo.Factura.monto, dbo.Factura.estado
ORDER BY dbo.Factura.estado
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[27] 4[17] 2[17] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1[50] 2[25] 3) )"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Pago"
            Begin Extent = 
               Top = 1
               Left = 381
               Bottom = 109
               Right = 570
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Factura"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 114
               Right = 243
            End
            DisplayFlags = 280
            TopColumn = 4
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 12
         Column = 5220
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'SumaPagos'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'SumaPagos'
GO
/****** Object:  View [dbo].[DetalleFactura]    Script Date: 04/27/2014 10:27:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[DetalleFactura]
AS
SELECT     dbo.Pago.idPago, dbo.Pago.monto, dbo.Pago.fechaEmision, dbo.Factura.idFactura
FROM         dbo.Factura INNER JOIN
                      dbo.Pago ON dbo.Factura.idFactura = dbo.Pago.factura
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[41] 4[21] 2[10] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Factura"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 114
               Right = 227
            End
            DisplayFlags = 280
            TopColumn = 1
         End
         Begin Table = "Pago"
            Begin Extent = 
               Top = 9
               Left = 321
               Bottom = 117
               Right = 510
            End
            DisplayFlags = 280
            TopColumn = 1
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'DetalleFactura'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'DetalleFactura'
GO
/****** Object:  View [dbo].[SaldoFactura]    Script Date: 04/27/2014 10:27:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[SaldoFactura]
AS
SELECT     TOP (100) PERCENT dbo.Factura.idFactura AS factura, dbo.Factura.monto AS montoInicial, SUM(dbo.SumaPagos.debito) AS debito, 
                      dbo.Factura.monto - dbo.SumaPagos.debito AS saldo, dbo.Factura.fechaVencimiento AS vence, dbo.Factura.cliente
FROM         dbo.Factura LEFT OUTER JOIN
                      dbo.SumaPagos ON dbo.Factura.idFactura = dbo.SumaPagos.factura
GROUP BY dbo.Factura.idFactura, dbo.Factura.monto, dbo.Factura.fechaVencimiento, dbo.Factura.monto - dbo.SumaPagos.debito, dbo.Factura.cliente
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[42] 4[18] 2[14] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Factura"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 114
               Right = 227
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "SumaPagos"
            Begin Extent = 
               Top = 6
               Left = 265
               Bottom = 84
               Right = 454
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 12
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'SaldoFactura'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'SaldoFactura'
GO
/****** Object:  View [dbo].[CuentaCliente]    Script Date: 04/27/2014 10:27:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[CuentaCliente]
AS
SELECT     dbo.Cliente.idCliente, dbo.Cliente.nombre, SUM(dbo.SaldoFactura.saldo) AS debe, SUM(dbo.SaldoFactura.debito) AS haber, 
                      SUM(dbo.SaldoFactura.saldo + dbo.SaldoFactura.debito) AS total
FROM         dbo.Cliente INNER JOIN
                      dbo.Factura ON dbo.Cliente.idCliente = dbo.Factura.cliente INNER JOIN
                      dbo.SaldoFactura ON dbo.Factura.idFactura = dbo.SaldoFactura.factura
WHERE     (dbo.SaldoFactura.saldo > 0)
GROUP BY dbo.Cliente.idCliente, dbo.Cliente.nombre
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[25] 4[23] 2[19] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Cliente"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 114
               Right = 227
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Factura"
            Begin Extent = 
               Top = 9
               Left = 547
               Bottom = 117
               Right = 736
            End
            DisplayFlags = 280
            TopColumn = 1
         End
         Begin Table = "SaldoFactura"
            Begin Extent = 
               Top = 118
               Left = 295
               Bottom = 226
               Right = 484
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 12
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1935
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'CuentaCliente'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'CuentaCliente'
GO
/****** Object:  View [dbo].[SaldoVencido]    Script Date: 04/27/2014 10:27:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[SaldoVencido]
AS
SELECT     dbo.Cliente.idCliente, dbo.Cliente.nombre, SUM(dbo.SaldoFactura.saldo) AS saldoVencido
FROM         dbo.Cliente LEFT OUTER JOIN
                      dbo.Factura ON dbo.Cliente.idCliente = dbo.Factura.cliente LEFT OUTER JOIN
                      dbo.SaldoFactura ON dbo.Factura.idFactura = dbo.SaldoFactura.factura
WHERE     (dbo.SaldoFactura.vence < GETDATE())
GROUP BY dbo.Cliente.idCliente, dbo.Cliente.nombre
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[34] 4[23] 2[15] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Cliente"
            Begin Extent = 
               Top = 0
               Left = 7
               Bottom = 108
               Right = 196
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Factura"
            Begin Extent = 
               Top = 0
               Left = 221
               Bottom = 108
               Right = 410
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "SaldoFactura"
            Begin Extent = 
               Top = 0
               Left = 442
               Bottom = 108
               Right = 631
            End
            DisplayFlags = 280
            TopColumn = 1
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 12
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'SaldoVencido'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'SaldoVencido'
GO
/****** Object:  View [dbo].[CuentaTotalCliente]    Script Date: 04/27/2014 10:27:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[CuentaTotalCliente]
AS
SELECT     TOP (100) PERCENT dbo.Cliente.idCliente, COALESCE (SUM(dbo.Factura.monto), 0) AS montoFacturado, COALESCE (SUM(dbo.Pago.monto), 0) AS montoCancelado, 
                      COALESCE (dbo.SaldoVencido.saldoVencido, 0) AS saldoVencido
FROM         dbo.Cliente LEFT OUTER JOIN
                      dbo.Factura ON dbo.Cliente.idCliente = dbo.Factura.cliente LEFT OUTER JOIN
                      dbo.Pago ON dbo.Factura.idFactura = dbo.Pago.factura LEFT OUTER JOIN
                      dbo.SaldoVencido ON dbo.Cliente.idCliente = dbo.SaldoVencido.idCliente
GROUP BY dbo.Cliente.idCliente, dbo.SaldoVencido.saldoVencido
ORDER BY montoFacturado DESC
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[41] 4[21] 2[13] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Cliente"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 114
               Right = 227
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Factura"
            Begin Extent = 
               Top = 2
               Left = 294
               Bottom = 110
               Right = 483
            End
            DisplayFlags = 280
            TopColumn = 3
         End
         Begin Table = "Pago"
            Begin Extent = 
               Top = 0
               Left = 562
               Bottom = 108
               Right = 751
            End
            DisplayFlags = 280
            TopColumn = 1
         End
         Begin Table = "SaldoVencido"
            Begin Extent = 
               Top = 117
               Left = 284
               Bottom = 210
               Right = 473
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 12
         Column = 1155
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
 ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'CuentaTotalCliente'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane2', @value=N'  End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'CuentaTotalCliente'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=2 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'CuentaTotalCliente'
GO
/****** Object:  Check [StringTipoNota]    Script Date: 04/27/2014 10:27:57 ******/
ALTER TABLE [dbo].[Nota]  WITH CHECK ADD  CONSTRAINT [StringTipoNota] CHECK  (([tipoNota]='debito' OR [tipoNota]='credito'))
GO
ALTER TABLE [dbo].[Nota] CHECK CONSTRAINT [StringTipoNota]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Establece los valores que puede recibir el campo tipoNota' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Nota', @level2type=N'CONSTRAINT',@level2name=N'StringTipoNota'
GO
/****** Object:  ForeignKey [FK_Carga_Usuario]    Script Date: 04/27/2014 10:27:57 ******/
ALTER TABLE [dbo].[Carga]  WITH CHECK ADD  CONSTRAINT [FK_Carga_Usuario] FOREIGN KEY([usuario])
REFERENCES [dbo].[Usuario] ([idUsuario])
GO
ALTER TABLE [dbo].[Carga] CHECK CONSTRAINT [FK_Carga_Usuario]
GO
/****** Object:  ForeignKey [FK_ClasificacionCliente_Cliente]    Script Date: 04/27/2014 10:27:57 ******/
ALTER TABLE [dbo].[ClasificacionCliente]  WITH CHECK ADD  CONSTRAINT [FK_ClasificacionCliente_Cliente] FOREIGN KEY([cliente])
REFERENCES [dbo].[Cliente] ([idCliente])
GO
ALTER TABLE [dbo].[ClasificacionCliente] CHECK CONSTRAINT [FK_ClasificacionCliente_Cliente]
GO
/****** Object:  ForeignKey [FK_ClasificacionCliente_Subclasificacion]    Script Date: 04/27/2014 10:27:57 ******/
ALTER TABLE [dbo].[ClasificacionCliente]  WITH CHECK ADD  CONSTRAINT [FK_ClasificacionCliente_Subclasificacion] FOREIGN KEY([subClasificacion])
REFERENCES [dbo].[Subclasificacion] ([idSubClasificacion])
GO
ALTER TABLE [dbo].[ClasificacionCliente] CHECK CONSTRAINT [FK_ClasificacionCliente_Subclasificacion]
GO
/****** Object:  ForeignKey [FK_Factura_Carga1]    Script Date: 04/27/2014 10:27:57 ******/
ALTER TABLE [dbo].[Factura]  WITH CHECK ADD  CONSTRAINT [FK_Factura_Carga1] FOREIGN KEY([carga])
REFERENCES [dbo].[Carga] ([idCarga])
GO
ALTER TABLE [dbo].[Factura] CHECK CONSTRAINT [FK_Factura_Carga1]
GO
/****** Object:  ForeignKey [FK_Factura_Cliente]    Script Date: 04/27/2014 10:27:57 ******/
ALTER TABLE [dbo].[Factura]  WITH CHECK ADD  CONSTRAINT [FK_Factura_Cliente] FOREIGN KEY([cliente])
REFERENCES [dbo].[Cliente] ([idCliente])
GO
ALTER TABLE [dbo].[Factura] CHECK CONSTRAINT [FK_Factura_Cliente]
GO
/****** Object:  ForeignKey [FK_Factura_Vendedor]    Script Date: 04/27/2014 10:27:57 ******/
ALTER TABLE [dbo].[Factura]  WITH CHECK ADD  CONSTRAINT [FK_Factura_Vendedor] FOREIGN KEY([vendedor])
REFERENCES [dbo].[Vendedor] ([idVendedor])
GO
ALTER TABLE [dbo].[Factura] CHECK CONSTRAINT [FK_Factura_Vendedor]
GO
/****** Object:  ForeignKey [FK_Gestion_Factura]    Script Date: 04/27/2014 10:27:57 ******/
ALTER TABLE [dbo].[Gestion]  WITH CHECK ADD  CONSTRAINT [FK_Gestion_Factura] FOREIGN KEY([factura])
REFERENCES [dbo].[Factura] ([idFactura])
GO
ALTER TABLE [dbo].[Gestion] CHECK CONSTRAINT [FK_Gestion_Factura]
GO
/****** Object:  ForeignKey [FK_Gestion_TipoGestion]    Script Date: 04/27/2014 10:27:57 ******/
ALTER TABLE [dbo].[Gestion]  WITH CHECK ADD  CONSTRAINT [FK_Gestion_TipoGestion] FOREIGN KEY([tipoGestion])
REFERENCES [dbo].[TipoGestion] ([idTipoGestion])
GO
ALTER TABLE [dbo].[Gestion] CHECK CONSTRAINT [FK_Gestion_TipoGestion]
GO
/****** Object:  ForeignKey [FK_Gestion_Usuario]    Script Date: 04/27/2014 10:27:57 ******/
ALTER TABLE [dbo].[Gestion]  WITH CHECK ADD  CONSTRAINT [FK_Gestion_Usuario] FOREIGN KEY([usuario])
REFERENCES [dbo].[Usuario] ([idUsuario])
GO
ALTER TABLE [dbo].[Gestion] CHECK CONSTRAINT [FK_Gestion_Usuario]
GO
/****** Object:  ForeignKey [FK_Nota_Carga]    Script Date: 04/27/2014 10:27:57 ******/
ALTER TABLE [dbo].[Nota]  WITH CHECK ADD  CONSTRAINT [FK_Nota_Carga] FOREIGN KEY([carga])
REFERENCES [dbo].[Carga] ([idCarga])
GO
ALTER TABLE [dbo].[Nota] CHECK CONSTRAINT [FK_Nota_Carga]
GO
/****** Object:  ForeignKey [FK_Nota_Factura]    Script Date: 04/27/2014 10:27:57 ******/
ALTER TABLE [dbo].[Nota]  WITH CHECK ADD  CONSTRAINT [FK_Nota_Factura] FOREIGN KEY([factura])
REFERENCES [dbo].[Factura] ([idFactura])
GO
ALTER TABLE [dbo].[Nota] CHECK CONSTRAINT [FK_Nota_Factura]
GO
/****** Object:  ForeignKey [FK_Pago_Carga]    Script Date: 04/27/2014 10:27:57 ******/
ALTER TABLE [dbo].[Pago]  WITH CHECK ADD  CONSTRAINT [FK_Pago_Carga] FOREIGN KEY([carga])
REFERENCES [dbo].[Carga] ([idCarga])
GO
ALTER TABLE [dbo].[Pago] CHECK CONSTRAINT [FK_Pago_Carga]
GO
/****** Object:  ForeignKey [FK_Pago_Factura]    Script Date: 04/27/2014 10:27:57 ******/
ALTER TABLE [dbo].[Pago]  WITH CHECK ADD  CONSTRAINT [FK_Pago_Factura] FOREIGN KEY([factura])
REFERENCES [dbo].[Factura] ([idFactura])
GO
ALTER TABLE [dbo].[Pago] CHECK CONSTRAINT [FK_Pago_Factura]
GO
/****** Object:  ForeignKey [FK_Rechazo_Carga]    Script Date: 04/27/2014 10:27:57 ******/
ALTER TABLE [dbo].[Rechazo]  WITH CHECK ADD  CONSTRAINT [FK_Rechazo_Carga] FOREIGN KEY([carga])
REFERENCES [dbo].[Carga] ([idCarga])
GO
ALTER TABLE [dbo].[Rechazo] CHECK CONSTRAINT [FK_Rechazo_Carga]
GO
/****** Object:  ForeignKey [FK_Subclasificacion_Clasificacion]    Script Date: 04/27/2014 10:27:57 ******/
ALTER TABLE [dbo].[Subclasificacion]  WITH CHECK ADD  CONSTRAINT [FK_Subclasificacion_Clasificacion] FOREIGN KEY([clasificacion])
REFERENCES [dbo].[Clasificacion] ([idClasificacion])
GO
ALTER TABLE [dbo].[Subclasificacion] CHECK CONSTRAINT [FK_Subclasificacion_Clasificacion]
GO
/****** Object:  ForeignKey [FK_Usuario_Rol]    Script Date: 04/27/2014 10:27:57 ******/
ALTER TABLE [dbo].[Usuario]  WITH CHECK ADD  CONSTRAINT [FK_Usuario_Rol] FOREIGN KEY([rol])
REFERENCES [dbo].[Rol] ([idRol])
GO
ALTER TABLE [dbo].[Usuario] CHECK CONSTRAINT [FK_Usuario_Rol]
GO
