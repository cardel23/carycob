USE [Cartera]
GO

/****** Object:  View [dbo].[CuentaCliente]    Script Date: 04/22/2014 08:51:50 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER VIEW [dbo].[CuentaCliente]
AS
SELECT     dbo.Cliente.idCliente, dbo.Cliente.nombre, SUM(dbo.SaldoFactura.saldo) AS debe, SUM(dbo.SaldoFactura.debito) AS haber, 
                      SUM(dbo.SaldoFactura.saldo + dbo.SaldoFactura.debito) AS total
FROM         dbo.Cliente INNER JOIN
                      dbo.Factura ON dbo.Cliente.idCliente = dbo.Factura.cliente INNER JOIN
                      dbo.SaldoFactura ON dbo.Factura.idFactura = dbo.SaldoFactura.Factura
WHERE     (dbo.SaldoFactura.vence < GETDATE()) AND (dbo.SaldoFactura.vence > GETDATE() - 30) AND (dbo.SaldoFactura.saldo > 0)
GROUP BY dbo.Cliente.idCliente, dbo.Cliente.nombre

GO


USE [Cartera]
GO

/****** Object:  View [dbo].[CuentaTotalCliente]    Script Date: 04/22/2014 08:52:14 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER VIEW [dbo].[CuentaTotalCliente]
AS
SELECT     TOP (100) PERCENT dbo.Cliente.idCliente, COALESCE (SUM(dbo.Factura.monto), 0) AS montoFacturado, COALESCE (SUM(dbo.Pago.monto), 0) AS montoCancelado, 
                      COALESCE (dbo.SaldoVencido.saldoVencido, 0) AS saldoVencido
FROM         dbo.Cliente LEFT OUTER JOIN
                      dbo.Factura ON dbo.Cliente.idCliente = dbo.Factura.cliente LEFT OUTER JOIN
                      dbo.Pago ON dbo.Factura.idFactura = dbo.Pago.factura LEFT OUTER JOIN
                      dbo.SaldoVencido ON dbo.Cliente.idCliente = dbo.SaldoVencido.idCliente
GROUP BY dbo.Cliente.idCliente, dbo.SaldoVencido.saldoVencido
ORDER BY montoFacturado DESC

GO


USE [Cartera]
GO

/****** Object:  View [dbo].[DetalleFactura]    Script Date: 04/22/2014 08:52:20 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER VIEW [dbo].[DetalleFactura]
AS
SELECT     dbo.Pago.idPago, dbo.Pago.monto, dbo.Pago.fechaEmision, dbo.Factura.idFactura
FROM         dbo.Factura INNER JOIN
                      dbo.Pago ON dbo.Factura.idFactura = dbo.Pago.factura

GO


USE [Cartera]
GO

/****** Object:  View [dbo].[SaldoFactura]    Script Date: 04/22/2014 08:52:34 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER VIEW [dbo].[SaldoFactura]
AS
SELECT     TOP (100) PERCENT dbo.Factura.idFactura AS factura, dbo.Factura.monto AS montoInicial, SUM(dbo.SumaPagos.debito) AS debito, 
                      dbo.Factura.monto - dbo.SumaPagos.debito AS saldo, dbo.Factura.fechaVencimiento AS vence
FROM         dbo.Factura INNER JOIN
                      dbo.SumaPagos ON dbo.Factura.idFactura = dbo.SumaPagos.factura
GROUP BY dbo.Factura.idFactura, dbo.Factura.monto, dbo.Factura.fechaVencimiento, dbo.Factura.monto - dbo.SumaPagos.debito

GO


USE [Cartera]
GO

/****** Object:  View [dbo].[SaldoVencido]    Script Date: 04/22/2014 08:52:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER VIEW [dbo].[SaldoVencido]
AS
SELECT     dbo.Cliente.idCliente, dbo.Cliente.nombre, SUM(dbo.SaldoFactura.saldo) AS saldoVencido
FROM         dbo.Cliente INNER JOIN
                      dbo.Factura ON dbo.Cliente.idCliente = dbo.Factura.cliente INNER JOIN
                      dbo.SaldoFactura ON dbo.Factura.idFactura = dbo.SaldoFactura.factura
WHERE     (dbo.SaldoFactura.vence < GETDATE())
GROUP BY dbo.Cliente.idCliente, dbo.Cliente.nombre

GO


USE [Cartera]
GO

/****** Object:  View [dbo].[SumaPagos]    Script Date: 04/22/2014 08:52:52 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER VIEW [dbo].[SumaPagos]
AS
SELECT     TOP (100) PERCENT dbo.Factura.idFactura AS factura, dbo.Factura.cliente, dbo.Factura.monto, COALESCE (SUM(dbo.Pago.monto), 0) AS debito, 
                      COUNT(dbo.Pago.monto) AS pagosRealizados, dbo.Factura.fechaVencimiento, dbo.Factura.estado
FROM         dbo.Pago RIGHT OUTER JOIN
                      dbo.Factura ON dbo.Pago.factura = dbo.Factura.idFactura
GROUP BY dbo.Factura.idFactura, dbo.Factura.cliente, dbo.Factura.fechaVencimiento, dbo.Factura.monto, dbo.Factura.estado
ORDER BY dbo.Factura.estado

GO

