/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cubisoft.carycob.ejb;

import cubisoft.carycob.entidades.Usuario;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author Estacion 2
 */
@Stateless
public class UsuarioFacade extends AbstractFacade<Usuario> {
    @PersistenceContext(unitName = "carycobPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public UsuarioFacade() {
        super(Usuario.class);
    }
    
    /**
     * Crea usuario por defecto
     * @return usuario
     */
    public Usuario defaultUser(){
        Usuario usuario = new Usuario("carlos");
        return usuario;
    }
    
    public Usuario buscarUsuario(String idUsuario, String pass){
        try {
            Query q = em.createQuery("SELECT u FROM Usuario u WHERE u.idUsuario = :idUsuario and u.contrasena = :pass")
                .setParameter("idUsuario", idUsuario)
                .setParameter("pass", pass);
            return (Usuario)q.getSingleResult();
        } catch (Exception e) {
            return null;
        }

    }
    
}
