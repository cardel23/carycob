/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cubisoft.carycob.ejb;

import cubisoft.carycob.entidades.Gestion;
import cubisoft.carycob.entidades.Usuario;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author TOSHIBA
 */
@Stateless
public class GestionFacade extends AbstractFacade<Gestion> {
    @PersistenceContext(unitName = "carycobPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public GestionFacade() {
        super(Gestion.class);
    }
    
    public List<Gestion> gestionesUsuario(Usuario usuario){
        Query q = em.createNamedQuery("Gestion.findByUsuario")
                .setParameter("usuario", usuario);
        return q.getResultList();
    }
}
