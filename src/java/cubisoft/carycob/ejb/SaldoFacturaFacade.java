/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cubisoft.carycob.ejb;

import cubisoft.carycob.entidades.Factura;
import cubisoft.carycob.vistas.SaldoFactura;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author Estacion 2
 */
@Stateless
public class SaldoFacturaFacade extends AbstractFacade<SaldoFactura> {
    @PersistenceContext(unitName = "carycobPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public SaldoFacturaFacade() {
        super(SaldoFactura.class);
    }
    
    public Double saldoFacturado(){
        try {
            Query q = em.createNamedQuery("SaldoFactura.totalFacturado");
            return (Double) q.getSingleResult();
        } catch (Exception e) {
            return 0.0;
        }
        
    }
    
    public double saldo(Factura f){
        try {
            Query q = em.createNamedQuery("SaldoFactura.findSaldo")
                    .setParameter("factura", f.getIdFactura());
            return (double) q.getSingleResult();
        } catch (Exception e) {
            return 0.0;
        }
    }
    
    public double importe(Factura f){
        try {
            Query q = em.createNamedQuery("SaldoFactura.findImporte")
                    .setParameter("factura", f.getIdFactura());
            return (double) q.getSingleResult();
        } catch (Exception e) {
            return 0.0;
        }
    }
    
    public List<SaldoFactura> listaFacturasCliente(String idCliente, Date fechaInicio, Date fechaFin){
        try {
            StringBuilder sb = new StringBuilder("SELECT s FROM SaldoFactura s WHERE s.cliente = :cliente");
            if(fechaInicio != null){
                sb.append(" AND s.fechaEmision >= :fechaEmision");
            }
            if(fechaFin != null){
                if(fechaInicio != null)
                    sb.append(" OR s.vence <= :vence");
                sb.append(" AND s.vence <= :vence");
            }
            Query q = em.createQuery(sb.toString())
                    .setParameter("cliente", idCliente);
            if(fechaInicio != null){
                q.setParameter("fechaEmision", fechaInicio);
            }
            if(fechaFin != null){
                q.setParameter("vence", fechaFin);
            }
            List<SaldoFactura> lista = new ArrayList<>();
            for(Object s : q.getResultList()){
                SaldoFactura a = (SaldoFactura)s;
                lista.add(a);
            }
            return lista;
        } catch (Exception e) {
            return null;
        }
    }
    
    public List<SaldoFactura> listaFacturasVendedor(String vendedor){
        try {
            List<SaldoFactura> lista = em.createQuery("select f from SaldoFactura f where f.vendedor = :vendedor")
                    .setParameter("vendedor", vendedor)
                    .getResultList();
            return lista;
        } catch (Exception e) {
            return null;
        }
        
    }
    
     
}
