/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cubisoft.carycob.ejb;


import cubisoft.carycob.entidades.Cliente;
import cubisoft.carycob.vistas.CuentaCliente;
import cubisoft.carycob.excepciones.NoClienteException;
import cubisoft.carycob.vistas.GestionesCliente;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author Estacion 2
 */
@Stateless
public class ClienteFacade extends AbstractFacade<Cliente> {
    @PersistenceContext(unitName = "carycobPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public ClienteFacade() {
        super(Cliente.class);
    }
    private Cliente cliente;
    /**
     * Busca cliente por nombre
     * @param nombre es el nombre del cliente que se busca
     * @return objeto cliente
     * @throws NoClienteException
     * @since 14/03/2014
     */
    public Cliente findByNombre(String nombre) throws NoClienteException{
        try{
            cliente = (Cliente) em.createNamedQuery("Cliente.findByNombre")
                    .setParameter("nombre", nombre).getSingleResult();
            return cliente;
        }
        catch(NoResultException e){
            throw new NoClienteException("El cliente "+nombre+" no existe");
        }
    }
    
    public CuentaCliente obtenerCuenta(String cliente){
        try {
            return (CuentaCliente) em.createNamedQuery("CuentaCliente.findByIdCliente")
                .setParameter("idCliente", cliente)
                .getSingleResult();
        } catch (Exception e) {
            return null;
        }
        
    }
    
    public List<GestionesCliente> listaGestiones(String idCliente){
        try {
            return (List<GestionesCliente>) em.createQuery("SELECT g FROM GestionesCliente g WHERE g.idCliente = :idCliente")
                    .setParameter("idCliente", idCliente)
                    .getResultList();
        } catch (Exception e) {
            return null;
        }
        
    }
    
}
