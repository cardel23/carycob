/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cubisoft.carycob.ejb;

import cubisoft.carycob.entidades.Clasificacion;
import cubisoft.carycob.entidades.Subclasificacion;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author Estacion 2
 */
@Stateless
public class SubclasificacionFacade extends AbstractFacade<Subclasificacion> {
    @PersistenceContext(unitName = "carycobPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public SubclasificacionFacade() {
        super(Subclasificacion.class);
    }
    
    /**
     * 
     * @param c
     * @return
     * @autor Carlos D
     * @fecha 04/04/2014
     */
    public List<Subclasificacion> subclasificaciones(Clasificacion c){
        List<Subclasificacion> lista = new ArrayList<>();
        lista.addAll(em.createQuery("SELECT s FROM Subclasificacion s WHERE s.clasificacion = :c")
                .setParameter("c", c)
                .getResultList());
        return lista;
    }
    
    public Subclasificacion findByDescripcion(String descripcion){
        try {
            Query q = em.createNamedQuery("Subclasificacion.findByDescripcion")
                    .setParameter("descripcion", descripcion);
            return (Subclasificacion)q.getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
        
    }
    
}
