/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cubisoft.carycob.ejb;

import cubisoft.carycob.entidades.Clasificacion;
import cubisoft.carycob.vistas.VentasClasificacion;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author Carlos Delgadillo
 */
@Stateless
public class VentasClasificacionFacade extends AbstractFacade<VentasClasificacion> {
    @PersistenceContext(unitName = "carycobPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public VentasClasificacionFacade() {
        super(VentasClasificacion.class);
    }
    
    public List<VentasClasificacion> listaPorClasificacion(String clasificacion){
        Query q = em.createQuery("SELECT v FROM VentasClasificacion v WHERE v.clasificacion = :clasificacion")
                .setParameter("clasificacion", clasificacion);
//        if(q.getResultList().size()>0)
            return q.getResultList();
//        else return null;
    }
    
}
