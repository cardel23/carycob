/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cubisoft.carycob.ejb;

import cubisoft.carycob.entidades.Vendedor;
import cubisoft.carycob.vistas.VentasCliente;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author Estacion 2
 */
@Stateless
public class VendedorFacade extends AbstractFacade<Vendedor> {
    @PersistenceContext(unitName = "carycobPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public VendedorFacade() {
        super(Vendedor.class);
    }
    
    public Vendedor findByNombre(String nombre){
        try {
            Query q = em.createNamedQuery("Vendedor.findByNombre")
                    .setParameter("nombre", nombre);
            return (Vendedor)q.getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
        
    }
    public List<VentasCliente> vendedores(){
        return em.createQuery("SELECT v FROM VentasCliente v WHERE v.vendedor <> '[Sin comisión]'").getResultList();
    }
}
