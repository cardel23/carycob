/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cubisoft.carycob.ejb;

import cubisoft.carycob.vistas.Vencimiento;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author Carlos Delgadillo
 * @since 01/05/2014
 */
@Stateless
public class VencimientoFacade extends AbstractFacade<Vencimiento> {
    @PersistenceContext(unitName = "carycobPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public VencimientoFacade() {
        super(Vencimiento.class);
    }
    
    public List<Vencimiento> getGraficoVencimiento(){
        Query q = em.createNamedQuery("Vencimiento.findAll");
        return q.getResultList();
    }
    
}
