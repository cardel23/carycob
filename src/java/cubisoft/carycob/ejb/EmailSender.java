/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cubisoft.carycob.ejb;

import cubisoft.carycob.emailUtils.Email;
import cubisoft.carycob.emailUtils.EmailSenderConfig;
import java.io.ByteArrayOutputStream;
import java.util.Date;
import java.util.Properties;
import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.ejb.Stateless;
import javax.mail.*;
import javax.mail.internet.*;
import javax.mail.util.ByteArrayDataSource;

/**
 *
 * @author Jose
 */
@Stateless
public class EmailSender {
    EmailSenderConfig emailSenderConfig;
    
    public boolean sendEmail(Email email, byte[] file, boolean adjunto, String nombreArchivo) {
        emailSenderConfig=EmailSenderConfig.getInstance();
        Properties props = new Properties();
        props.put("mail.smtp.host", emailSenderConfig.getHost());
        props.put("mail.smtp.socketFactory.port", emailSenderConfig.getPort());
	props.put("mail.smtp.socketFactory.class","javax.net.ssl.SSLSocketFactory");        
        Authenticator authenticator = null;
        if (emailSenderConfig.isAuth()) {
            props.put("mail.smtp.auth", true);
            authenticator = new Authenticator() {
                @Override
                public PasswordAuthentication getPasswordAuthentication() {
                    return new PasswordAuthentication(emailSenderConfig.getUsername(), emailSenderConfig.getPassword());
                }
            };
        }
        Session session = Session.getInstance(props, authenticator);
        MimeMessage message = new MimeMessage(session);
        Multipart multipart = new MimeMultipart();
        
        try {
            MimeBodyPart body = new MimeBodyPart();
            body.setContent(email.getBody(),"text/html; charset=utf-8" );
            body.setDisposition(Part.INLINE);
            
            multipart.addBodyPart(body);
            if(adjunto){
                DataSource ds = new ByteArrayDataSource(file, "application/pdf");
                MimeBodyPart pdf = new MimeBodyPart();
                pdf.setFileName(nombreArchivo.concat(".pdf"));
                pdf.setDataHandler(new DataHandler(ds));
                multipart.addBodyPart(pdf);
            }
            email.setFrom(emailSenderConfig.getUsername());
            message.setFrom(new InternetAddress(email.getFrom()));
            InternetAddress[] address = {new InternetAddress(email.getTo())};
            message.setRecipients(Message.RecipientType.TO, address);
            message.addRecipients(Message.RecipientType.BCC, email.getFrom());
            message.setSubject(email.getSubject());
            message.setSentDate(new Date());
            
            message.setContent(multipart, "text/html; charset=utf-8");
            
            Transport.send(message);
        } catch (MessagingException ex) {
            ex.printStackTrace();
            return false;
        }
        return true;
    }


}
