/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cubisoft.carycob.ejb;

import cubisoft.carycob.entidades.Cliente;
import cubisoft.carycob.vistas.CuentaCliente;
import java.util.List;
import javax.ejb.EJBException;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 * 
 * @author Estacion 2
 */
@Stateless
public class CuentaClienteFacade extends AbstractFacade<CuentaCliente> {
    @PersistenceContext(unitName = "carycobPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public CuentaClienteFacade() {
        super(CuentaCliente.class);
    }
    
    public CuentaCliente obtenerCuenta(String cliente){
        try {
            Query q =  em.createNamedQuery("CuentaCliente.findByIdCliente")
                .setParameter("idCliente", cliente);
            return (CuentaCliente) q.getSingleResult();
        } catch (Exception e) {
            return null;
        }
    }
    
    public List<CuentaCliente> obtenerClientesMorosos(){
        Query q = em.createQuery("SELECT c FROM CuentaCliente c ORDER BY c.debe desc")
                .setMaxResults(10);
        if(q.getResultList().size()>0)
            return q.getResultList();
        else
            throw new NoResultException();
    }
    
    
    
}
