/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cubisoft.carycob.ejb;

import cubisoft.carycob.vistas.VentasCliente;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author Carlos Delgadillo
 */
@Stateless
public class VentasClienteFacade extends AbstractFacade<VentasCliente> {
    @PersistenceContext(unitName = "carycobPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public VentasClienteFacade() {
        super(VentasCliente.class);
    }
    
    public List<VentasCliente> ventasPorCliente(String vendedor){
        Query q = em.createQuery("SELECT v FROM VentasCliente v WHERE v.vendedor = :vendedor")
                .setParameter("vendedor", vendedor);
//        if(q.getResultList().size()>0)
            return q.getResultList();
//        else
//            throw new Exception();
    }
}
