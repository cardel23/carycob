/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cubisoft.carycob.ejb;

import cubisoft.carycob.entidades.Cliente;
import cubisoft.carycob.vistas.DetalleFactura;
import cubisoft.carycob.entidades.Factura;
import cubisoft.carycob.vistas.SaldoFactura;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author Estacion 2
 */
@Stateless
public class FacturaFacade extends AbstractFacade<Factura> {
    @PersistenceContext(unitName = "carycobPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    private Factura factura;

    public FacturaFacade() {
        super(Factura.class);
    }
    /**
     * Busca una factura en la base de datos
     * @param unaFactura una factura
     * @return la factura si existe
     */
    public boolean existeFactura(Factura unaFactura){
        try {
            if(this.getFactura() == null || this.getFactura().getIdFactura()!=unaFactura.getIdFactura()){
            this.factura = (Factura) getEntityManager().createNamedQuery("Factura.findByIdFactura")
                    .setParameter("idFactura", unaFactura.getIdFactura())
                    .getSingleResult();}
            return true;
        } catch (NoResultException e) {
            return false;
        }
        
    }
    
    public Factura findFactura(String idFactura){
        try {
            Query q = em.createNamedQuery("Factura.findByIdFactura")
                    .setParameter("idFactura", idFactura);
            return (Factura)q.getSingleResult();
        } catch (NoResultException e) {
            throw new NoResultException("No hay una factura con id "+idFactura);
        }
        
    }
    
    public List<Factura> getListaFacturas(Cliente cliente){
        Query q = em.createNamedQuery("Factura.findByCliente")
                .setParameter("cliente", cliente);
        return q.getResultList();
    }
    
    public List<DetalleFactura> getDetalleFactura(String idFactura){
        Query q = em.createNamedQuery("DetalleFactura.findByIdFactura")
                .setParameter("idFactura", idFactura);
        return q.getResultList();
    }

    /**
     * @return the factura
     */
    public Factura getFactura() {
        return factura;
    }
    
    /**
     * El total facturado a los clientes que tienen facturas pagadas
     * @autor Carlos Delgadillo
     * @fecha 31/03/2014
     * @return saldo 
     */
    public double saldoFacturado(){
        double techo = 0;
        try {
            Query q = em.createNamedQuery("Factura.findByEstado")
                    .setParameter("estado", Factura.pendiente);
            for(Factura f : (List<Factura>)q.getResultList()){
                techo += f.getMonto();
            }
            q = em.createNamedQuery("Factura.findByEstado")
                    .setParameter("estado", Factura.vencida);
            for(Factura f : (List<Factura>)q.getResultList()){
                techo += f.getMonto();
            }
            return techo;
        } catch (Exception e) {
            return 0;
        }
        
    }
    
    /**
     * Saldo a vencer
     * @autor Carlos Delgadillo
     * @fecha 31/03/2014
     * @return saldo que esta vencido a la fecha
     */
    public double saldoAVencer(){
        Query q = em.createQuery("SELECT SUM(f.monto) FROM Factura f WHERE f.fechaVencimiento <= CURRENT_DATE");
        return (double)q.getSingleResult();
    }
    
    public double saldoNoVencido(){
        Query q = em.createQuery("SELECT SUM(f.monto) FROM Factura f WHERE f.fechaVencimiento > CURRENT_DATE");
        return (double)q.getSingleResult();
    }
    
    /**
     * Obtiene una lista de facturas por estado
     * @param estado anulada, vencida, cancelada, pendiente
     * @return lista de facturas
     */
    public List<Factura> facturasPorEstado(String estado){
        Query q = em.createNamedQuery("Factura.findByEstado")
                .setParameter("estado", estado);
        return q.getResultList();
    }
    
    public List<Factura> facturasPorCliente(Cliente cliente){
        try {
            Query q = em.createNamedQuery("Factura.findByCliente")
                    .setParameter("cliente", cliente);
            return q.getResultList();
        } catch (Exception e) {
            return null;
        }
        
    }
    
    public boolean isFacturaVencida(Factura factura){
        if(factura.getFechaVencimiento().before(Calendar.getInstance().getTime()) || factura.getEstado().equals(Factura.vencida))
            return true;
        else
            return false;
    }
    
       
}
