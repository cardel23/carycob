/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cubisoft.carycob.ejb;

import cubisoft.carycob.entidades.Carga;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Estacion 2
 */
@Stateless
public class CargaFacade extends AbstractFacade<Carga> {
    @PersistenceContext(unitName = "carycobPU")
    private EntityManager em;
    private Carga carga;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public CargaFacade() {
        super(Carga.class);
    }
    
    public boolean existeCarga(Carga carga){
        try {
            carga = (Carga) getEntityManager().createNamedQuery("Carga.findByIdCarga")
                    .setParameter("idCarga", carga.getIdCarga())
                    .getSingleResult();
            return true;
        } catch (NoResultException e) {
            return false;
        }
    }
    
}
