/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cubisoft.carycob.ejb;

import cubisoft.carycob.entidades.Carga;
import cubisoft.carycob.entidades.Pago;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Estacion 2
 */
@Stateless
public class PagoFacade extends AbstractFacade<Pago> {
    @PersistenceContext(unitName = "carycobPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    private Pago pago;
    public PagoFacade() {
        super(Pago.class);
    }
    /**
     * Busca un pago en la base de datos
     * @param unPago un objeto Pago
     * @return el pago solicitado
     */
    public boolean existePago(Pago unPago){
        try {
            pago = (Pago) em.createNamedQuery("Pago.findByFechaEmisionMontoFactura")
                    .setParameter("fechaEmision", unPago.getFechaEmision())
                    .setParameter("monto", unPago.getMonto())
                    .setParameter("factura", unPago.getFactura())
                    .getSingleResult();
            return true;
        } catch (NoResultException e) {
            return false;
        }
        
    }
    
    public List<Pago> findByCarga(Carga carga){
        List<Pago> listaPago = new ArrayList<>();
        
        listaPago = em.createNamedQuery("Pago.findByCarga")
                .setParameter("carga", carga)
                .getResultList();
        return listaPago;
    }
    
    public Pago getPago(){
        return pago;
    }
}
