package cubisoft.carycob.managedBeans;

import cubisoft.carycob.entidades.NotaPendiente;
import cubisoft.carycob.managedBeans.util.JsfUtil;
import cubisoft.carycob.managedBeans.util.PaginationHelper;
import cubisoft.carycob.ejb.NotaPendienteFacade;
import cubisoft.carycob.entidades.Carga;
import cubisoft.carycob.entidades.Cliente;
import cubisoft.carycob.entidades.Factura;
import cubisoft.carycob.entidades.Nota;
import cubisoft.carycob.entidades.Usuario;
import java.io.Serializable;
import java.util.ResourceBundle;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import javax.faces.model.SelectItem;

@ManagedBean(name = "notaPendienteController")
@SessionScoped
public class NotaPendienteController implements Serializable {

    private NotaPendiente current;
    private DataModel items = null;
    @EJB
    private cubisoft.carycob.ejb.NotaPendienteFacade ejbFacade;
    @EJB
    private cubisoft.carycob.ejb.FacturaFacade ejbFactura;
    @EJB
    private cubisoft.carycob.ejb.ClienteFacade ejbCliente;
    private PaginationHelper pagination;
    private int selectedItemIndex;
    private Nota nota;
    private Factura factura;
    private Cliente cliente;
    private Usuario usuario;

    public NotaPendienteController() {
    }
    
    public String asignarCliente(){
        try {
            cliente = current.getCliente();
            getFactura();
            factura.setCliente(cliente);
            factura.setIdFactura(JsfUtil.generarID(JsfUtil.factura));
            factura.setEstado(Factura.debito);
            factura.setMonto(current.getMonto());
            factura.setFechaEmision(new java.util.Date(System.currentTimeMillis()));
            factura.setFechaVencimiento(null);
            factura.setVendedor(null);
            factura.setCarga(new Carga(JsfUtil.generarID(JsfUtil.carga),new java.util.Date(System.currentTimeMillis())));
            factura.getCarga().setUsuario(usuario);
            cliente.getFacturaList().add(factura);
            cliente.getNotaPendienteList().remove(current);
            ejbCliente.edit(cliente);
            factura = null;
            recreateModel();
            JsfUtil.addSuccessMessage("Factura agregada exitosamente");
//            return null;
        } catch (Exception e) {
            factura = null;
            JsfUtil.addErrorMessage(e, "No se pudo agregar la factura");
//            return "List?faces-redirect=true";
        }
        return "List?faces-redirect=true";
    }
    
    public String asignarFactura(){
        try {
            getNota();
            nota.setTipoNota(current.getTipoNota());
            nota.setMonto(current.getMonto());
            nota.setFecha(new java.util.Date(System.currentTimeMillis()));
            nota.setObservaciones(current.getObservaciones());
            nota.setCarga(new Carga(JsfUtil.generarID(JsfUtil.carga),new java.util.Date(System.currentTimeMillis())));
            nota.getCarga().setUsuario(usuario);
            if(nota.getTipoNota().equals(Nota.CREDITO)){
                nota.setIdNota(JsfUtil.generarID(JsfUtil.notaCredito));
                nota.getCarga().setDescripcion(Carga.cargaSimpleNotaCredito);
            }
            else{
                nota.setIdNota(JsfUtil.generarID(JsfUtil.notaDebito));
                nota.getCarga().setDescripcion(Carga.cargaSimpleNotaDebito);
            }
            factura = nota.getFactura();
            factura.getNotaList().add(nota);
            ejbFactura.edit(factura);
            ejbFacade.remove(current);
            factura = null;
            nota = null;
            recreateModel();
            JsfUtil.addSuccessMessage("Nota agregada exitosamente");
            return "/nota/List?faces-redirect=true";
        } catch (Exception e) {
            factura = null;
            nota = null;
            JsfUtil.addErrorMessage(e, "No se pudo agregar la nota");
            return "List?faces-redirect=true";
        }
    }

    public NotaPendiente getSelected() {
        if (current == null) {
            current = new NotaPendiente();
            selectedItemIndex = -1;
        }
        return current;
    }
    
    public NotaPendiente getNotaPendiente(){
        current = (NotaPendiente) getItems().getRowData();
        return current;
    }

    private NotaPendienteFacade getFacade() {
        return ejbFacade;
    }

    public PaginationHelper getPagination() {
        if (pagination == null) {
            pagination = new PaginationHelper(10) {
                @Override
                public int getItemsCount() {
                    return getFacade().count();
                }

                @Override
                public DataModel createPageDataModel() {
                    return new ListDataModel(getFacade().findRange(new int[]{getPageFirstItem(), getPageFirstItem() + getPageSize()}));
                }
            };
        }
        return pagination;
    }

    public String prepareList() {
        recreateModel();
        return "List?faces-redirect=true";
    }

    public String prepareView() {
        current = (NotaPendiente) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        return "View?faces-redirect=true";
    }

    public String prepareCreate() {
        current = new NotaPendiente();
        selectedItemIndex = -1;
        return "Create?faces-redirect=true";
    }

    public String create() {
        try {
            
            if(current.getTipoNota().equals(Nota.CREDITO))
            {
                current.setIdNotaPendiente(JsfUtil.generarID(JsfUtil.notaCredito));
                current.getCarga().setDescripcion(Carga.cargaSimpleNotaCredito);
            }
            else{
                current.setIdNotaPendiente(JsfUtil.generarID(JsfUtil.notaDebito));
                current.getCarga().setDescripcion(Carga.cargaSimpleNotaDebito);}
            current.getCarga().getNotaPendienteList().add(current);
            getFacade().create(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/Bundle").getString("NotaPendienteCreated"));
            return prepareCreate();
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
            return null;
        }
    }

    public String prepareEdit() {
        current = (NotaPendiente) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        return "Edit?faces-redirect=true";
    }

    public String update() {
        try {
            getFacade().edit(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/Bundle").getString("NotaPendienteUpdated"));
            return "View?faces-redirect=true";
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
            return null;
        }
    }

    public String destroy() {
        current = (NotaPendiente) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        performDestroy();
        recreatePagination();
        recreateModel();
        return "List";
    }

    public String destroyAndView() {
        performDestroy();
        recreateModel();
        updateCurrentItem();
        if (selectedItemIndex >= 0) {
            return "View?faces-redirect=true";
        } else {
            // all items were removed - go back to list
            recreateModel();
            return "List?faces-redirect=true";
        }
    }

    private void performDestroy() {
        try {
            getFacade().remove(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/Bundle").getString("NotaPendienteDeleted"));
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
        }
    }

    private void updateCurrentItem() {
        int count = getFacade().count();
        if (selectedItemIndex >= count) {
            // selected index cannot be bigger than number of items:
            selectedItemIndex = count - 1;
            // go to previous page if last page disappeared:
            if (pagination.getPageFirstItem() >= count) {
                pagination.previousPage();
            }
        }
        if (selectedItemIndex >= 0) {
            current = getFacade().findRange(new int[]{selectedItemIndex, selectedItemIndex + 1}).get(0);
        }
    }

    public DataModel getItems() {
        if (items == null) {
            items = getPagination().createPageDataModel();
        }
        return items;
    }

    private void recreateModel() {
        items = null;
    }

    private void recreatePagination() {
        pagination = null;
    }

    public String next() {
        getPagination().nextPage();
        recreateModel();
        return "List";
    }

    public String previous() {
        getPagination().previousPage();
        recreateModel();
        return "List";
    }

    public SelectItem[] getItemsAvailableSelectMany() {
        return JsfUtil.getSelectItems(ejbFacade.findAll(), false);
    }

    public SelectItem[] getItemsAvailableSelectOne() {
        return JsfUtil.getSelectItems(ejbFacade.findAll(), true);
    }

    public NotaPendiente getNotaPendiente(java.lang.String id) {
        return ejbFacade.find(id);
    }

    public Nota getNota() {
        if(nota == null)
            nota = new Nota();
        return nota;
    }

    public void setNota(Nota nota) {
        this.nota = nota;
    }

    public Factura getFactura() {
        if (factura == null)
            factura = new Factura();
        return factura;
    }

    public void setFactura(Factura factura) {
        this.factura = factura;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    @FacesConverter(forClass = NotaPendiente.class)
    public static class NotaPendienteControllerConverter implements Converter {

        @Override
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            NotaPendienteController controller = (NotaPendienteController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "notaPendienteController");
            return controller.getNotaPendiente(getKey(value));
        }

        java.lang.String getKey(String value) {
            java.lang.String key;
            key = value;
            return key;
        }

        String getStringKey(java.lang.String value) {
            StringBuilder sb = new StringBuilder();
            sb.append(value);
            return sb.toString();
        }

        @Override
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof NotaPendiente) {
                NotaPendiente o = (NotaPendiente) object;
                return getStringKey(o.getIdNotaPendiente());
            } else {
                throw new IllegalArgumentException("object " + object + " is of type " + object.getClass().getName() + "; expected type: " + NotaPendiente.class.getName());
            }
        }
    }
}
