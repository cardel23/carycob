package cubisoft.carycob.managedBeans;

import cubisoft.carycob.entidades.Carga;
import cubisoft.carycob.managedBeans.util.JsfUtil;
import cubisoft.carycob.managedBeans.util.PaginationHelper;
import cubisoft.carycob.ejb.CargaFacade;
import cubisoft.carycob.ejb.ClienteFacade;
import cubisoft.carycob.ejb.FacturaFacade;
import cubisoft.carycob.ejb.PagoFacade;
import cubisoft.carycob.ejb.VendedorFacade;
import cubisoft.carycob.entidades.Cliente;
import cubisoft.carycob.entidades.Factura;
import cubisoft.carycob.entidades.Nota;
import cubisoft.carycob.entidades.NotaPendiente;
import cubisoft.carycob.entidades.Pago;
import cubisoft.carycob.entidades.Rechazo;
import cubisoft.carycob.entidades.Usuario;
import cubisoft.carycob.excepciones.FacturaException;
import cubisoft.carycob.excepciones.NoClienteException;
import cubisoft.carycob.excepciones.PagoException;
import cubisoft.carycob.excepciones.ProcesadorExcelException;
import cubisoft.carycob.excepciones.RegistroException;
import cubisoft.carycob.managedBeans.util.ProcesadorExcel;
import cubisoft.carycob.managedBeans.util.Registro;
import cubisoft.carycob.managedBeans.util.RegistroNota;
import cubisoft.carycob.managedBeans.util.SesionMB;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import javax.faces.model.SelectItem;
import javax.persistence.NoResultException;
import javax.validation.ConstraintViolationException;
import org.primefaces.event.FileUploadEvent;

@ManagedBean(name = "cargaController")
@SessionScoped
public class CargaController implements Serializable {

    private Carga current;
    private DataModel items = null;
    @EJB
    private cubisoft.carycob.ejb.CargaFacade ejbFacade;
    @EJB
    private FacturaFacade ejbFactura;
    @EJB
    private ClienteFacade ejbCliente;
    @EJB
    private PagoFacade ejbPago;
    @EJB
    private VendedorFacade ejbVendedor;
    private PaginationHelper pagination;
    private int selectedItemIndex;
    private Factura nuevaFactura;
    private boolean panelGuardar;
    private Usuario usuario;
    @ManagedProperty(value = "#{sesionMB}")
    private SesionMB sesionMB;
    
    private boolean activarGuardar = false;

    public CargaController() {
    }
    
    /**
     * Carga las facturas de un archivo de excel al sistema
     * @author Carlos Delgadillo
     * @since 13/03/2014
     * @param event es el archivo que obtiene del evento FileUploadEvent
     * de Primefaces
     * @throws FileNotFoundException si no encuentra el archivo
     * @throws IOException si se produce un error de lectura
     */
    public void cargarFacturas(FileUploadEvent event) throws FileNotFoundException, IOException, FacturaException{
        List<String> validador = new ArrayList<>();
        crearCarga(Carga.cargaMasivaFacturas); //crea el objeto Carga
        try {
            for(Registro registro : ProcesadorExcel.leerFacturas(event.getFile())){
                try{
                    validarRegistro(validador, registro);
                    if(registro.getMonto()<=0)
                        throw new RegistroException("El monto de una factura o pago no puede ser negativo o igual a 0");
                    crearNuevaFactura(registro); //crea la nueva factura
                    if(!ejbFactura.existeFactura(getNuevaFactura()) && !current.getFacturaList().contains(getNuevaFactura())){
                        current.getFacturaList().add(getNuevaFactura()); //agrega la nueva factura a la lista si no se encuentra registrada
                    }
                    else{
                        crearNuevoPago(registro, getNuevaFactura()); //crea un nuevo pago
                    }
                }
                catch(NoResultException | NoClienteException | PagoException | RegistroException e){
                    //crea un nuevo pago
                    //genera un rechazo
                    current.getRechazoList().add(crearRechazoCarga(registro, e));
                    validador.add(registro.getId());
                }
            }
            /*
             * Ya se ha creado la lista
             * Ahora se procede a guardar la carga con todos sus elementos
             */ 
        
            if(current.getFacturaList().size()>0)
                JsfUtil.addSuccessMessage("Se van a registrar "+current.getFacturaList().size()+" nuevas facturas");
            int pagos = 0;
            for(Factura f:current.getFacturaList()){
                pagos += f.getPagoList().size();
            }
            if(current.getPagoList().size()>0 || pagos > 0)
                JsfUtil.addSuccessMessage("Se van a registrar "+(pagos+current.getPagoList().size())+" pagos");
            if(current.getRechazoList().size()>0)
                JsfUtil.addAlertMessage(current.getRechazoList().size()+" registros no fueron validados");
            activarGuardar = true;
        }catch (ProcesadorExcelException ex) {
            JsfUtil.addErrorMessage(ex.getMessage());
        }
    }
    private void validarRegistro(List<String> lista, Registro registro) throws RegistroException{
        if(lista.contains(registro.getId())) //Si hay un registro invalidado con un id, todos los demas se invalidan
            throw new RegistroException("El registro fue rechazado porque "
                +new PagoException("un pago para el nº de factura '"+registro.getId()
                    +" no se validó correctamente").getMessage());
    }
    private void crearCarga(String descripcion){
        current = new Carga();
        current.setIdCarga(JsfUtil.generarID(JsfUtil.carga));
        while(ejbFacade.existeCarga(current))
            current.setIdCarga(JsfUtil.generarID(JsfUtil.carga));
        if(isCargaFacturas(descripcion)){
            current.setFacturaList(new ArrayList<Factura>());
            current.setPagoList(new ArrayList<Pago>());
        }
        else{
            current.setNotaList(new ArrayList<Nota>());
            current.setNotaPendienteList(new ArrayList<NotaPendiente>());
        }
        
        current.setRechazoList(new ArrayList<Rechazo>());
        Date fecha = new Date(System.currentTimeMillis());
        current.setFecha(fecha);
        current.setUsuario(sesionMB.getUsuario());
        current.setDescripcion(descripcion);
    }
    
    /**
     * Crea un registro rechazado por no pasar las validaciones correspondientes
     * @param registro
     * @param e
     * @return el registro invalidado
     * @autor Carlos Delgadillo
     * @fecha 09/05/2014
     */
    private Rechazo crearRechazoCarga(Registro registro, Throwable e){
        Rechazo rechazo = new Rechazo(JsfUtil.generarID(JsfUtil.rechazo),registro.getNumFila());
        rechazo.setCliente(registro.getCliente());
        rechazo.setFactura(registro.getId());
        rechazo.setFechaEmision(registro.getFechaEmision());
        rechazo.setFechaVencimiento(registro.getFechaVencimiento());
        rechazo.setMonto(registro.getMonto());
        rechazo.setObservaciones(e.getMessage());
        rechazo.setCarga(current);
        return rechazo;
    }
    private Rechazo crearRechazoCarga(RegistroNota registro, Throwable e){
        Rechazo rechazo = new Rechazo(JsfUtil.generarID(JsfUtil.rechazo),registro.getNumFila());
        rechazo.setCliente(registro.getCliente());
        rechazo.setFactura(registro.getId());
        rechazo.setFechaEmision(registro.getFecha());
//        rechazo.setFechaVencimiento(registro.getFechaVencimiento());
        rechazo.setMonto(registro.getMonto());
        rechazo.setObservaciones(e.getMessage());
        rechazo.setCarga(current);
        return rechazo;
    }
    
    /**
     * Se encarga de crear una nueva factura a la carga masiva
     * @param registro
     * @throws NoClienteException 
     * @autor Carlos Delgadillo
     * @fecha 09/05/2014
     */
    private void crearNuevaFactura(Registro registro) throws NoClienteException{
        setNuevaFactura(new Factura(registro.getId()));

        Cliente cliente;
        cliente = ejbCliente.findByNombre(registro.getCliente());

        nuevaFactura.setCliente(cliente);
        nuevaFactura.setFechaEmision(registro.getFechaEmision());
        nuevaFactura.setFechaVencimiento(registro.getFechaVencimiento());
        nuevaFactura.setMonto(registro.getMonto());
        nuevaFactura.setCarga(current);
        nuevaFactura.setPagoList(new ArrayList<Pago>());
        nuevaFactura.setVendedor(ejbVendedor.findByNombre(registro.getVendedor()));
        if(registro.getFechaVencimiento().before(Calendar.getInstance().getTime()) ) {
            nuevaFactura.setEstado(Factura.vencida);
        } else {
            nuevaFactura.setEstado(Factura.pendiente);
        }
    }
    /**
     * Se encarga de crear un pago a la carga masiva
     * @param registro
     * @param factura
     * @throws PagoException
     * @autor Carlos Delgadillo
     * @fecha 09/05/2014
     */
    private void crearNuevoPago(Registro registro, Factura factura) throws PagoException{
        Pago pago = new Pago(JsfUtil.generarID(JsfUtil.pago));
        pago.setFechaEmision(Calendar.getInstance().getTime());
        pago.setFactura(factura);
        pago.setCarga(current);

        if(ejbFactura.existeFactura(factura)){
            double saldo = 0;
            Factura f = ejbFactura.getFactura();
            if(!factura.getPagoList().isEmpty())
                for (Pago p : getNuevaFactura().getPagoList()){
                    saldo -= p.getMonto();
                }

            if(!current.getPagoList().isEmpty())
                for (Pago p : current.getPagoList()){
                    if(p.getFactura().equals(factura))
                    saldo -= p.getMonto();
                }
            saldo += f.getMonto();

            if(registro.getMonto()>saldo){
                throw new PagoException("El saldo del registro no puede ser mayor que el saldo actual de la factura");
            }
            else{
                pago.setMonto(saldo-registro.getMonto());
                if(pago.getMonto()==0)
                    throw new PagoException("No se puede registrar un pago por monto 0");
                current.getPagoList().add(pago);
            }
        }
        if(current.getFacturaList().contains(factura)){
            double saldo = 0;
            Factura f = current.getFacturaList().get(current.getFacturaList().indexOf(factura));
            if(!f.getPagoList().isEmpty())
                for (Pago p : f.getPagoList()){
                    saldo -= p.getMonto();
                }
            if(!current.getPagoList().isEmpty())
                for (Pago p : current.getPagoList()){
                    if(p.getFactura().equals(f))
                    saldo -= p.getMonto();
                }
            saldo += f.getMonto();

            if(registro.getMonto()>saldo){
                throw new PagoException("El saldo del registro no puede ser mayor que el saldo actual de la factura");
            }
            else{
                pago.setMonto(saldo-registro.getMonto());
                if(pago.getMonto()==0)
                    throw new PagoException("No se puede registrar un pago por monto 0");
                f.getPagoList().add(pago);
            }
        }
                
    }
    
    /**
     * Hace una carga masiva de notas de crédito o débito al sistema
     * @param event
     * @throws IOException
     * @throws Exception
     * @autor Carlos Delgadillo
     * @fecha 09/05/2014
     */
    public void cargarNotas(FileUploadEvent event) throws IOException, Exception{
        crearCarga(Carga.cargaMasivaNotas);
        for(RegistroNota r : ProcesadorExcel.leerNotas(event.getFile())){
            try {
                if(!isNotaCliente(r))
                {
                    current.getNotaList().add(crearNota(r));
                }
                else
                    current.getNotaPendienteList().add(crearNotaPendiente(r));
            } 
            catch (NoClienteException e){
                current.getRechazoList().add(crearRechazoCarga(r, e));
                System.out.print(current.getRechazoList().size());
            }
            catch (Exception e) {
                current.getRechazoList().add(crearRechazoCarga(r, e.getCause()));
                System.out.print(current.getRechazoList().size());
            }
            
        }
        JsfUtil.addSuccessMessage("Se crearan "+current.getNotaList().size()+" notas de factura");
        JsfUtil.addSuccessMessage("Se crearan "+current.getNotaPendienteList().size()+" notas de cliente");
        JsfUtil.addAlertMessage(current.getRechazoList().size()+" registros son rechazados");
        activarGuardar = true;
    }
    /**
     * Crea una nota de crédito o debito a una factura
     * @param reg
     * @return la nota que va a la lista de notas de factura
     * @throws Exception
     * @autor Carlos Delgadillo
     * @fecha 10/05/2014
     */
    private Nota crearNota(RegistroNota reg) throws NoResultException {
        
        try{Nota nota = new Nota();
        nota.setCarga(current);
        nota.setFactura(ejbFactura.findFactura(reg.getFactura()));
        nota.setFecha(reg.getFecha());
        nota.setMonto(reg.getMonto());
        nota.setObservaciones(reg.getObservaciones());
        nota.setTipoNota(reg.getTipoNota());
//        if(nota.getTipoNota().equals(Nota.CREDITO))
            nota.setIdNota(reg.getId());
//        else
//            nota.setIdNota(JsfUtil.generarID(JsfUtil.notaDebito));
        return nota;}
        catch(NoResultException e){
            throw e;
        }
    }
    
    /**
     * Crea una nota de crédito o debito a un cliente
     * @param reg
     * @return la nota que va a la lista de notas de cliente
     * @throws Exception
     * @autor Carlos Delgadillo
     * @fecha 10/05/2014
     */
    private NotaPendiente crearNotaPendiente(RegistroNota reg) throws NoClienteException{
        try{NotaPendiente nota = new NotaPendiente();
        nota.setCarga(current);
        nota.setCliente(ejbCliente.findByNombre(reg.getCliente()));
        nota.setFecha(reg.getFecha());
        nota.setMonto(reg.getMonto());
        nota.setObservaciones(reg.getObservaciones());
        nota.setTipoNota(reg.getTipoNota());
//        if(nota.getTipoNota().equals(Nota.CREDITO))
            nota.setIdNotaPendiente(reg.getId());
        
        return nota;}
        catch(Exception e){
            throw e;
        }
    }
    
    public Carga crearCargaDefault(Usuario usuario){
        current = new Carga();
        current.setIdCarga(JsfUtil.generarID(JsfUtil.carga));
        current.setFecha(Calendar.getInstance().getTime());
        current.setUsuario(usuario);
        current.setFacturaList(new ArrayList<Factura>());
        current.setPagoList(new ArrayList<Pago>());
        current.setNotaList(new ArrayList<Nota>());
        current.setNotaPendienteList(new ArrayList<NotaPendiente>());
        return current;
    }
    
    private boolean isCargaFacturas(String descripcion){
        if(descripcion.equals(Carga.cargaMasivaFacturas))
            return true;
        else
            return false;
    }
    
    private boolean isNotaCliente(RegistroNota registro) throws RegistroException {
        if(registro.getCliente()!=null && registro.getFactura()==null || registro.getFactura().equals("") || registro.getFactura().equals("false"))
            return true;
        else if(registro.getCliente()==null || registro.getCliente().equals("") || registro.getCliente().equals("false") && registro.getFactura()!=null)
            return false;
        else
            throw new RegistroException("Se ha producido un error de validación en el registro");
    }
    public void activarBoton(){
        setPanelGuardar(true);
    }
    
//    public void limpiarClases(){
//        ejbFactura.limpiar(ejbFactura.entidad);
//        ejbPago.limpiar(ejbPago.entidad);
//        ejbRechazo.limpiar(ejbRechazo.entidad);
//        
//    }
    
    public List<Carga> getListaItems(){
        return ejbFacade.findAll();
    }
    public void anularCarga(){
        current = null;
        setPanelGuardar(false);
    }
    
    public List<Pago> obtenerPagosCarga(){
        return ejbPago.findByCarga(current);
    }
    
    public String regInv(){
        return "RegistrosInvalidos?faces-redirect=true";
    }

    public Carga getSelected() {
        if (current == null) {
            current = new Carga();
            selectedItemIndex = -1;
        }
        return current;
    }

    private CargaFacade getFacade() {
        return ejbFacade;
    }

    public PaginationHelper getPagination() {
        if (pagination == null) {
            pagination = new PaginationHelper(10) {
                @Override
                public int getItemsCount() {
                    return getFacade().count();
                }

                @Override
                public DataModel createPageDataModel() {
                    return new ListDataModel(getFacade().findAll());
                }
            };
        }
        return pagination;
    }

    public String prepareList() {
        recreateModel();
        getPagination();
        return "/carga/List?faces-redirect=true";
    }

    public String prepareView() {
        current = (Carga) getItems().getRowData();
//        current.setFacturaList(facturaFacade.getListaFacturas(current));
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        return "DetalleCarga?faces-redirect=true";
    }

    public String prepareCreate() {
        current = new Carga();
        selectedItemIndex = -1;
        return "Create";
    }

    public String create() {
//        try {
//            getFacade().create(current);
//            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/Bundle").getString("CargaCreated"));
//            return prepareCreate();
//        } catch (Exception e) {
//            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
//            return null;
//        }
        try {
            ejbFacade.create(current);
            JsfUtil.addSuccessMessage("Carga guardada");
            activarGuardar = false;
            return "DetalleCarga?faces-redirect=true";
        } catch (EJBException e) {
            JsfUtil.addErrorMessage("Debe subir un nuevo archivo de excel", e.getMessage());
            return null;
        }
    }

    public String prepareEdit() {
        current = (Carga) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        return "Edit?faces-redirect=true";
    }

    public String update() {
        try {
            getFacade().edit(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/Bundle").getString("CargaUpdated"));
            return "DetalleCarga?faces-redirect=true";
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
            return null;
        }
    }

    public String destroy() {
        current = (Carga) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        performDestroy();
        recreatePagination();
        recreateModel();
        return "List";
    }

    public String destroyAndView() {
        performDestroy();
        recreateModel();
        updateCurrentItem();
        if (selectedItemIndex >= 0) {
            return "View";
        } else {
            // all items were removed - go back to list
            recreateModel();
            return "List";
        }
    }

    private void performDestroy() {
        try {
            getFacade().remove(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/Bundle").getString("CargaDeleted"));
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
        }
    }

    private void updateCurrentItem() {
        int count = getFacade().count();
        if (selectedItemIndex >= count) {
            // selected index cannot be bigger than number of items:
            selectedItemIndex = count - 1;
            // go to previous page if last page disappeared:
            if (pagination.getPageFirstItem() >= count) {
                pagination.previousPage();
            }
        }
        if (selectedItemIndex >= 0) {
            current = getFacade().findRange(new int[]{selectedItemIndex, selectedItemIndex + 1}).get(0);
        }
    }

    public DataModel getItems() {
        if (items == null) {
            items = getPagination().createPageDataModel();
        }
        return items;
    }

    private void recreateModel() {
        items = null;
    }

    private void recreatePagination() {
        pagination = null;
    }

    public String next() {
        getPagination().nextPage();
        recreateModel();
        return "List";
    }

    public String previous() {
        getPagination().previousPage();
        recreateModel();
        return "List";
    }

    public SelectItem[] getItemsAvailableSelectMany() {
        return JsfUtil.getSelectItems(ejbFacade.findAll(), false);
    }

    public SelectItem[] getItemsAvailableSelectOne() {
        return JsfUtil.getSelectItems(ejbFacade.findAll(), true);
    }

    public Carga getCarga(java.lang.String id) {
        return ejbFacade.find(id);
    }

    public boolean isPanelGuardar() {
        return panelGuardar;
    }

    public void setPanelGuardar(boolean panelGuardar) {
        this.panelGuardar = panelGuardar;
    }

    /**
     * @return the usuario
     */
    public Usuario getUsuario() {
        return usuario;
    }

    /**
     * @param usuario the usuario to set
     */
    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public SesionMB getSesionMB() {
        return sesionMB;
    }

    public void setSesionMB(SesionMB sesionMB) {
        this.sesionMB = sesionMB;
    }

    public Factura getNuevaFactura() {
        return nuevaFactura;
    }

    public void setNuevaFactura(Factura nuevaFactura) {
        this.nuevaFactura = nuevaFactura;
    }

    public void setSelected(Carga current) {
        this.current = current;
    }

    public boolean isActivarGuardar() {
        return activarGuardar;
    }

    public void setActivarGuardar(boolean activarGuardar) {
        this.activarGuardar = activarGuardar;
    }
    
    @FacesConverter(forClass = Carga.class)
    public static class CargaControllerConverter implements Converter {

        @Override
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            CargaController controller = (CargaController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "cargaController");
            return controller.getCarga(getKey(value));
        }

        java.lang.String getKey(String value) {
            java.lang.String key;
            key = value;
            return key;
        }

        String getStringKey(java.lang.String value) {
            StringBuilder sb = new StringBuilder();
            sb.append(value);
            return sb.toString();
        }

        @Override
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof Carga) {
                Carga o = (Carga) object;
                return getStringKey(o.getIdCarga());
            } else {
                throw new IllegalArgumentException("object " + object + " is of type " + object.getClass().getName() + "; expected type: " + Carga.class.getName());
            }
        }
    }
}
