/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cubisoft.carycob.managedBeans;

import cubisoft.carycob.ejb.EmailSender;
import cubisoft.carycob.emailUtils.Email;
import cubisoft.carycob.entidades.Cliente;
import cubisoft.carycob.managedBeans.util.JsfUtil;
import cubisoft.carycob.managedBeans.util.SesionMB;
import cubisoft.carycob.vistas.CuentaCliente;
import java.io.Serializable;
import java.util.ArrayList;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;

/**
 *
 * @author Darlon
 */
@ManagedBean(name = "emailSenderController")
@SessionScoped
public class EmailSenderController implements Serializable{
    private Email email;
    @EJB
    private EmailSender emailSender;
    
    private String destinatario;
    @ManagedProperty(value = "#{sesionMB}")
    private SesionMB sesionMB;
    
    private byte[] pdf;
    private boolean adjunto;
    
    /**
     * Creates a new instance of EmailController
     */
    public EmailSenderController() {
    }
    
    public void sendEmail(CuentaCliente cuenta, byte[] pdf, boolean adjunto, String nombreArchivo){
        if(email!=null){
            
            email.setTo(destinatario);
            email.setCc(new ArrayList<String>());
            email.getCc().add(email.getFrom());
            boolean enviado=emailSender.sendEmail(email, pdf, adjunto, nombreArchivo);
            if(enviado)
                JsfUtil.addSuccessMessage("El mensaje a sido enviado a ".concat(email.getTo()));
            else
                JsfUtil.addErrorMessage("Error al enviar el correo");            
        }
        else{
            JsfUtil.addAlertMessage("No se han definido los datos del email");
        }
    }

    public Email getEmail() {
        if(email==null)
            email=new Email();
        return email;
    }

    public void setEmail(Email email) {
        this.email = email;
    }


    public SesionMB getSesionMB() {
        return sesionMB;
    }

    public void setSesionMB(SesionMB sesionMB) {
        this.sesionMB = sesionMB;
    }

    public byte[] getPdf() {
        return pdf;
    }

    public void setPdf(byte[] pdf) {
        this.pdf = pdf;
    }

    public boolean isAdjunto() {
        return adjunto;
    }

    public void setAdjunto(boolean adjunto) {
        this.adjunto = adjunto;
    }

    public String getDestinatario() {
        return destinatario;
    }

    public void setDestinatario(String destinatario) {
        this.destinatario = destinatario;
    }
 
}
