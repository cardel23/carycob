package cubisoft.carycob.managedBeans;

import cubisoft.carycob.entidades.Gestion;
import cubisoft.carycob.managedBeans.util.JsfUtil;
import cubisoft.carycob.managedBeans.util.PaginationHelper;
import cubisoft.carycob.ejb.GestionFacade;
import cubisoft.carycob.entidades.Factura;
import cubisoft.carycob.entidades.Usuario;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.ResourceBundle;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import javax.faces.model.SelectItem;

@ManagedBean(name = "gestionController")
@SessionScoped
public class GestionController implements Serializable {

    private Gestion current;
    private DataModel items = null;
    @EJB
    private cubisoft.carycob.ejb.GestionFacade ejbFacade;
    @EJB
    private cubisoft.carycob.ejb.UsuarioFacade ejbUsuario;
    private PaginationHelper pagination;
    private int selectedItemIndex;
    private Factura factura;
    private Usuario usuario;

    public GestionController() {
    }
    
    public void crearGestion(){
        current.setFactura(factura);
        current.setEstado(Gestion.pendiente);
        current.setFechaGestion(Calendar.getInstance().getTime());
        current.setIdGestion(JsfUtil.generarID("G"));
        usuario = current.getUsuario();
        if(usuario.getGestionList()==null){
            usuario.setGestionList(new ArrayList<Gestion>());
        }
        usuario.getGestionList().add(current);
        ejbUsuario.edit(usuario);
//        JsfUtil.cerrarDialogo(current);
    }

    public Gestion getSelected() {
        if (current == null) {
            current = new Gestion();
            selectedItemIndex = -1;
        }
        return current;
    }

    private GestionFacade getFacade() {
        return ejbFacade;
    }

    public PaginationHelper getPagination() {
        if (pagination == null) {
            pagination = new PaginationHelper(10) {
                @Override
                public int getItemsCount() {
                    return getFacade().count();
                }

                @Override
                public DataModel createPageDataModel() {
                    return new ListDataModel(getFacade().findRange(new int[]{getPageFirstItem(), getPageFirstItem() + getPageSize()}));
                }
            };
        }
        return pagination;
    }

    public String prepareList() {
        recreateModel();
        return "List";
    }

    public String prepareView() {
        current = (Gestion) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        return "View";
    }

    public String prepareCreate() {
        current = new Gestion();
        selectedItemIndex = -1;
        return "/gestion/Create?faces-redirect=true";
    }
    
    public void preparar(){
        current = new Gestion();
//        JsfUtil.abrirDialogo("dlg");
    }

    public String create() {
        try {
            getFacade().create(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/Bundle").getString("GestionCreated"));
//            preparar();
            current = null;
            return null;
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
            return null;
        }
    }

    public String prepareEdit() {
        current = (Gestion) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        return "Edit";
    }

    public String update() {
        try {
            getFacade().edit(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/Bundle").getString("GestionUpdated"));
            return "/inicio?faces-redirect=true";
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
            return null;
        }
    }

    public String destroy() {
        current = (Gestion) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        performDestroy();
        recreatePagination();
        recreateModel();
        return "List";
    }

    public String destroyAndView() {
        performDestroy();
        recreateModel();
        updateCurrentItem();
        if (selectedItemIndex >= 0) {
            return "View";
        } else {
            // all items were removed - go back to list
            recreateModel();
            return "List";
        }
    }

    private void performDestroy() {
        try {
            getFacade().remove(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/Bundle").getString("GestionDeleted"));
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
        }
    }

    private void updateCurrentItem() {
        int count = getFacade().count();
        if (selectedItemIndex >= count) {
            // selected index cannot be bigger than number of items:
            selectedItemIndex = count - 1;
            // go to previous page if last page disappeared:
            if (pagination.getPageFirstItem() >= count) {
                pagination.previousPage();
            }
        }
        if (selectedItemIndex >= 0) {
            current = getFacade().findRange(new int[]{selectedItemIndex, selectedItemIndex + 1}).get(0);
        }
    }

    public DataModel getItems() {
        if (items == null) {
            items = getPagination().createPageDataModel();
        }
        return items;
    }

    private void recreateModel() {
        items = null;
    }

    private void recreatePagination() {
        pagination = null;
    }

    public String next() {
        getPagination().nextPage();
        recreateModel();
        return "List";
    }

    public String previous() {
        getPagination().previousPage();
        recreateModel();
        return "List";
    }

    public SelectItem[] getItemsAvailableSelectMany() {
        return JsfUtil.getSelectItems(ejbFacade.findAll(), false);
    }

    public SelectItem[] getItemsAvailableSelectOne() {
        return JsfUtil.getSelectItems(ejbFacade.findAll(), true);
    }

    public Gestion getGestion(java.lang.String id) {
        return ejbFacade.find(id);
    }

    public Factura getFactura() {
        return factura;
    }

    public void setFactura(Factura factura) {
        this.factura = factura;
    }

    public void setSelected(Gestion current) {
        this.current = current;
    }

    public cubisoft.carycob.ejb.UsuarioFacade getEjbUsuario() {
        return ejbUsuario;
    }

    public void setEjbUsuario(cubisoft.carycob.ejb.UsuarioFacade ejbUsuario) {
        this.ejbUsuario = ejbUsuario;
    }

    @FacesConverter(forClass = Gestion.class)
    public static class GestionControllerConverter implements Converter {

        @Override
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            GestionController controller = (GestionController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "gestionController");
            return controller.getGestion(getKey(value));
        }

        java.lang.String getKey(String value) {
            java.lang.String key;
            key = value;
            return key;
        }

        String getStringKey(java.lang.String value) {
            StringBuilder sb = new StringBuilder();
            sb.append(value);
            return sb.toString();
        }

        @Override
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof Gestion) {
                Gestion o = (Gestion) object;
                return getStringKey(o.getIdGestion());
            } else {
                throw new IllegalArgumentException("object " + object + " is of type " + object.getClass().getName() + "; expected type: " + Gestion.class.getName());
            }
        }
    }
}
