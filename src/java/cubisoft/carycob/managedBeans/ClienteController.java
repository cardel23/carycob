package cubisoft.carycob.managedBeans;

import cubisoft.carycob.entidades.Cliente;
import cubisoft.carycob.managedBeans.util.JsfUtil;
import cubisoft.carycob.managedBeans.util.PaginationHelper;
import cubisoft.carycob.ejb.ClienteFacade;
import cubisoft.carycob.ejb.CuentaClienteFacade;
import cubisoft.carycob.ejb.SaldoFacturaFacade;
import cubisoft.carycob.vistas.CuentaCliente;
import cubisoft.carycob.entidades.Subclasificacion;
import cubisoft.carycob.excepciones.NoClienteException;
import cubisoft.carycob.managedBeans.util.Cuenta;
import cubisoft.carycob.managedBeans.util.ProcesadorExcel;
import cubisoft.carycob.managedBeans.util.RegistroCliente;
import cubisoft.carycob.managedBeans.util.reportes.PDFController;
import cubisoft.carycob.managedBeans.util.reportes.ReportesUtil;
import cubisoft.carycob.vistas.GestionesCliente;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.ResourceBundle;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpServletResponse;
import net.sf.jasperreports.engine.JRException;
import org.primefaces.event.FileUploadEvent;

@ManagedBean(name = "clienteController")
@SessionScoped
public class ClienteController implements Serializable {

    private Cliente current;
    private DataModel items = null;
    @EJB
    private cubisoft.carycob.ejb.ClienteFacade ejbFacade;
    @EJB
    private cubisoft.carycob.ejb.SubclasificacionFacade ejbSubclasificacion;
    @EJB
    private CuentaClienteFacade ejbCuentaCliente;
    @EJB
    private SaldoFacturaFacade facturaFacade;
    private PaginationHelper pagination;
    private int selectedItemIndex;
    
    private Subclasificacion subclasificacion;
    private CuentaCliente c;
    
    private boolean adjunto = false;
    private boolean activarGuardar = false;
    private double progresoBarra = 0;
    private List<RegistroCliente> listaClientes;
    
    @ManagedProperty(value = "#{pdfMB}")
    private PDFController pdfMB;
    @ManagedProperty(value = "#{emailSenderController}")
    private EmailSenderController emailMB;

    public ClienteController() {
    }
    
    
    /**
     * Listener para agregar los clientes desde excel a la BD
     * @param event
     * @return la URL redirigiendo a la lista de clientes
     * @throws IOException si se produce error de lectura del archivo excel
     * @autor Carlos Delgadillo
     * @since 04/06/2014
     */
    public void guardarClientes(){
        try {
            int contador=1;
            
            
            for(RegistroCliente registro: listaClientes)
            {
                try{
                    ejbFacade.create(registroCliente(registro));
                    progresoBarra = (contador/listaClientes.size())*100;
                    System.out.print("Cliente procesado " +contador);
                    contador ++;
                }
                catch(Exception e){
                    progresoBarra = (contador/listaClientes.size())*100;
                    System.out.print("Cliente no procesado " +contador);
                    contador ++;
                    e.getMessage();
                }
            }
            recreatePagination();
            recreateModel();
            adjunto = false;
            progresoBarra = 0;
            listaClientes = null;
            activarGuardar = false;
            HttpServletResponse respuesta = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();
            respuesta.sendRedirect("/cliente/List.xhtml");
            JsfUtil.addSuccessMessage("Se agregaron nuevos clientes a la base de datos");
            
//            return "/cliente/List?faces-redirect=true";
        } catch (Exception e) {
            adjunto = false;
            JsfUtil.addErrorMessage("No se pudo realizar la operación", e.getLocalizedMessage());
//            return null;
        }
    }
    
    public void agregarClientes(FileUploadEvent event) throws IOException{
        listaClientes = ProcesadorExcel.leerClientes(event.getFile());
        activarGuardar = true;
        JsfUtil.addSuccessMessage("Se agregarán "+listaClientes.size()+" clientes");
    }
    
    /**
     * Construye un cliente a partir de un registro procesado
     * @param registroCliente
     * @return el objeto cliente a guardar en la BD
     * @autor Carlos Delgadillo
     * @since 04/06/2014
     */
    public Cliente registroCliente(RegistroCliente registroCliente){
        current = new Cliente();
        current.setIdCliente(registroCliente.getIdCliente());
        current.setNombre(registroCliente.getNombre());
        current.setCorreo(registroCliente.getCorreo());
        for(String string : registroCliente.getSubclasificaciones()){
            subclasificacion = ejbSubclasificacion.findByDescripcion(string);
            if(subclasificacion!= null){
               if(current.getSubclasificacionList() == null)
                   current.setSubclasificacionList(new ArrayList<Subclasificacion>());
               current.getSubclasificacionList().add(subclasificacion);
            }
        }
        return current;
    }
    
    /**
     * Agrega una subclasificacion al cliente
     * @autor: Carlos Delgadillo
     * @fecha: 07/04/2014
     */
    public void agregarSubclasificacion(){
        if(current.getSubclasificacionList()== null)
            current.setSubclasificacionList(new ArrayList<Subclasificacion>());
        if(subclasificacion!=null)
            current.getSubclasificacionList().add(subclasificacion);
    }
    
    /**
     * Elimina una subclasficacion del cliente
     * @autor: Carlos Delgadillo
     * @fecha: 07/04/2014
     */
    public void eliminarSubclasificacion(){
        current.getSubclasificacionList().remove(subclasificacion);
    }
    
    /**
     * Elimina todas las subclasificaciones del cliente
     * @autor: Carlos Delgadillo
     * @fecha: 07/04/2014
     */
    public void eliminarSubclasificaciones(){
        current.getSubclasificacionList().clear();
    }
    
    /**
     * Obtiene la cuenta del cliente
     * @return la vista mapeada que genera los datos de la cuenta
     */
    public CuentaCliente getCuentaCliente(){
        if(c==null){
            c= new CuentaCliente();
        }
        c = ejbFacade.obtenerCuenta(current.getIdCliente());
        return c;
    }
    
    public void actualizarCuenta(){
        c = ejbFacade.obtenerCuenta(current.getIdCliente());
    }
    
    public void enviarMail() throws JRException{
        Cuenta cuenta = new Cuenta();
        cuenta.setCliente(current);
        cuenta.setCuentaCliente(getCuentaCliente());
        cuenta.setListaFacturas(facturaFacade.listaFacturasCliente(current.getIdCliente(),pdfMB.getFechaInicio(),pdfMB.getFechaFin()));
        List<Cuenta> lista = new ArrayList<>();
        lista.add(cuenta);
        String ruta = "/Reportes/EstadoDeCuenta.jasper";
        String nombreReporte = ReportesUtil.nombreReporte(current.getNombre(), new Date(System.currentTimeMillis()));
        byte[] pdf = pdfMB.pdf(lista, new HashMap(), ruta, nombreReporte);
        emailMB.sendEmail(cuenta.getCuentaCliente(), pdf, adjunto, nombreReporte);
    }
    
    public void getEstadoDeCuentaPDF(boolean impresion) throws JRException, IOException{
        Cuenta cuenta = new Cuenta();
        cuenta.setCliente(current);
        cuenta.setCuentaCliente(getCuentaCliente());
        cuenta.setListaFacturas(facturaFacade.listaFacturasCliente(current.getIdCliente(),pdfMB.getFechaInicio(),pdfMB.getFechaFin()));
        List<Cuenta> lista = new ArrayList<>();
        lista.add(cuenta);
        String ruta = "/Reportes/EstadoDeCuenta.jasper";
        String nombreReporte = ReportesUtil.nombreReporte(current.getNombre(), new Date(System.currentTimeMillis()));
        pdfMB.reporteGenerico(lista, new HashMap(), ruta, nombreReporte, impresion);
        
    }
    
    public List<GestionesCliente> getListaGestiones(){
        return getFacade().listaGestiones(current.getIdCliente());
    }
    public List<Cliente> obtenerClientes(){
        return getFacade().findAll();
    }
    public Cliente getSelected() {
        if (current == null) {
            current = new Cliente();
            selectedItemIndex = -1;
        }
        return current;
    }
    
    public void setSelected(Cliente cliente){
        this.current=cliente;
    }

    private ClienteFacade getFacade() {
        return ejbFacade;
    }

    public PaginationHelper getPagination() {
        if (pagination == null) {
            pagination = new PaginationHelper(10) {
                @Override
                public int getItemsCount() {
                    return getFacade().count();
                }

                @Override
                public DataModel createPageDataModel() {
                    return new ListDataModel(getFacade().findAll());
                }
            };
        }
        return pagination;
    }

    public String prepareList() {
        recreateModel();
        return "List?faces-redirect=true";
    }

    public String prepareView() throws NoClienteException {
        current = ejbFacade.findByNombre(((Cliente) getItems().getRowData()).getNombre());
//        current.setFacturaList(facturaFacade.getListaFacturas(current));
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        return "DetalleCliente?faces-redirect=true";
    }

    public String prepareCreate() {
        current = new Cliente();
        selectedItemIndex = -1;
        return "Create?faces-redirect=true";
    }

    public String create() {
        try {
            getFacade().create(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/Bundle").getString("ClienteCreated"));
            return prepareCreate();
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
            return null;
        }
    }

    public String prepareEdit() {
        current = (Cliente) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        return "Edit?faces-redirect=true";
    }

    public String update() {
        try {
            getFacade().edit(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/Bundle").getString("ClienteUpdated"));
            return "DetalleCliente?faces-redirect=true";
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
            return null;
        }
    }
    
    public String eliminarCliente(){
        performDestroy();
        recreatePagination();
        recreateModel();
        return "List?faces-redirect=true";
    }

    public String destroy() {
        current = (Cliente) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        performDestroy();
        recreatePagination();
        recreateModel();
        return "List?faces-redirect=true";
    }

    public String destroyAndView() {
        performDestroy();
        recreateModel();
        updateCurrentItem();
        if (selectedItemIndex >= 0) {
            return "DetalleCliente?faces-redirect=true";
        } else {
            // all items were removed - go back to list
            recreateModel();
            return "List?faces-redirect=true";
        }
    }

    private void performDestroy() {
        try {
            getFacade().remove(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/Bundle").getString("ClienteDeleted"));
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
        }
    }

    private void updateCurrentItem() {
        int count = getFacade().count();
        if (selectedItemIndex >= count) {
            // selected index cannot be bigger than number of items:
            selectedItemIndex = count - 1;
            // go to previous page if last page disappeared:
            if (pagination.getPageFirstItem() >= count) {
                pagination.previousPage();
            }
        }
        if (selectedItemIndex >= 0) {
            current = getFacade().findRange(new int[]{selectedItemIndex, selectedItemIndex + 1}).get(0);
        }
    }

    public DataModel getItems() {
        try{
        if (items == null) {
            items = getPagination().createPageDataModel();
        }
        return items;}
        catch (Exception e){
            e.getMessage();
            return null;
        }
    }

    private void recreateModel() {
        items = null;
    }

    private void recreatePagination() {
        pagination = null;
    }

    public String next() {
        getPagination().nextPage();
        recreateModel();
        return "List?faces-redirect=true";
    }

    public String previous() {
        getPagination().previousPage();
        recreateModel();
        return "List?faces-redirect=true";
    }

    public SelectItem[] getItemsAvailableSelectMany() {
        return JsfUtil.getSelectItems(ejbFacade.findAll(), false);
    }

    public SelectItem[] getItemsAvailableSelectOne() {
        return JsfUtil.getSelectItems(ejbFacade.findAll(), true);
    }

    public Cliente getCliente(java.lang.String id) {
        return ejbFacade.find(id);
    }

    public Subclasificacion getSubclasificacion() {
        return subclasificacion;
    }

    public void setSubclasificacion(Subclasificacion subclasificacion) {
        this.subclasificacion = subclasificacion;
    }

    public CuentaCliente getCuenta() {
        if(c==null){
            c= new CuentaCliente();
        }
        return c;
    }

    public PDFController getPdfMB() {
        return pdfMB;
    }

    public void setPdfMB(PDFController pdfMB) {
        this.pdfMB = pdfMB;
    }

    public cubisoft.carycob.ejb.SubclasificacionFacade getEjbSubclasificacion() {
        return ejbSubclasificacion;
    }

    public void setEjbSubclasificacion(cubisoft.carycob.ejb.SubclasificacionFacade ejbSubclasificacion) {
        this.ejbSubclasificacion = ejbSubclasificacion;
    }

    public boolean isAdjunto() {
        return adjunto;
    }

    public void setAdjunto(boolean adjunto) {
        this.adjunto = adjunto;
    }

    public double getProgresoBarra() {
        return progresoBarra;
    }

    public void setProgresoBarra(double progresoBarra) {
        this.progresoBarra = progresoBarra;
    }

    public List<RegistroCliente> getListaClientes() {
        return listaClientes;
    }

    public void setListaClientes(List<RegistroCliente> listaClientes) {
        this.listaClientes = listaClientes;
    }

    public EmailSenderController getEmailMB() {
        return emailMB;
    }

    public void setEmailMB(EmailSenderController emailMB) {
        this.emailMB = emailMB;
    }

    public boolean isActivarGuardar() {
        return activarGuardar;
    }

    public void setActivarGuardar(boolean activarGuardar) {
        this.activarGuardar = activarGuardar;
    }

    @FacesConverter(forClass = Cliente.class)
    public static class ClienteControllerConverter implements Converter {

        @Override
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            ClienteController controller = (ClienteController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "clienteController");
            return controller.getCliente(getKey(value));
        }

        java.lang.String getKey(String value) {
            java.lang.String key;
            key = value;
            return key;
        }

        String getStringKey(java.lang.String value) {
            StringBuilder sb = new StringBuilder();
            sb.append(value);
            return sb.toString();
        }

        @Override
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof Cliente) {
                Cliente o = (Cliente) object;
                return getStringKey(o.getIdCliente());
            } else {
                throw new IllegalArgumentException("object " + object + " is of type " + object.getClass().getName() + "; expected type: " + Cliente.class.getName());
            }
        }
    }
}
