package cubisoft.carycob.managedBeans;

import cubisoft.carycob.entidades.Factura;
import cubisoft.carycob.managedBeans.util.JsfUtil;
import cubisoft.carycob.managedBeans.util.PaginationHelper;
import cubisoft.carycob.ejb.FacturaFacade;
import cubisoft.carycob.entidades.Carga;
import cubisoft.carycob.entidades.Cliente;
import cubisoft.carycob.entidades.Nota;
import cubisoft.carycob.vistas.DetalleFactura;
import cubisoft.carycob.entidades.Pago;
import cubisoft.carycob.entidades.Usuario;
import cubisoft.carycob.excepciones.PagoException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import javax.faces.model.SelectItem;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.ScheduleEvent;

@ManagedBean(name = "facturaController")
@SessionScoped
public class FacturaController implements Serializable {

    private Factura current;
    private DataModel items = null;
    @EJB
    private cubisoft.carycob.ejb.FacturaFacade ejbFacade;
    private PaginationHelper pagination;
    private int selectedItemIndex;
    
    private Usuario usuario;
    
    private FacturaControllerConverter convertidor;
    
    private Pago pago;
    @EJB
    private cubisoft.carycob.ejb.UsuarioFacade ejbUsuario;
    @EJB
    private cubisoft.carycob.ejb.PagoFacade ejbPago;
    @EJB
    private cubisoft.carycob.ejb.SaldoFacturaFacade ejbSaldo;

    public FacturaController() {
    }
    
    public void cambiarEstadoFactura(){
        if(current.getEstado().equals(Factura.pendiente) || current.getEstado().equals(Factura.vencida))
            if(ejbSaldo.saldo(current)== 0){
                current.setEstado(Factura.cancelada);
            }
        if(!current.getEstado().equals(Factura.pendiente) || !current.getEstado().equals(Factura.vencida))
            if(ejbSaldo.saldo(current) != 0){
                if(ejbFacade.isFacturaVencida(current))
                    current.setEstado(Factura.vencida);
                else
                    current.setEstado(Factura.pendiente);
            }
        ejbFacade.edit(current);
    }
    
    public List<Factura> getFacturasCliente(Cliente cliente){
        return ejbFacade.facturasPorCliente(cliente);
    }
    public List<DetalleFactura> obtenerDetalle(Factura factura){
        return getFacade().getDetalleFactura(factura.getIdFactura());
    }
    
    public void agregarFactura(){
        current.setCarga(new Carga(JsfUtil.generarID(JsfUtil.carga), new Date()));
        current.getCarga().setDescripcion(Carga.cargaSimpleFactura);
        current.getCarga().setUsuario(usuario);//corregir
        try {
            ejbFacade.create(current);
            JsfUtil.addSuccessMessage("Factura guardada con éxito");
            current = new Factura();
        } catch (Exception e) {
            JsfUtil.addErrorMessage("No se registró la factura");
        }
    }
    
    /**
     * Agrega un pago a la factura
     * @autor: Carlos Delgadillo
     * @fecha: 07/04/2014
     */
    public void agregarPago(){
        if(current.getPagoList() == null)
            current.setPagoList(new ArrayList<Pago>());
        if(getPago() != null){
            pago.setCarga(new Carga(JsfUtil.generarID(JsfUtil.carga), getPago().getFechaEmision()));
            pago.getCarga().setUsuario(usuario);
            pago.getCarga().setDescripcion(Carga.cargaSimplePago);
            pago.setFactura(current);
            pago.setIdPago(JsfUtil.generarID(JsfUtil.pago));
            current.getPagoList().add(pago);
            try {
                if(pago.getMonto()>saldoFactura(current))
                    throw new PagoException("El pago no puede ser mayor que el saldo de la factura");
                ejbFacade.edit(current);
                JsfUtil.addSuccessMessage("Se ha agregado un pago por C$"+pago.getMonto()+" a la factura "+current.getIdFactura());
            }
            catch(PagoException e){
                JsfUtil.addErrorMessage(e.getMessage());
            }
            catch (Exception e) {
                e.getCause();
            }
            destruirPago();
        }
    }
    
    
    /**
     * Elimina un pago de la factura
     * @autor: Carlos Delgadillo
     * @fecha: 07/04/2014
     */
    public void eliminarPago(){
        current.getPagoList().remove(getPago());
        ejbPago.remove(getPago());
        ejbFacade.edit(current);
        destruirPago();
    }
    
    private void destruirPago(){
        pago = null;
    }
    
    public void anularFactura(){
        if(current.getNotaList() == null)
            current.setNotaList(new ArrayList<Nota>());
        current.getNotaList().add(crearNotaCredito());
        
        current.setEstado(Factura.anulada);
        try{
            ejbFacade.edit(current);
        }catch(Exception e){
            JsfUtil.addErrorMessage("Error al anular la factura");
        }
    }
    
    public Nota crearNotaCredito(){
        Nota n = new Nota(JsfUtil.generarID(JsfUtil.notaCredito));
        n.setCarga(new Carga(JsfUtil.generarID(JsfUtil.carga), new Date()));
        n.setFecha(new Date());
        n.setMonto(current.getMonto());
        n.setFactura(current);
        n.setObservaciones("Anulación de factura");
        n.getCarga().setDescripcion(Carga.cargaSimpleNotaCredito);
        n.getCarga().setUsuario(usuario);
        n.setTipoNota(Nota.CREDITO);
        
        return n;
    }
    
    public double saldoFactura(Factura f){
        return ejbSaldo.saldo(f);
    }
    
    public double importe(Factura f){
        return ejbSaldo.importe(f);
    }
    
    public String verFactura(SelectEvent eventoSeleccionado){
        ScheduleEvent event = (ScheduleEvent)eventoSeleccionado.getObject();
        current = (Factura) event.getData();
        return "/factura/View?faces-redirect=true";
    }

    
    public Factura getSelected() {
        if (current == null) {
            current = new Factura();
            selectedItemIndex = -1;
        }
        return current;
    }
    
    public void setSelected(Factura factura){
        current = factura;
    }

    private FacturaFacade getFacade() {
        return ejbFacade;
    }

    public PaginationHelper getPagination() {
        if (pagination == null) {
            pagination = new PaginationHelper(10) {
                @Override
                public int getItemsCount() {
                    return getFacade().count();
                }

                @Override
                public DataModel createPageDataModel() {
                    return new ListDataModel(getFacade().findRange(new int[]{getPageFirstItem(), getPageFirstItem() + getPageSize()}));
                }
            };
        }
        return pagination;
    }

    public String prepareList() {
        recreateModel();
        return "List";
    }

    public String prepareView() {
        current = (Factura) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        return "View";
    }

    public String prepareCreate() {
        current = new Factura();
        selectedItemIndex = -1;
        return "Create";
    }

    public String create() {
        try {
            getFacade().create(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/Bundle").getString("FacturaCreated"));
            return prepareCreate();
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
            return null;
        }
    }

    public String prepareEdit() {
        current = (Factura) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        return "Edit";
    }

    public String update() {
        try {
            getFacade().edit(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/Bundle").getString("FacturaUpdated"));
            return "View";
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
            return null;
        }
    }

    public String destroy() {
//        current = (Factura) getItems().getRowData();
//        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        performDestroy();
        recreatePagination();
        recreateModel();
        return "DetalleCliente";
    }

    public String destroyAndView() {
        performDestroy();
        recreateModel();
        updateCurrentItem();
        if (selectedItemIndex >= 0) {
            return "View";
        } else {
            // all items were removed - go back to list
            recreateModel();
            return "List";
        }
    }

    private void performDestroy() {
        try {
            getFacade().remove(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/Bundle").getString("FacturaDeleted"));
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
        }
    }

    private void updateCurrentItem() {
        int count = getFacade().count();
        if (selectedItemIndex >= count) {
            // selected index cannot be bigger than number of items:
            selectedItemIndex = count - 1;
            // go to previous page if last page disappeared:
            if (pagination.getPageFirstItem() >= count) {
                pagination.previousPage();
            }
        }
        if (selectedItemIndex >= 0) {
            current = getFacade().findRange(new int[]{selectedItemIndex, selectedItemIndex + 1}).get(0);
        }
    }

    public DataModel getItems() {
        if (items == null) {
            items = getPagination().createPageDataModel();
        }
        return items;
    }

    private void recreateModel() {
        items = null;
    }

    private void recreatePagination() {
        pagination = null;
    }

    public String next() {
        getPagination().nextPage();
        recreateModel();
        return "List";
    }

    public String previous() {
        getPagination().previousPage();
        recreateModel();
        return "List";
    }

    public SelectItem[] getItemsAvailableSelectMany() {
        return JsfUtil.getSelectItems(ejbFacade.findAll(), false);
    }

    public SelectItem[] getItemsAvailableSelectOne() {
        return JsfUtil.getSelectItems(ejbFacade.findAll(), true);
    }
    
    public List<Factura> getFacturas(){
        return ejbFacade.findAll();
    }
    
    public void check(Factura factura){
        setSelected(factura);
        getSelected();
        return;
    }

    public Factura getFactura(java.lang.String id) {
        return ejbFacade.find(id);
    }

    public Pago getPago() {
        if(pago == null)
            pago = new Pago();
        return pago;
    }

    public void setPago(Pago pago) {
        this.pago = pago;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public FacturaControllerConverter getConvertidor() {
        if(convertidor == null)
            convertidor = new FacturaControllerConverter();
        return convertidor;
    }

    public void setConvertidor(FacturaControllerConverter convertidor) {
        this.convertidor = convertidor;
    }
    
    @FacesConverter(forClass = Factura.class)
    public static class FacturaControllerConverter implements Converter {

        @Override
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            FacturaController controller = (FacturaController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "facturaController");
            return controller.getFactura(getKey(value));
        }

        java.lang.String getKey(String value) {
            java.lang.String key;
            key = value;
            return key;
        }

        String getStringKey(java.lang.String value) {
            StringBuilder sb = new StringBuilder();
            sb.append(value);
            return sb.toString();
        }

        @Override
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof Factura) {
                Factura o = (Factura) object;
                return getStringKey(o.getIdFactura());
            } else {
                throw new IllegalArgumentException("object " + object + " is of type " + object.getClass().getName() + "; expected type: " + Factura.class.getName());
            }
        }
    }
}
