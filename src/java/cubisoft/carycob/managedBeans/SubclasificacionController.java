package cubisoft.carycob.managedBeans;

import cubisoft.carycob.entidades.Subclasificacion;
import cubisoft.carycob.managedBeans.util.JsfUtil;
import cubisoft.carycob.managedBeans.util.PaginationHelper;
import cubisoft.carycob.ejb.SubclasificacionFacade;
import cubisoft.carycob.entidades.Clasificacion;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import javax.faces.model.SelectItem;

@ManagedBean(name = "subclasificacionController")
@SessionScoped
public class SubclasificacionController implements Serializable {

    private Subclasificacion current;
    private DataModel items = null;
    @EJB
    private cubisoft.carycob.ejb.SubclasificacionFacade ejbFacade;
    private PaginationHelper pagination;
    private int selectedItemIndex;
    
    private Clasificacion clasificacion;
    private List<Subclasificacion> subclasificaciones;
    private SubclasificacionControllerConverter convertidor;

    public SubclasificacionController() {
    }
    
    public void actualizarSubclasificaciones(){
        if(clasificacion!= null)
            setSubclasificaciones((ArrayList<Subclasificacion>)getFacade().subclasificaciones(clasificacion));
        else
            setSubclasificaciones(new ArrayList<Subclasificacion>());
    }

    public Subclasificacion getSelected() {
        if (current == null) {
            current = new Subclasificacion();
            selectedItemIndex = -1;
        }
        return current;
    }

    private SubclasificacionFacade getFacade() {
        return ejbFacade;
    }

    public PaginationHelper getPagination() {
        if (pagination == null) {
            pagination = new PaginationHelper(10) {
                @Override
                public int getItemsCount() {
                    return getFacade().count();
                }

                @Override
                public DataModel createPageDataModel() {
                    return new ListDataModel(getFacade().findRange(new int[]{getPageFirstItem(), getPageFirstItem() + getPageSize()}));
                }
            };
        }
        return pagination;
    }

    public String prepareList() {
        recreateModel();
        return "List";
    }

    public String prepareView() {
        current = (Subclasificacion) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        return "View";
    }

    public String prepareCreate() {
        current = new Subclasificacion();
        selectedItemIndex = -1;
        return "Create";
    }
    
    public Subclasificacion getNuevo(){
        return new Subclasificacion();
    }
    
    public List<Clasificacion> listclasificacion(){
        List<Clasificacion> lista = new ArrayList<>();
        lista.add(clasificacion);
        return lista;
    }

    public String create() {
        current.setClasificacion(clasificacion);
        try {
            getFacade().create(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/Bundle").getString("SubclasificacionCreated"));
            return prepareCreate();
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
            return null;
        }
    }

    public String prepareEdit() {
        current = (Subclasificacion) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        return "Edit";
    }

    public String update() {
        try {
            getFacade().edit(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/Bundle").getString("SubclasificacionUpdated"));
            return "View";
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
            return null;
        }
    }

    public String destroy() {
        current = (Subclasificacion) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        performDestroy();
        recreatePagination();
        recreateModel();
        return "List";
    }

    public String destroyAndView() {
        performDestroy();
        recreateModel();
        updateCurrentItem();
        if (selectedItemIndex >= 0) {
            return "View";
        } else {
            // all items were removed - go back to list
            recreateModel();
            return "List";
        }
    }

    private void performDestroy() {
        try {
            getFacade().remove(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/Bundle").getString("SubclasificacionDeleted"));
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
        }
    }

    private void updateCurrentItem() {
        int count = getFacade().count();
        if (selectedItemIndex >= count) {
            // selected index cannot be bigger than number of items:
            selectedItemIndex = count - 1;
            // go to previous page if last page disappeared:
            if (pagination.getPageFirstItem() >= count) {
                pagination.previousPage();
            }
        }
        if (selectedItemIndex >= 0) {
            current = getFacade().findRange(new int[]{selectedItemIndex, selectedItemIndex + 1}).get(0);
        }
    }

    public DataModel getItems() {
        if (items == null) {
            items = getPagination().createPageDataModel();
        }
        return items;
    }

    private void recreateModel() {
        items = null;
    }

    private void recreatePagination() {
        pagination = null;
    }

    public String next() {
        getPagination().nextPage();
        recreateModel();
        return "List";
    }

    public String previous() {
        getPagination().previousPage();
        recreateModel();
        return "List";
    }

    public SelectItem[] getItemsAvailableSelectMany() {
        return JsfUtil.getSelectItems(ejbFacade.findAll(), false);
    }

    public SelectItem[] getItemsAvailableSelectOne() {
        return JsfUtil.getSelectItems(ejbFacade.findAll(), true);
    }

    public Subclasificacion getSubclasificacion(java.lang.String id) {
        return ejbFacade.find(id);
    }

    public Clasificacion getClasificacion() {
        return clasificacion;
    }

    public void setClasificacion(Clasificacion clasificacion) {
        this.clasificacion = clasificacion;
    }

    public List<Subclasificacion> getSubclasificaciones() {
        return subclasificaciones;
    }

    public void setSubclasificaciones(List<Subclasificacion> subclasificaciones) {
        this.subclasificaciones = subclasificaciones;
    }

    public SubclasificacionControllerConverter getConvertidor() {
        if(convertidor==null)
            convertidor = new SubclasificacionControllerConverter();
        return convertidor;
    }

    public void setConvertidor(SubclasificacionControllerConverter convertidor) {
        this.convertidor = convertidor;
    }

    public void setSelected(Subclasificacion current) {
        this.current = current;
    }

    @FacesConverter(forClass = Subclasificacion.class)
    public static class SubclasificacionControllerConverter implements Converter {

        @Override
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            SubclasificacionController controller = (SubclasificacionController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "subclasificacionController");
            return controller.getSubclasificacion(getKey(value));
        }

        java.lang.String getKey(String value) {
            java.lang.String key;
            key = value;
            return key;
        }

        String getStringKey(java.lang.String value) {
            StringBuilder sb = new StringBuilder();
            sb.append(value);
            return sb.toString();
        }

        @Override
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof Subclasificacion) {
                Subclasificacion o = (Subclasificacion) object;
                return getStringKey(o.getIdSubClasificacion());
            } else {
                throw new IllegalArgumentException("object " + object + " is of type " + object.getClass().getName() + "; expected type: " + Subclasificacion.class.getName());
            }
        }
    }
}
