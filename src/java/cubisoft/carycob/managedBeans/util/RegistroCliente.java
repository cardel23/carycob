/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cubisoft.carycob.managedBeans.util;

import java.util.List;

/**
 * Registro de cliente para procesamiento
 * @author Carlos Delgadillo
 */
public class RegistroCliente {
    
    private String idCliente;
    private String nombre;
    private String correo;
    private List<String> subclasificaciones;
    
    public RegistroCliente(){}

    public String getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(String idCliente) {
        this.idCliente = idCliente;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public List<String> getSubclasificaciones() {
        return subclasificaciones;
    }

    public void setSubclasificaciones(List<String> subclasificaciones) {
        this.subclasificaciones = subclasificaciones;
    }
    
    
}
