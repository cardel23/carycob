package cubisoft.carycob.managedBeans.util;

import cubisoft.carycob.ejb.FacturaFacade;
import java.io.IOException;
import java.util.List;
import java.util.UUID;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.primefaces.context.RequestContext;

public class JsfUtil {
    
    public static String cliente = "CL",
                         factura = "F",
                         carga = "C",
                         pago = "P",
                         rechazo = "R",
                         notaDebito = "ND",
                         notaCredito = "NC",
                         vendedor = "V";
    
    @EJB
    private FacturaFacade facturaFacade;
    public static SelectItem[] getSelectItems(List<?> entities, boolean selectOne) {
        int size = selectOne ? entities.size() + 1 : entities.size();
        SelectItem[] items = new SelectItem[size];
        int i = 0;
        if (selectOne) {
            items[0] = new SelectItem("", "---");
            i++;
        }
        for (Object x : entities) {
            items[i++] = new SelectItem(x, x.toString());
        }
        return items;
    }

    public static void addErrorMessage(Exception ex, String defaultMsg) {
        String msg = ex.getLocalizedMessage();
        if (msg != null && msg.length() > 0) {
            addErrorMessage(msg);
        } else {
            addErrorMessage(defaultMsg);
        }
    }

    public static void addErrorMessages(List<String> messages) {
        for (String message : messages) {
            addErrorMessage(message);
        }
    }

    public static void addErrorMessage(String msg) {
        FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_ERROR, msg, msg);
        FacesContext.getCurrentInstance().addMessage(null, facesMsg);
    }
    public static void addErrorMessage(String msg, String alerta) {
        FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_ERROR, msg, alerta);
        FacesContext.getCurrentInstance().addMessage(null, facesMsg);
    }

    public static void addSuccessMessage(String msg) {
        FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_INFO, msg, msg);
        FacesContext.getCurrentInstance().addMessage("successInfo", facesMsg);
    }
    
    public static void addAlertMessage(String msg){
        FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_WARN, msg, msg);
        FacesContext.getCurrentInstance().addMessage(null, facesMsg);
    }

    public static String getRequestParameter(String key) {
        return FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get(key);
    }

    public static Object getObjectFromRequestParameter(String requestParameterName, Converter converter, UIComponent component) {
        String theId = JsfUtil.getRequestParameter(requestParameterName);
        return converter.getAsObject(FacesContext.getCurrentInstance(), component, theId);
    }
    
    /**
     * Crea un id único para la clave primaria de una entidad
     * @autor Carlos Delgadillo
     * @since 13/03/2014
     * @param codigoEntidad define el sufijo que se agregará al id
     * @return el identificador único
     */
    public static String generarID(String codigoEntidad){
        StringBuilder st = new StringBuilder(codigoEntidad);
        st.append(UUID.randomUUID().toString().toUpperCase().substring(0, 8));
        return st.toString();
    }
    
    public static void respuesta(String ruta) throws IOException{
        HttpServletResponse response = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();
        response.sendRedirect(ruta);
    }
    
    public static void abrirDialogo(String dialogo){
        RequestContext.getCurrentInstance().openDialog(dialogo);
    }
    
    public static void cerrarDialogo(Object o){
        RequestContext.getCurrentInstance().closeDialog(o);
    }
}