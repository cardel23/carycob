/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cubisoft.carycob.managedBeans.util.reportes;

import java.io.IOException;
import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import net.sf.jasperreports.engine.JRException;

/**
 * Controlador de reportes en formato PDF
 * @author Carlos Delgadillo
 * @since 05/05/2014
 */
@ManagedBean(name = "pdfMB")
@SessionScoped
public class PDFController<T> implements Serializable {
    
    private static ReportesUtil ru = new ReportesUtil();
    private Date fechaInicio, fechaFin;
    private boolean marcadorFechas = false;
    /**
     * Crea un reporte genérico en formato PDF
     * @param lista
     * @param p
     * @param ruta
     * @param nombreReporte
     * @param impresion
     * @throws JRException
     * @throws IOException 
     */
    public void reporteGenerico(List<T> lista, HashMap p, String ruta, String nombreReporte, boolean impresion) throws JRException, IOException{
        ru.setLista(lista);
        ru.setParametros(p);
        ru.setNombreReporte(nombreReporte);
        ru.setRuta(ruta);
        ru.exportarPDF(impresion);
    }
    
    public byte[] pdf(List<T> lista, HashMap p, String ruta, String nombreReporte) throws JRException{
        ru.setLista(lista);
        ru.setParametros(p);
        ru.setNombreReporte(nombreReporte);
        ru.setRuta(ruta);
        
        return ru.exportarPDF();
    }
    
    public void cambiarFechas(){
        if(marcadorFechas != false){
            fechaInicio = null;
            fechaFin = null;
        }
    }

    public Date getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(Date fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public Date getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(Date fechaFin) {
        this.fechaFin = fechaFin;
    }

    public boolean isMarcadorFechas() {
        return marcadorFechas;
    }

    public void setMarcadorFechas(boolean marcadorFechas) {
        this.marcadorFechas = marcadorFechas;
    }
}
