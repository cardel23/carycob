/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cubisoft.carycob.managedBeans.util.reportes;

import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.faces.context.FacesContext;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperPrintManager;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.view.JasperViewer;

/**
 * Herramienta de creación e impresión de reportes
 * @author Carlos Delgadillo
 * @since 05/05/2014
 */
public class ReportesUtil<T> {
    
    private JasperPrint jasperPrint;
    private String RUTA_REPORTE;
    private String ruta;
    private List<T> lista;
    private String nombreReporte;
    private Map parametros;
    private HttpServletResponse respuesta;
    private ServletOutputStream servlet;
    
    public void init(String ruta, List<T> lista) throws JRException{
        setRUTA_REPORTE(FacesContext.getCurrentInstance().getExternalContext().getRealPath(ruta));
        JRBeanCollectionDataSource coleccion = new JRBeanCollectionDataSource(lista);
        jasperPrint = JasperFillManager.fillReport(getRUTA_REPORTE(), getParametros(), coleccion);
    }
    /**
     * Método encargado de la exportación de Reportes a PDF. Recibe un parametro
     * booleano. Si es "true", el reporte aparecerá en pantalla. Si es "false",
     * el navegador descargará el archivo en la máquina cliente.
     * 
     * @autor Carlos Delgadillo
     * @fecha 15/08/2013
     * @param impresion
     * @throws JRException
     * @throws IOException 
     */
    public void exportarPDF(boolean impresion) throws JRException, IOException{
        init(getRuta(), getLista());
        respuesta = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();
        respuesta.setHeader( "Content-Description", "File Transfer" );
        if(!impresion)
            respuesta.addHeader("Content-disposition", "attachment; filename=Reporte "+getNombreReporte());
        else
            respuesta.addHeader("Content-disposition", "inline; filename=Reporte "+getNombreReporte());
        respuesta.setContentType("application/pdf");
        respuesta.setHeader( "Content-Transfer-Encoding", "binary" );
        servlet = respuesta.getOutputStream();
        JasperExportManager.exportReportToPdfStream(jasperPrint, servlet);
        FacesContext.getCurrentInstance().responseComplete();
        FacesContext.getCurrentInstance().renderResponse();
    }
    
    public byte[] exportarPDF() throws JRException{
        init(getRuta(), getLista());
        respuesta = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();
//        respuesta.setHeader( "Content-Description", "File Transfer" );
//        if(!impresion)
//            respuesta.addHeader("Content-disposition", "attachment; filename=Reporte "+getNombreReporte());
//        else
//            respuesta.addHeader("Content-disposition", "inline; filename=Reporte "+getNombreReporte());
//        respuesta.setContentType("application/pdf");
//        respuesta.setHeader( "Content-Transfer-Encoding", "binary" );
//        servlet = respuesta.getOutputStream();
        return JasperExportManager.exportReportToPdf(jasperPrint);
    }
    
    public static String nombreReporte(String nombreCliente, Date fecha){
        StringBuilder sb = new StringBuilder();
        sb.append("Estado de cuenta ");
        sb.append(nombreCliente);
        sb.append(" ");
        sb.append(fecha.toString());
        return sb.toString();
    }

    public JasperPrint getJasperPrint() {
        return jasperPrint;
    }

    public void setJasperPrint(JasperPrint jasperPrint) {
        this.jasperPrint = jasperPrint;
    }

    public String getRUTA_REPORTE() {
        return RUTA_REPORTE;
    }

    public void setRUTA_REPORTE(String RUTA_REPORTE) {
        this.RUTA_REPORTE = RUTA_REPORTE;
    }

    public String getRuta() {
        return ruta;
    }

    public void setRuta(String ruta) {
        this.ruta = ruta;
    }

    public List<T> getLista() {
        return lista;
    }

    public void setLista(List<T> lista) {
        this.lista = lista;
    }

    public String getNombreReporte() {
        return nombreReporte;
    }

    public void setNombreReporte(String nombreReporte) {
        this.nombreReporte = nombreReporte;
    }

    public Map getParametros() {
        if(parametros == null)
            parametros = new HashMap();
        parametros.put("rutaSubRep",FacesContext.getCurrentInstance().getExternalContext().getRealPath("/Reportes"));
        return parametros;
    }

    public void setParametros(Map parametros) {
        this.parametros = parametros;
    }

    public HttpServletResponse getRespuesta() {
        return respuesta;
    }

    public void setRespuesta(HttpServletResponse respuesta) {
        this.respuesta = respuesta;
    }

    public ServletOutputStream getServlet() {
        return servlet;
    }

    public void setServlet(ServletOutputStream servlet) {
        this.servlet = servlet;
    }
}
