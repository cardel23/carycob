/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cubisoft.carycob.managedBeans.util;


import cubisoft.carycob.entidades.Cliente;
import cubisoft.carycob.entidades.Subclasificacion;
import cubisoft.carycob.excepciones.ProcesadorExcelException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.ejb.EJB;
import javax.persistence.NoResultException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.primefaces.model.UploadedFile;

/**
 * Procesador de registros en archivos de excel
 * @author Carlos Delgadillo
 * @version 1.0
 * @since 06/03/2014
 */
public class ProcesadorExcel {
        
    private boolean leerPrimeraFila = true;
    @EJB
    private static cubisoft.carycob.ejb.SubclasificacionFacade ejbSubclasificacionFacade;
    
    /**
     * Se encarga de crear una lista de registros que son procesados en la carga
     * masiva de facturas
     * @author Carlos Delgadillo
     * @since 17/03/2014
     * @param file el archivo de excel que contiene los registros de las 
     * facturas
     * @return lista de registros procesados
     * @throws IOException si se produce un error de lectura
     * 
     */
    @SuppressWarnings("UseSpecificCatch")
    public static List<Registro> leerFacturas(UploadedFile file) throws IOException, ProcesadorExcelException{
        List<Registro> lista = new ArrayList<>();
//        try {
            XSSFWorkbook libro = new XSSFWorkbook(file.getInputstream());
            XSSFSheet hoja = libro.getSheetAt(0);
            Iterator<Row> iteradorFila = hoja.iterator();
            int numero = 1;
            /* PENDIENTE: Crear el algoritmo construyendo una lista de registros
             * a partir de la asignación de las celdas donde están ubicados los
             * valores
             */

            //Si el valor es falso, avanza al siguiente registro
//            if(!leerPrimeraFila)
//                iteradorFila.next();
            int contador = 0;
            int errores = 3;
            //Por cada fila leer los valores y asignar
            while(iteradorFila.hasNext()){
                Registro registro = new Registro();
                Row fila = iteradorFila.next();
                try {
                    
                    registro.setId(leerCelda(fila.getCell(0)));
                    registro.setCliente(fila.getCell(1).getStringCellValue());
                    registro.setFechaEmision(fila.getCell(2).getDateCellValue());
                    registro.setFechaVencimiento(fila.getCell(3).getDateCellValue());
                    registro.setVendedor(leerCelda(fila.getCell(5)));
                    registro.setMonto(fila.getCell(4).getNumericCellValue());
                    registro.setNumFila(fila.getRowNum()+1);

                    System.out.println("----------Registro nº ["+(fila.getRowNum()+1)+"] procesado correctamente-------------");
                    lista.add(registro);
                    contador ++;
                } catch (Exception e) {
                    System.out.println("----------Registro nº ["+(fila.getRowNum()+1)+"] falló en la lectura-------------");
                    contador ++;
                    errores ++;
                }
            }
            if(lista.isEmpty())
                throw new ProcesadorExcelException(ProcesadorExcelException.NOELEMENTOS);
//            Validar que el tamaño de la lista sea de el 10% de los registros procesados
            if((lista.size()/contador)*100<10){
                throw new ProcesadorExcelException(ProcesadorExcelException.PCT);
            }
            
            file.getInputstream().close();
            return lista;
//            
//        }
//        catch (ProcesadorExcelException e){
//            
//        }
//        catch (Exception e) {
//            return null;
//        }
    }
    
    private static String leerCelda(Cell cell) {  
        int type;
        Object result;
        try {
            type = cell.getCellType();

            switch (type) {

                case Cell.CELL_TYPE_NUMERIC: // numeric value in Excel
        //        case Cell.CELL_TYPE_FORMULA: // precomputed value based on formula
                    result = (int) cell.getNumericCellValue();
                    break;
                case Cell.CELL_TYPE_STRING: // String Value in Excel 
                    result = cell.getStringCellValue();
                    break;
                case Cell.CELL_TYPE_BLANK:
                    result = "";
                    break;
                case Cell.CELL_TYPE_BOOLEAN: //boolean value 
                    result =  cell.getBooleanCellValue();
                    break;
                case Cell.CELL_TYPE_ERROR:
                default:  
                    throw new RuntimeException("There is no support for this type of cell");                        
            }

            return result.toString();
        } catch (Exception e) {
            result = "";
            return null;
        }
        
} 
    
    public static List<RegistroNota> leerNotas(UploadedFile file) throws IOException{
        List<RegistroNota> lista = new ArrayList<>();
        XSSFWorkbook libro = new XSSFWorkbook(file.getInputstream());
        XSSFSheet hoja = libro.getSheetAt(0);
        Iterator<Row> iteradorFila = hoja.iterator();
        try {
            while(iteradorFila.hasNext()){
                Row fila = iteradorFila.next();
                RegistroNota reg = new RegistroNota();
                reg.setId(leerCelda(fila.getCell(0)));
                reg.setFactura(leerCelda(fila.getCell(1)));
                reg.setCliente(leerCelda(fila.getCell(2)));
                reg.setFecha(fila.getCell(3).getDateCellValue());
                reg.setTipoNota(fila.getCell(4).getStringCellValue());
                reg.setMonto(fila.getCell(5).getNumericCellValue());
                reg.setObservaciones(leerCelda(fila.getCell(6)));
                reg.setNumFila(fila.getRowNum()+1);

                lista.add(reg);
            }
            file.getInputstream().close();
            return lista;
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e.getMessage());
            return null;
        }
        
    }
    
    /**
     * Procesa una lista de clientes para carga masiva
     * @param file
     * @return la lista de clientes procesados correctamente que se van a guardar
     * @throws IOException si se produce error de lectura del archivo excel
     * @autor Carlos Delgadillo
     * @since 03/06/2014
     */
    public static List<RegistroCliente> leerClientes(UploadedFile file) throws IOException{
        try {
            List<RegistroCliente> lista = new ArrayList<>();
            Iterator<Row> iteradorFila = setearLibro(file).iterator();
            while(iteradorFila.hasNext()){
                lista.add(setearCliente(iteradorFila.next()));
            }
            return lista;
        } catch (Exception e) {
            return null;
        }
    }
    
    /**
     * Crea un cliente a partir de los datos del archivo de excel
     * @param fila la fila del archivo de excel donde se encuentran los datos
     *        del cliente
     * @return cliente procesado
     * @autor Carlos Delgadillo
     * @since 03/06/2014
     */
    private static RegistroCliente setearCliente(Row fila){
        RegistroCliente regcliente = new RegistroCliente();
        regcliente.setIdCliente(fila.getCell(0).getStringCellValue());
        regcliente.setNombre(fila.getCell(1).getStringCellValue());
        regcliente.setCorreo(fila.getCell(2).getStringCellValue());
        regcliente.setSubclasificaciones(new ArrayList<String>());
        try {
            for(int i=3;i<=fila.getLastCellNum();i++){
                regcliente.getSubclasificaciones().add(leerCelda(fila.getCell(i)));
                }
            return regcliente;
        } catch (Exception e) {
            e.getMessage();
            return null;
        }
        
    }
    
    /**
     * Prepara el libro de excel que se va a leer
     * @param file el archivo de excel a procesar
     * @return la hoja del archivo que se va a procesar
     * @throws IOException si se produce un error de lectura del archivo
     * @autor Carlos Delgadillo
     * @since 03/06/2014
     */
    private static XSSFSheet setearLibro(UploadedFile file) throws IOException{
        XSSFWorkbook libro = new XSSFWorkbook(file.getInputstream());
        XSSFSheet hoja = libro.getSheetAt(0);
        return hoja;
    }

    private static cubisoft.carycob.ejb.SubclasificacionFacade getEjbSubclasificacionFacade() {
        return ejbSubclasificacionFacade;
    }

    public void setEjbSubclasificacionFacade(cubisoft.carycob.ejb.SubclasificacionFacade ejbSubclasificacionFacade) {
        this.ejbSubclasificacionFacade = ejbSubclasificacionFacade;
    }
        
}
