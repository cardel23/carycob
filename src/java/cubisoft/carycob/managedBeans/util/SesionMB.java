/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cubisoft.carycob.managedBeans.util;

import cubisoft.carycob.ejb.UsuarioFacade;
import cubisoft.carycob.entidades.Rol;
import cubisoft.carycob.entidades.Usuario;
import java.io.IOException;
import java.io.Serializable;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Controlador de sesion
 * @author Estacion 2
 */
@ManagedBean(name = "sesionMB")
@SessionScoped
public class SesionMB implements Serializable {
    
    private Usuario usuario;
    private String nombreUsuario, pass;
    public Boolean logueado;
    private HttpSession sesion;
    private HttpServletResponse response;
    private static FacesContext context = FacesContext.getCurrentInstance();
    
    @EJB
    private UsuarioFacade ejbUsuario;
    
    
    public SesionMB(){}
    
    /**
     * Inicia la sesión en el sistema
     * @throws IOException 
     */
    public void inicio() throws IOException{
        usuario = ejbUsuario.buscarUsuario(nombreUsuario, pass);
        if(usuario==null){
            JsfUtil.addErrorMessage("El usuario o contraseña no existe");
        }
        else{
            try {
                sesion = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(true);
                sesion.setAttribute("currentUser", usuario);
                response = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();
                response.sendRedirect("inicio.xhtml?faces-redirect=true");
            } catch (Exception e) {
                e.getCause();
                e.getMessage();
                JsfUtil.addErrorMessage(e.getMessage());
            }
            
        }
    }
    
    /**
     * Cierra la sesión del usuario en el sistema
     * @return la URL para el inicio de sesión
     */
    public String logout(){
        ejbUsuario.limpiar();
        sesion = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
        sesion.invalidate();   
        FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
        return "/login?faces-redirect=true";
    }

    public Usuario getUsuario() {
        sesion = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
        return (Usuario) sesion.getAttribute("currentUser");
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public String getNombreUsuario() {
        return nombreUsuario;
    }

    public void setNombreUsuario(String nombreUsuario) {
        this.nombreUsuario = nombreUsuario;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }
   
    public Boolean isLogueado() {
       sesion = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
       usuario = (Usuario) sesion.getAttribute("currentUser");
       
       if(usuario != null) {
           return true;
       } else {
           return false;
       }
    }
    
    public Boolean isAdmin(){
        try{
            if(usuario.getRol().getIdRol().equals(Rol.ROL_ADMIN))
            return true;
        else
            return false;
        }
        catch(Exception e){
            return false;
        }
        
    }

    
    
}
