/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cubisoft.carycob.managedBeans.util;

import cubisoft.carycob.entidades.Cliente;
import cubisoft.carycob.vistas.CuentaCliente;
import cubisoft.carycob.vistas.SaldoFactura;
import java.util.List;

/**
 * Esta clase se encarga de manejar los saldos de los clientes en
 * una cuenta.
 * @author Carlos Delgadillo
 */

public class Cuenta {
    
    private Cliente cliente;
    private CuentaCliente cuentaCliente;
    private List<SaldoFactura> listaFacturas;
    
    public Cuenta(){}

    /**
     * @return the cliente
     */
    public Cliente getCliente() {
        return cliente;
    }

    /**
     * @param cliente the cliente to set
     */
    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    /**
     * @return the listaFacturas
     */
    public List<SaldoFactura> getListaFacturas() {
        return listaFacturas;
    }

    /**
     * @param listaFacturas the listaFacturas to set
     */
    public void setListaFacturas(List<SaldoFactura> listaFacturas) {
        this.listaFacturas = listaFacturas;
    }

    public CuentaCliente getCuentaCliente() {
        return cuentaCliente;
    }

    public void setCuentaCliente(CuentaCliente cuentaCliente) {
        this.cuentaCliente = cuentaCliente;
    }
}
