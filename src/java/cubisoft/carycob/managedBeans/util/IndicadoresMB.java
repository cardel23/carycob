/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cubisoft.carycob.managedBeans.util;

import cubisoft.carycob.entidades.Cliente;
import cubisoft.carycob.vistas.CuentaCliente;
import cubisoft.carycob.vistas.VentasClasificacion;
import cubisoft.carycob.vistas.VentasCliente;
import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
//import org.primefaces.model.chart.AxisType;
import org.primefaces.model.chart.CartesianChartModel;
import org.primefaces.model.chart.BarChartSeries;
import org.primefaces.model.chart.LineChartSeries;

/**
 * 
 * @author Carlos Delgadillo
 */
@ViewScoped
@ManagedBean(name = "indicadoresMB")
public class IndicadoresMB implements Serializable {
    
    private String vendedor, clasificacion;
    private Cliente cliente;
    
    private static String Vertical = "vertical";
    private static String Horizontal = "horizontal";
    private String orientacion2, ejeX2, ejeY2;
    private CartesianChartModel clientesMorosos, ventasCategoria, vendedores, ventasClientes;
    @EJB
    private cubisoft.carycob.ejb.CuentaClienteFacade ejbCuenta;
    @EJB
    private cubisoft.carycob.ejb.VentasClasificacionFacade ejbClasificacion;
    @EJB
    private cubisoft.carycob.ejb.VentasClienteFacade ejbCliente;
    
    
    @PostConstruct
    public void init(){
        construct();
    }
    
    private void construct(){
        clientesMasMorosos();
        ventasPorCategoria();
        ventasPorVendedores();
        ventasPorCliente();
    }
    /**
     * Grafico de los clientes mas morosos
     */
    public CartesianChartModel clientesMasMorosos(){
        clientesMorosos = new CartesianChartModel();
        BarChartSeries debe = new BarChartSeries();
        BarChartSeries haber = new BarChartSeries();
        LineChartSeries vencido = new LineChartSeries();
        debe.setLabel("Debe");
        haber.setLabel("Haber");
        vencido.setLabel("Vencido");
        try {
            for(CuentaCliente c: ejbCuenta.obtenerClientesMorosos()){
                debe.set(c.getNombre(), c.getDebe());
                haber.set(c.getNombre(), c.getHaber());
                vencido.set(c.getNombre(), c.getSaldoVencido());
            }
        } catch (Exception e) {
            debe.set("Sin selección", 0);
            haber.set("Sin selección", 0);
            vencido.set("Sin selección", 0);
        }
        
        
        
        clientesMorosos.addSeries(debe);
        clientesMorosos.addSeries(haber);
        clientesMorosos.addSeries(vencido);
        
        
//        clientesMorosos.setTitle("Top 10 de clientes morosos");
//        clientesMorosos.setLegendPosition("ne");
//        clientesMorosos.setSeriesColors("00B0F6,D40D7D,FFE900");
//        clientesMorosos.setShowDatatip(false);
//        clientesMorosos.setShowPointLabels(true);
//        clientesMorosos.getAxis(AxisType.Y).setMin(0);
        return clientesMorosos;
    }
    
    /**
     * Obtiene las ventas hechas por clasificación
     */
    public CartesianChartModel ventasPorCategoria(){
        ventasCategoria = new CartesianChartModel();
        BarChartSeries vendido = new BarChartSeries();
        BarChartSeries vencido = new BarChartSeries();
        BarChartSeries cobrado = new BarChartSeries();
        
        vencido.setLabel("Vencido");
        vendido.setLabel("Vendido");
        cobrado.setLabel("Cobrado");
        
        if(clasificacion == null)
            clasificacion = "No clasificado";
        try {
           if(ejbClasificacion.listaPorClasificacion(clasificacion).isEmpty())
               throw new Exception();
           for(VentasClasificacion v : ejbClasificacion.listaPorClasificacion(clasificacion)){
               vendido.set(v.getSubclasificacion(), v.getVendido());
               vencido.set(v.getSubclasificacion(), v.getVencido());
               cobrado.set(v.getSubclasificacion(), v.getCobrado());
        } 
        } catch (Exception e) {
            vendido.set("Sin selección", 0);
            cobrado.set("Sin selección", 0);
            vencido.set("Sin selección", 0);
        }
        
        ventasCategoria.addSeries(vendido);
        ventasCategoria.addSeries(vencido);
        ventasCategoria.addSeries(cobrado);
        return ventasCategoria;
    }
    
    public CartesianChartModel ventasPorVendedores(){
        vendedores = new CartesianChartModel();
        BarChartSeries vendido = new BarChartSeries();
        BarChartSeries vencido = new BarChartSeries();
        BarChartSeries cobrado = new BarChartSeries();
        
        vencido.setLabel("Vencido");
        vendido.setLabel("Vendido");
        cobrado.setLabel("Cobrado");
        if(vendedor == null)
            vendedor = "[Sin comisión]";
        try {
            for(VentasCliente v : ejbCliente.ventasPorCliente(vendedor)){
                vendido.set(v.getVendedor(), v.getVendido());
                vencido.set(v.getVendedor(), v.getVencido());
                cobrado.set(v.getVendedor(), v.getCobrado());
        }
        } catch (Exception e) {
            vendido.set("Sin selección", 0);
            cobrado.set("Sin selección", 0);
            vencido.set("Sin selección", 0);
        }
        
        vendedores.addSeries(vendido);
        vendedores.addSeries(vencido);
        vendedores.addSeries(cobrado);
        /**
        vendedores.setTitle("Ventas por vendedor");
        vendedores.setLegendPosition("ne");
        vendedores.setSeriesColors("00B0F6,D40D7D,FFE900");
        vendedores.setShowDatatip(false);
        vendedores.setShowPointLabels(true);
        vendedores.getAxis(AxisType.Y).setMin(0);
        */
        return vendedores;
    }
    
    public CartesianChartModel ventasPorCliente(){
        ventasClientes = new CartesianChartModel();
        BarChartSeries debe = new BarChartSeries();
        BarChartSeries haber = new BarChartSeries();
        BarChartSeries vencido = new BarChartSeries();
        debe.setLabel("Debe");
        haber.setLabel("Haber");
        vencido.setLabel("Vencido");
        try {
            CuentaCliente c = ejbCuenta.obtenerCuenta(cliente.getIdCliente());
            debe.set(c.getNombre(), c.getDebe());
            haber.set(c.getNombre(), c.getHaber());
            vencido.set(c.getNombre(), c.getSaldoVencido());
            
        } catch (Exception e) {
            if(cliente == null){
                debe.set("Sin selección", 0);
                haber.set("Sin selección", 0);
                vencido.set("Sin selección", 0);
            }
            else{
                debe.set(cliente.getNombre(), 0);
                haber.set(cliente.getNombre(), 0);
                vencido.set(cliente.getNombre(), 0);
            }
        }
        
        ventasClientes.addSeries(debe);
        ventasClientes.addSeries(haber);
        ventasClientes.addSeries(vencido);
        /*
        ventasClientes.setTitle("Ventas por cliente");
        ventasClientes.setLegendPosition("ne");
        ventasClientes.setSeriesColors("00B0F6,D40D7D,FFE900");
        ventasClientes.setShowDatatip(false);
        ventasClientes.setShowPointLabels(true);
        ventasClientes.getAxis(AxisType.Y).setMin(0);
        */
        return ventasClientes;
    }

    public String getVendedor() {
//        if(vendedor == null)
//            vendedor = "[Sin comisión]";
        return vendedor;
    }

    public void setVendedor(String vendedor) {
        this.vendedor = vendedor;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public String getClasificacion() {
//        if(clasificacion == null)
//            clasificacion = "No clasificado";
        return clasificacion;
    }

    public void setClasificacion(String clasificacion) {
        this.clasificacion = clasificacion;
    }

    public cubisoft.carycob.ejb.VentasClasificacionFacade getEjbClasificacion() {
        return ejbClasificacion;
    }

    public cubisoft.carycob.ejb.VentasClienteFacade getEjbCliente() {
        return ejbCliente;
    }

    public CartesianChartModel getClientesMorosos() {
//        if(clientesMorosos.getSeries().get(0).getData().size()>5)
//            orientacion = Vertical;
//        else
//            orientacion = Horizontal;
        return clientesMorosos;
    }

    public CartesianChartModel getVentasCategoria() {
        if(ventasCategoria.getSeries().get(0).getData().size()<=4)
            orientacion2 = Vertical;
        else
            orientacion2 = Horizontal;
        return ventasCategoria;
    }

    public CartesianChartModel getVendedores() {
//        if(vendedores.getSeries().get(0).getData().size()>5)
//            orientacion = Vertical;
//        else
//            orientacion = Horizontal;
        return vendedores;
    }

    public CartesianChartModel getVentasClientes() {
//        if(ventasClientes.getSeries().get(0).getData().size()>5)
//            orientacion = Vertical;
//        else
//            orientacion = Horizontal;
        return ventasClientes;
    }

    public String getOrientacion2() {
        return orientacion2;
    }

    public void setOrientacion2(String orientacion2) {
        this.orientacion2 = orientacion2;
    }

    public String getEjeX2() {
        return ejeX2;
    }

    public void setEjeX2(String ejeX2) {
        this.ejeX2 = ejeX2;
    }

    public String getEjeY2() {
        return ejeY2;
    }

    public void setEjeY2(String ejeY2) {
        this.ejeY2 = ejeY2;
    }

}
