/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cubisoft.carycob.managedBeans.util;

import cubisoft.carycob.ejb.FacturaFacade;
import cubisoft.carycob.entidades.Factura;
import cubisoft.carycob.entidades.Gestion;
import cubisoft.carycob.entidades.Usuario;
import cubisoft.carycob.managedBeans.FacturaController;
import cubisoft.carycob.vistas.CuentaCliente;
import cubisoft.carycob.vistas.Vencimiento;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.DefaultScheduleEvent;
import org.primefaces.model.DefaultScheduleModel;
import org.primefaces.model.ScheduleEvent;
import org.primefaces.model.ScheduleModel;
import org.primefaces.model.chart.BarChartSeries;
import org.primefaces.model.chart.CartesianChartModel;
import org.primefaces.model.chart.ChartSeries;
import org.primefaces.model.chart.LineChartSeries;

/**
 * Controlador adicional
 * @author Carlos Delgadillo
 */

@ManagedBean(name = "miscMB")
@SessionScoped
public class UtilMB implements Serializable{
    
    @EJB
    private FacturaFacade ejbFactura;
    @EJB
    private cubisoft.carycob.ejb.GestionFacade ejbGestion;
    
    @EJB
    private cubisoft.carycob.ejb.VencimientoFacade ejbVenc;
    
    @EJB
    private cubisoft.carycob.ejb.CuentaClienteFacade ejbCuenta;
    
    private List<Factura> listaFacturas;
    private ScheduleModel calendario;
    private ScheduleEvent evento;
    private Factura factura;
    private List<Gestion> listaGestion;
    private Gestion gestion, nuevaGestion;
    private String nombreVendedor;
    
    @ManagedProperty(value = "#{facturaController}")
    private FacturaController facturaController;
    public UtilMB() {}
    
    
    /**
     * 
     * @return el valor maximo que el grafico va a desplegar
     */
    public double maxGrafico(){
        return ejbFactura.saldoFacturado();
    }
    
    /**
     * Crea grafico de vencidos
     * @return 
     */
    public CartesianChartModel vencimientoDeFacturas(){
        CartesianChartModel modelo = new CartesianChartModel();
        ChartSeries s = new ChartSeries();
        int mas90 = 0, menos90 = 0, menos60 = 0, menos30 = 0, entre30 = 0, entre60 = 0, entre90 = 0;
        for(Vencimiento v : ejbVenc.getGraficoVencimiento()){
            if(v.getDiferenciaDias()>90)
                mas90+=v.getContador();
            else if(v.getDiferenciaDias()>60)
                menos90+=v.getContador();
            else if(v.getDiferenciaDias()>30)
                menos60+=v.getContador();
            else if(v.getDiferenciaDias()>0)
                menos30+=v.getContador();
            else if(v.getDiferenciaDias()>=-30)
                entre30+=v.getContador();
            else if(v.getDiferenciaDias()>=-60)
                entre60+=v.getContador();
            else if(v.getDiferenciaDias()>=-90)
                entre90+=v.getContador();
        }
        
        s.set(">90 días", mas90);
        s.set("entre 60 y 90", menos90);
        s.set("entre 30 y 60", menos60);
        s.set("entre 1 y 30", menos30);
        s.set("1 a 30", entre30);
        s.set("30 a 60", entre60);
        s.set("60 a 90", entre90);
        
        modelo.addSeries(s);
        return modelo;
    }
    
    private List<Factura> getListaFacturasAVencer(){
//        if(listaFacturas == null){
            listaFacturas = new ArrayList<>();
            listaFacturas.addAll(ejbFactura.facturasPorEstado(Factura.vencida));
            listaFacturas.addAll(ejbFactura.facturasPorEstado(Factura.pendiente));
//        }
        return listaFacturas;
    }
    
    private List<Gestion> getListaGestiones(){
//        if(listaGestion == null){
//            listaGestion = new ArrayList<>();
//            listaGestion.addAll(ejbGestion.findAll());
//        }
        listaGestion = ejbGestion.findAll();
        return listaGestion;
    }
    
    private List<Gestion> getListaGestionesAux(Usuario usuario){
//        if(listaGestion == null){
//            listaGestion = new ArrayList<>();
//            listaGestion.addAll(usuario.getGestionList());
//        }
        listaGestion = ejbGestion.gestionesUsuario(usuario);
        return listaGestion;
    }
    
    
    public void actualizarCalendarioAdmin(){
        calendario = new DefaultScheduleModel();
        for(Factura f : getListaFacturasAVencer()){
            DefaultScheduleEvent e = new DefaultScheduleEvent("Nº "+f.getIdFactura(), f.getFechaVencimiento(), f.getFechaVencimiento(),f);
            if(ejbFactura.isFacturaVencida(f))
                e.setStyleClass("eventoFacturaVencida");
            e.setAllDay(true);
            calendario.addEvent(e);
        }
        for(Gestion g : getListaGestiones()){
            DefaultScheduleEvent e = new DefaultScheduleEvent(g.getDescripcion(), g.getFechaLimiteGestion(), g.getFechaLimiteGestion(),g);
            e.setAllDay(true);
            if(g.getEstado().equals(Gestion.pendiente))
                e.setStyleClass("eventoGestionPendiente");
            else
                e.setStyleClass("eventoGestionRealizada");
            calendario.addEvent(e);
        }
    }
    
    /**
     * Valida si un pago es mayor al saldo de la factura
     * @param pago
     * @param saldo
     * @return verdadero o falso
     */
    public void validaPago(Double pago, Double saldo) throws java.lang.Exception{
        if(pago>saldo)
            return;
        else{
            JsfUtil.addErrorMessage("El pago no puede ser mayor que el saldo");
            throw new Exception();
        }
    }
    
    public void actualizarCalendarioAux(Usuario usuario){
        calendario = new DefaultScheduleModel();
        for(Gestion g : getListaGestionesAux(usuario)){
            DefaultScheduleEvent e = new DefaultScheduleEvent(g.getDescripcion(), g.getFechaLimiteGestion(), g.getFechaLimiteGestion(),g);
            e.setAllDay(true);
            if(g.getEstado().equals(Gestion.pendiente))
                e.setStyleClass("eventoGestionPendiente");
            else
                e.setStyleClass("eventoGestionRealizada");
//            e.setStyleClass("eventoGestion");
            calendario.addEvent(e);
        }
    }

    public ScheduleModel getCalendario() {
        return calendario;
    }
    
    public void seleccionarEvento(SelectEvent eventoSeleccionado){
        evento = (ScheduleEvent) eventoSeleccionado.getObject();
        try {
            if(evento.getData().getClass().equals(Factura.class)){
                gestion = null;
                factura = (Factura) evento.getData();
            }
            else if(evento.getData().getClass().equals(Gestion.class))
            {
                factura = null;
                gestion = (Gestion) evento.getData();
            }
        } catch (Exception e) {
            
        }
    }
    
    public void dialogoEvento(){
        JsfUtil.abrirDialogo("dlg");
    }
    
    public void anularEvento(SelectEvent fechaSeleccionada){
        evento = null;
        factura = null;
        gestion = null;
    }
    
    public void agregarGestion(Gestion gestion){
        DefaultScheduleEvent event = new DefaultScheduleEvent(gestion.getDescripcion(), gestion.getFechaLimiteGestion(), gestion.getFechaLimiteGestion(),gestion);
        event.setAllDay(true);
        event.setStyleClass("eventoGestionPendiente");
        calendario.addEvent(event);
    }
    
    public void editarGestion(Gestion gestion){
        calendario.deleteEvent(evento);
        DefaultScheduleEvent event = new DefaultScheduleEvent(gestion.getDescripcion(), gestion.getFechaLimiteGestion(), gestion.getFechaLimiteGestion(),gestion);
        event.setAllDay(true);
        event.setStyleClass("eventoGestion");
        calendario.addEvent(event);
    }
    
    public void eliminarEvento(ScheduleEvent evento){
        calendario.deleteEvent(evento);
    }

    public ScheduleEvent getEvento() {
        return evento;
    }

    public void setEvento(ScheduleEvent evento) {
        this.evento = evento;
    }

    public FacturaController getFacturaController() {
        return facturaController;
    }

    public void setFacturaController(FacturaController facturaController) {
        this.facturaController = facturaController;
    }

    public Factura getFactura() {
        return factura;
    }

    public void setFactura(Factura factura) {
        this.factura = factura;
    }

    public Gestion getGestion() {
//        if(gestion == null)
//            gestion = new Gestion();
        return gestion;
    }

    public void setGestion(Gestion gestion) {
        this.gestion = gestion;
    }

    public Gestion getNuevaGestion() {
        if(nuevaGestion == null)
            nuevaGestion = new Gestion();
        return nuevaGestion;
    }
    
    public void anularNuevaGestion(){
        nuevaGestion = new Gestion();
    }

    public void setNuevaGestion(Gestion nuevaGestion) {
        this.nuevaGestion = nuevaGestion;
    }

    public String getNombreVendedor() {
        return nombreVendedor;
    }

    public void setNombreVendedor(String nombreVendedor) {
        this.nombreVendedor = nombreVendedor;
    }

    private Exception Exception() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
