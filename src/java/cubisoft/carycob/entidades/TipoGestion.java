/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cubisoft.carycob.entidades;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Estacion 2
 */
@Entity
@Table(name = "TipoGestion")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TipoGestion.findAll", query = "SELECT t FROM TipoGestion t"),
    @NamedQuery(name = "TipoGestion.findByIdTipoGestion", query = "SELECT t FROM TipoGestion t WHERE t.idTipoGestion = :idTipoGestion"),
    @NamedQuery(name = "TipoGestion.findByDescripcion", query = "SELECT t FROM TipoGestion t WHERE t.descripcion = :descripcion")})
public class TipoGestion implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "idTipoGestion", nullable = false)
    private Integer idTipoGestion;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "descripcion", nullable = false, length = 20)
    private String descripcion;
//    @OneToMany(cascade = CascadeType.ALL, mappedBy = "tipoGestion")
//    private List<Gestion> gestionList;

    public TipoGestion() {
    }

    public TipoGestion(Integer idTipoGestion) {
        this.idTipoGestion = idTipoGestion;
    }

    public TipoGestion(Integer idTipoGestion, String descripcion) {
        this.idTipoGestion = idTipoGestion;
        this.descripcion = descripcion;
    }

    public Integer getIdTipoGestion() {
        return idTipoGestion;
    }

    public void setIdTipoGestion(Integer idTipoGestion) {
        this.idTipoGestion = idTipoGestion;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
//
//    @XmlTransient
//    public List<Gestion> getGestionList() {
//        return gestionList;
//    }
//
//    public void setGestionList(List<Gestion> gestionList) {
//        this.gestionList = gestionList;
//    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idTipoGestion != null ? idTipoGestion.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TipoGestion)) {
            return false;
        }
        TipoGestion other = (TipoGestion) object;
        if ((this.idTipoGestion == null && other.idTipoGestion != null) || (this.idTipoGestion != null && !this.idTipoGestion.equals(other.idTipoGestion))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cubisoft.carycob.entidades.TipoGestion[ idTipoGestion=" + idTipoGestion + " ]";
    }
    
}
