/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cubisoft.carycob.entidades;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Estacion 2
 */
@Entity
@Table(name = "Rechazo")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Rechazo.findAll", query = "SELECT r FROM Rechazo r"),
    @NamedQuery(name = "Rechazo.findByIdRechazo", query = "SELECT r FROM Rechazo r WHERE r.idRechazo = :idRechazo"),
    @NamedQuery(name = "Rechazo.findByFactura", query = "SELECT r FROM Rechazo r WHERE r.factura = :factura"),
    @NamedQuery(name = "Rechazo.findByCliente", query = "SELECT r FROM Rechazo r WHERE r.cliente = :cliente"),
    @NamedQuery(name = "Rechazo.findByVendedor", query = "SELECT r FROM Rechazo r WHERE r.vendedor = :vendedor"),
    @NamedQuery(name = "Rechazo.findByFechaEmision", query = "SELECT r FROM Rechazo r WHERE r.fechaEmision = :fechaEmision"),
    @NamedQuery(name = "Rechazo.findByFechaVencimiento", query = "SELECT r FROM Rechazo r WHERE r.fechaVencimiento = :fechaVencimiento"),
    @NamedQuery(name = "Rechazo.findByMonto", query = "SELECT r FROM Rechazo r WHERE r.monto = :monto"),
    @NamedQuery(name = "Rechazo.findByNumFila", query = "SELECT r FROM Rechazo r WHERE r.numFila = :numFila")})
public class Rechazo implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "idRechazo", nullable = false, length = 10)
    private String idRechazo;
    @Size(max = 10)
    @Column(name = "factura", length = 10)
    private String factura;
    @Size(max = 50)
    @Column(name = "cliente", length = 50)
    private String cliente;
    @Size(max = 50)
    @Column(name = "vendedor", length = 50)
    private String vendedor;
    @Column(name = "fechaEmision")
    @Temporal(TemporalType.DATE)
    private Date fechaEmision;
    @Column(name = "fechaVencimiento")
    @Temporal(TemporalType.DATE)
    private Date fechaVencimiento;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "monto", precision = 53)
    private Double monto;
    @Lob
    @Size(max = 2147483647)
    @Column(name = "observaciones", length = 2147483647)
    private String observaciones;
    @Basic(optional = false)
    @NotNull
    @Column(name = "numFila", nullable = false)
    private int numFila;
    @JoinColumn(name = "carga", referencedColumnName = "idCarga", nullable = false)
    @ManyToOne(optional = false)
    private Carga carga;

    public Rechazo() {
    }

    public Rechazo(String idRechazo) {
        this.idRechazo = idRechazo;
    }

    public Rechazo(String idRechazo, int numFila) {
        this.idRechazo = idRechazo;
        this.numFila = numFila;
    }

    public String getIdRechazo() {
        return idRechazo;
    }

    public void setIdRechazo(String idRechazo) {
        this.idRechazo = idRechazo;
    }

    public String getFactura() {
        return factura;
    }

    public void setFactura(String factura) {
        this.factura = factura;
    }

    public String getCliente() {
        return cliente;
    }

    public void setCliente(String cliente) {
        this.cliente = cliente;
    }

    public String getVendedor() {
        return vendedor;
    }

    public void setVendedor(String vendedor) {
        this.vendedor = vendedor;
    }

    public Date getFechaEmision() {
        return fechaEmision;
    }

    public void setFechaEmision(Date fechaEmision) {
        this.fechaEmision = fechaEmision;
    }

    public Date getFechaVencimiento() {
        return fechaVencimiento;
    }

    public void setFechaVencimiento(Date fechaVencimiento) {
        this.fechaVencimiento = fechaVencimiento;
    }

    public Double getMonto() {
        return monto;
    }

    public void setMonto(Double monto) {
        this.monto = monto;
    }

    public String getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    public int getNumFila() {
        return numFila;
    }

    public void setNumFila(int numFila) {
        this.numFila = numFila;
    }

    public Carga getCarga() {
        return carga;
    }

    public void setCarga(Carga carga) {
        this.carga = carga;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idRechazo != null ? idRechazo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Rechazo)) {
            return false;
        }
        Rechazo other = (Rechazo) object;
        if ((this.idRechazo == null && other.idRechazo != null) || (this.idRechazo != null && !this.idRechazo.equals(other.idRechazo))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
//        return "cubisoft.carycob.entidades.Rechazo[ idRechazo=" + idRechazo + " ]";
        return idRechazo;
    }
    
}
