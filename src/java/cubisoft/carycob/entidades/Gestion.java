/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cubisoft.carycob.entidades;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Estacion 2
 */
@Entity
@Table(name = "Gestion")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Gestion.findAll", query = "SELECT g FROM Gestion g"),
    @NamedQuery(name = "Gestion.findByIdGestion", query = "SELECT g FROM Gestion g WHERE g.idGestion = :idGestion"),
    @NamedQuery(name = "Gestion.findByUsuario", query = "SELECT g FROM Gestion g WHERE g.usuario = :usuario"),
    @NamedQuery(name = "Gestion.findByFechaGestion", query = "SELECT g FROM Gestion g WHERE g.fechaGestion = :fechaGestion"),
    @NamedQuery(name = "Gestion.findByFechaLimiteGestion", query = "SELECT g FROM Gestion g WHERE g.fechaLimiteGestion = :fechaLimiteGestion"),
    @NamedQuery(name = "Gestion.findByEstado", query = "SELECT g FROM Gestion g WHERE g.estado = :estado")})
public class Gestion implements Serializable {
    @Basic(optional = false)
    @NotNull
    @Column(name = "fechaGestion")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaGestion;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "descripcion")
    private String descripcion;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "estado")
    private String estado;
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "idGestion", nullable = false)
    private String idGestion;
//    @Basic(optional = false)
//    @NotNull
//    @Size(min = 1, max = 10)
//    @Column(name = "cliente", nullable = false, length = 10)
//    private String cliente;
//    @Basic(optional = false)
//    
//    @Lob
    @Size(min = 0, max = 2147483647)
    @Column(name = "observaciones", length = 2147483647)
    private String observaciones;
    @Basic(optional = false)
    @NotNull
    @Column(name = "fechaLimiteGestion", nullable = false)
    @Temporal(TemporalType.DATE)
    private Date fechaLimiteGestion;
    @JoinColumn(name = "usuario", referencedColumnName = "idUsuario", nullable = false)
    @ManyToOne(optional = false)
    private Usuario usuario;
//    @JoinColumn(name = "tipoGestion", referencedColumnName = "idTipoGestion", nullable = false)
//    @ManyToOne(optional = false)
//    private TipoGestion tipoGestion;
    @JoinColumn(name = "factura", referencedColumnName = "idFactura", nullable = false)
    @ManyToOne(optional = false)
    private Factura factura;
    
    public static String pendiente = "pendiente";
    public static String realizada = "realizada";
    public static String vencida = "vencida";

    public Gestion() {
    }

    public Gestion(String idGestion) {
        this.idGestion = idGestion;
    }

    public Gestion(String idGestion, /*String cliente,*/ Date fechaGestion, String observaciones, Date fechaLimiteGestion, String estado) {
        this.idGestion = idGestion;
//        this.cliente = cliente;
        this.fechaGestion = fechaGestion;
        this.observaciones = observaciones;
        this.fechaLimiteGestion = fechaLimiteGestion;
        this.estado = estado;
    }

    public String getIdGestion() {
        return idGestion;
    }

    public void setIdGestion(String idGestion) {
        this.idGestion = idGestion;
    }

//    public String getCliente() {
//        return cliente;
//    }
//
//    public void setCliente(String cliente) {
//        this.cliente = cliente;
//    }

    public String getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    public Date getFechaLimiteGestion() {
        return fechaLimiteGestion;
    }

    public void setFechaLimiteGestion(Date fechaLimiteGestion) {
        this.fechaLimiteGestion = fechaLimiteGestion;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

//    public TipoGestion getTipoGestion() {
//        return tipoGestion;
//    }
//
//    public void setTipoGestion(TipoGestion tipoGestion) {
//        this.tipoGestion = tipoGestion;
//    }

    public Factura getFactura() {
        return factura;
    }

    public void setFactura(Factura factura) {
        this.factura = factura;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idGestion != null ? idGestion.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Gestion)) {
            return false;
        }
        Gestion other = (Gestion) object;
        if ((this.idGestion == null && other.idGestion != null) || (this.idGestion != null && !this.idGestion.equals(other.idGestion))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return descripcion;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public Date getFechaGestion() {
        return fechaGestion;
    }

    public void setFechaGestion(Date fechaGestion) {
        this.fechaGestion = fechaGestion;
    }
    
}
