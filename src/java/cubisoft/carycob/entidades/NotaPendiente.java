/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cubisoft.carycob.entidades;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author TOSHIBA
 */
@Entity
@Table(name = "NotaPendiente")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "NotaPendiente.findAll", query = "SELECT n FROM NotaPendiente n"),
    @NamedQuery(name = "NotaPendiente.findByIdNotaPendiente", query = "SELECT n FROM NotaPendiente n WHERE n.idNotaPendiente = :idNotaPendiente"),
    @NamedQuery(name = "NotaPendiente.findByFecha", query = "SELECT n FROM NotaPendiente n WHERE n.fecha = :fecha"),
    @NamedQuery(name = "NotaPendiente.findByTipoNota", query = "SELECT n FROM NotaPendiente n WHERE n.tipoNota = :tipoNota"),
    @NamedQuery(name = "NotaPendiente.findByMonto", query = "SELECT n FROM NotaPendiente n WHERE n.monto = :monto")})
public class NotaPendiente implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 15)
    @Column(name = "idNotaPendiente")
    private String idNotaPendiente;
    @Basic(optional = false)
    @NotNull
    @Column(name = "fecha")
    @Temporal(TemporalType.DATE)
    private Date fecha;
    @Lob
    @Size(max = 2147483647)
    @Column(name = "observaciones")
    private String observaciones;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 7)
    @Column(name = "tipoNota")
    private String tipoNota;
    @Basic(optional = false)
    @NotNull
    @Column(name = "monto")
    private double monto;
    @JoinColumn(name = "cliente", referencedColumnName = "idCliente")
    @ManyToOne(optional = false)
    private Cliente cliente;
    @JoinColumn(name = "carga", referencedColumnName = "idCarga")
    @ManyToOne(optional = false, cascade = CascadeType.ALL)
    private Carga carga;

    public NotaPendiente() {
    }

    public NotaPendiente(String idNotaPendiente) {
        this.idNotaPendiente = idNotaPendiente;
    }

    public NotaPendiente(String idNotaPendiente, Date fecha, String tipoNota, double monto) {
        this.idNotaPendiente = idNotaPendiente;
        this.fecha = fecha;
        this.tipoNota = tipoNota;
        this.monto = monto;
    }

    public String getIdNotaPendiente() {
        return idNotaPendiente;
    }

    public void setIdNotaPendiente(String idNotaPendiente) {
        this.idNotaPendiente = idNotaPendiente;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public String getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    public String getTipoNota() {
        return tipoNota;
    }

    public void setTipoNota(String tipoNota) {
        this.tipoNota = tipoNota;
    }

    public double getMonto() {
        return monto;
    }

    public void setMonto(double monto) {
        this.monto = monto;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public Carga getCarga() {
        return carga;
    }

    public void setCarga(Carga carga) {
        this.carga = carga;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idNotaPendiente != null ? idNotaPendiente.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof NotaPendiente)) {
            return false;
        }
        NotaPendiente other = (NotaPendiente) object;
        if ((this.idNotaPendiente == null && other.idNotaPendiente != null) || (this.idNotaPendiente != null && !this.idNotaPendiente.equals(other.idNotaPendiente))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
//        return "cubisoft.carycob.entidades.NotaPendiente[ idNotaPendiente=" + idNotaPendiente + " ]";
        return "Nota a cliente Nº"+idNotaPendiente;
    }
    
}
