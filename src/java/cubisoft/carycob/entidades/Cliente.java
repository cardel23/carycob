/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cubisoft.carycob.entidades;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Estacion 2
 */
@Entity
@Table(name = "Cliente")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Cliente.findAll", query = "SELECT c FROM Cliente c"),
    @NamedQuery(name = "Cliente.findByIdCliente", query = "SELECT c FROM Cliente c WHERE c.idCliente = :idCliente"),
    @NamedQuery(name = "Cliente.findByNombre", query = "SELECT c FROM Cliente c WHERE c.nombre = :nombre"),
//    @NamedQuery(name = "Cliente.findByRazonSocial", query = "SELECT c FROM Cliente c WHERE c.razonSocial = :razonSocial"),
    @NamedQuery(name = "Cliente.findByCorreo", query = "SELECT c FROM Cliente c WHERE c.correo = :correo"),
    @NamedQuery(name = "Cliente.findMorosos", query = "SELECT e FROM Cliente e")})
public class Cliente implements Serializable {
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "cliente",orphanRemoval = true)
    private List<NotaPendiente> notaPendienteList;
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "idCliente", nullable = false, length = 10)
    private String idCliente;
    @Basic(optional = false)
    @NotNull
//    @Size(min = 1, max = 50)
    @Column(name = "nombre", nullable = false, unique = true)
    private String nombre;
//    @Size(max = 20)
//    @Column(name = "razonSocial", length = 20)
//    private String razonSocial;
    @Size(max = 50)
    @Column(name = "correo", length = 50)
    private String correo;
    @JoinTable(name = "ClasificacionCliente", joinColumns = {
        @JoinColumn(name = "cliente", referencedColumnName = "idCliente", nullable = false)}, inverseJoinColumns = {
        @JoinColumn(name = "subClasificacion", referencedColumnName = "idSubClasificacion", nullable = false)})
    @ManyToMany(fetch = FetchType.EAGER)
    private List<Subclasificacion> subclasificacionList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "cliente",orphanRemoval = true, fetch = FetchType.EAGER)
    private List<Factura> facturaList;

    public Cliente() {
    }

    public Cliente(String idCliente) {
        this.idCliente = idCliente;
    }

    public Cliente(String idCliente, String nombre) {
        this.idCliente = idCliente;
        this.nombre = nombre;
    }

    public String getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(String idCliente) {
        this.idCliente = idCliente;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

//    public String getRazonSocial() {
//        return razonSocial;
//    }
//
//    public void setRazonSocial(String razonSocial) {
//        this.razonSocial = razonSocial;
//    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    @XmlTransient
    public List<Subclasificacion> getSubclasificacionList() {
        return subclasificacionList;
    }

    public void setSubclasificacionList(List<Subclasificacion> subclasificacionList) {
        this.subclasificacionList = subclasificacionList;
    }

    @XmlTransient
    public List<Factura> getFacturaList() {
        return facturaList;
    }

    public void setFacturaList(List<Factura> facturaList) {
        this.facturaList = facturaList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idCliente != null ? idCliente.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Cliente)) {
            return false;
        }
        Cliente other = (Cliente) object;
        if ((this.idCliente == null && other.idCliente != null) || (this.idCliente != null && !this.idCliente.equals(other.idCliente))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
//        return "cubisoft.carycob.entidades.Cliente[ idCliente=" + idCliente + " ]";
        return nombre;
    }

    @XmlTransient
    public List<NotaPendiente> getNotaPendienteList() {
        return notaPendienteList;
    }

    public void setNotaPendienteList(List<NotaPendiente> notaPendienteList) {
        this.notaPendienteList = notaPendienteList;
    }
    
}
