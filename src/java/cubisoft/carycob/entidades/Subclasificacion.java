/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cubisoft.carycob.entidades;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Estacion 2
 */
@Entity
@Table(name = "Subclasificacion")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Subclasificacion.findAll", query = "SELECT s FROM Subclasificacion s"),
    @NamedQuery(name = "Subclasificacion.findByIdSubClasificacion", query = "SELECT s FROM Subclasificacion s WHERE s.idSubClasificacion = :idSubClasificacion"),
    @NamedQuery(name = "Subclasificacion.findByDescripcion", query = "SELECT s FROM Subclasificacion s WHERE s.descripcion = :descripcion")})
public class Subclasificacion implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 5)
    @Column(name = "idSubClasificacion", nullable = false, length = 5)
    private String idSubClasificacion;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 15)
    @Column(name = "descripcion", nullable = false, length = 15)
    private String descripcion;
    @ManyToMany(mappedBy = "subclasificacionList")
    private List<Cliente> clienteList;
    @JoinColumn(name = "clasificacion", referencedColumnName = "idClasificacion", nullable = false)
    @ManyToOne(optional = false)
    private Clasificacion clasificacion;

    public Subclasificacion() {
    }

    public Subclasificacion(String idSubClasificacion) {
        this.idSubClasificacion = idSubClasificacion;
    }

    public Subclasificacion(String idSubClasificacion, String descripcion) {
        this.idSubClasificacion = idSubClasificacion;
        this.descripcion = descripcion;
    }

    public String getIdSubClasificacion() {
        return idSubClasificacion;
    }

    public void setIdSubClasificacion(String idSubClasificacion) {
        this.idSubClasificacion = idSubClasificacion;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @XmlTransient
    public List<Cliente> getClienteList() {
        return clienteList;
    }

    public void setClienteList(List<Cliente> clienteList) {
        this.clienteList = clienteList;
    }

    public Clasificacion getClasificacion() {
        return clasificacion;
    }

    public void setClasificacion(Clasificacion clasificacion) {
        this.clasificacion = clasificacion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idSubClasificacion != null ? idSubClasificacion.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Subclasificacion)) {
            return false;
        }
        Subclasificacion other = (Subclasificacion) object;
        if ((this.idSubClasificacion == null && other.idSubClasificacion != null) || (this.idSubClasificacion != null && !this.idSubClasificacion.equals(other.idSubClasificacion))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
//        return "cubisoft.carycob.entidades.Subclasificacion[ idSubClasificacion=" + idSubClasificacion + " ]";
        return descripcion;
    }
    
}
