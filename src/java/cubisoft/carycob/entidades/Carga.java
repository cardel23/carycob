/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cubisoft.carycob.entidades;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Estacion 2
 */
@Entity
@Table(name = "Carga")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Carga.findAll", query = "SELECT c FROM Carga c"),
    @NamedQuery(name = "Carga.findByIdCarga", query = "SELECT c FROM Carga c WHERE c.idCarga = :idCarga"),
    @NamedQuery(name = "Carga.findByDescripcion", query = "SELECT c FROM Carga c WHERE c.descripcion = :descripcion"),
    @NamedQuery(name = "Carga.findByFecha", query = "SELECT c FROM Carga c WHERE c.fecha = :fecha")})
public class Carga implements Serializable {
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "carga")
    private List<NotaPendiente> notaPendienteList;
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "idCarga", nullable = false, length = 10)
    private String idCarga;
    @Size(max = 50)
    @Column(name = "descripcion", length = 50)
    private String descripcion;
    @Basic(optional = false)
    @NotNull
    @Column(name = "fecha", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecha;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "carga")
    private List<Pago> pagoList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "carga")
    private List<Rechazo> rechazoList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "carga")
    private List<Factura> facturaList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "carga")
    private List<Nota> notaList;
    @JoinColumn(name = "usuario", referencedColumnName = "idUsuario", nullable = false)
    @ManyToOne(optional = false)
    private Usuario usuario;
    
    public static String cargaMasivaFacturas = "Carga masiva de facturas y pagos";
    public static String cargaMasivaNotas = "Carga masiva de notas de debito y credito";
    public static String cargaSimpleFactura = "Carga simple de factura";
    public static String cargaSimplePago = "Carga simple de pago";
    public static String cargaSimpleNotaCredito = "Carga simple de nota de credito";
    public static String cargaSimpleNotaDebito = "Carga simple de nota de debito";

    public Carga() {
    }

    public Carga(String idCarga) {
        this.idCarga = idCarga;
    }

    public Carga(String idCarga, Date fecha) {
        this.idCarga = idCarga;
        this.fecha = fecha;
    }

    public String getIdCarga() {
        return idCarga;
    }

    public void setIdCarga(String idCarga) {
        this.idCarga = idCarga;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    @XmlTransient
    public List<Pago> getPagoList() {
        return pagoList;
    }

    public void setPagoList(List<Pago> pagoList) {
        this.pagoList = pagoList;
    }

    @XmlTransient
    public List<Rechazo> getRechazoList() {
        return rechazoList;
    }

    public void setRechazoList(List<Rechazo> rechazoList) {
        this.rechazoList = rechazoList;
    }

    @XmlTransient
    public List<Factura> getFacturaList() {
        return facturaList;
    }

    public void setFacturaList(List<Factura> facturaList) {
        this.facturaList = facturaList;
    }

    @XmlTransient
    public List<Nota> getNotaList() {
        return notaList;
    }

    public void setNotaList(List<Nota> notaList) {
        this.notaList = notaList;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idCarga != null ? idCarga.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Carga)) {
            return false;
        }
        Carga other = (Carga) object;
        if ((this.idCarga == null && other.idCarga != null) || (this.idCarga != null && !this.idCarga.equals(other.idCarga))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
//        return "cubisoft.carycob.entidades.Carga[ idCarga=" + idCarga + " ]";
        return idCarga;
    }

    @XmlTransient
    public List<NotaPendiente> getNotaPendienteList() {
        return notaPendienteList;
    }

    public void setNotaPendienteList(List<NotaPendiente> notaPendienteList) {
        this.notaPendienteList = notaPendienteList;
    }
    
}
