/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cubisoft.carycob.entidades;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Estacion 2
 */
@Entity
@Table(name = "Pago")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Pago.findAll", query = "SELECT p FROM Pago p"),
    @NamedQuery(name = "Pago.findByIdPago", query = "SELECT p FROM Pago p WHERE p.idPago = :idPago"),
    @NamedQuery(name = "Pago.findByMonto", query = "SELECT p FROM Pago p WHERE p.monto = :monto"),
    @NamedQuery(name = "Pago.findByFechaEmision", query = "SELECT p FROM Pago p WHERE p.fechaEmision = :fechaEmision"),
    @NamedQuery(name = "Pago.findByCarga", query = "SELECT p FROM Pago p WHERE p.carga = :carga")})
public class Pago implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "idPago", nullable = false, length = 10)
    private String idPago;
    @Basic(optional = false)
    @NotNull
    @Column(name = "monto", nullable = false)
    private double monto;
    @Basic(optional = false)
    @NotNull
    @Column(name = "fechaEmision", nullable = false)
    @Temporal(TemporalType.DATE)
    private Date fechaEmision;
    @JoinColumn(name = "factura", referencedColumnName = "idFactura", nullable = false)
    @ManyToOne(optional = false)
    private Factura factura;
    @JoinColumn(name = "carga", referencedColumnName = "idCarga", nullable = false)
    @ManyToOne(optional = false, cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    private Carga carga;

    public Pago() {
    }

    public Pago(String idPago) {
        this.idPago = idPago;
    }

    public Pago(String idPago, double monto, Date fechaEmision) {
        this.idPago = idPago;
        this.monto = monto;
        this.fechaEmision = fechaEmision;
    }

    public String getIdPago() {
        return idPago;
    }

    public void setIdPago(String idPago) {
        this.idPago = idPago;
    }

    public double getMonto() {
        return monto;
    }

    public void setMonto(double monto) {
        this.monto = monto;
    }

    public Date getFechaEmision() {
        return fechaEmision;
    }

    public void setFechaEmision(Date fechaEmision) {
        this.fechaEmision = fechaEmision;
    }

    public Factura getFactura() {
        return factura;
    }

    public void setFactura(Factura factura) {
        this.factura = factura;
    }

    public Carga getCarga() {
        return carga;
    }

    public void setCarga(Carga carga) {
        this.carga = carga;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idPago != null ? idPago.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Pago)) {
            return false;
        }
        Pago other = (Pago) object;
        if ((this.idPago == null && other.idPago != null) || (this.idPago != null && !this.idPago.equals(other.idPago))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
//        return "cubisoft.carycob.entidades.Pago[ idPago=" + idPago + " ]";
        return idPago;
    }
    
}
