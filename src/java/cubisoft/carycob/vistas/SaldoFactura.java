/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cubisoft.carycob.vistas;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Estacion 2
 */
@Entity
@Table(name = "SaldoFactura")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "SaldoFactura.findAll", query = "SELECT s FROM SaldoFactura s"),
    @NamedQuery(name = "SaldoFactura.findByFactura", query = "SELECT s FROM SaldoFactura s WHERE s.factura = :factura"),
    @NamedQuery(name = "SaldoFactura.findByMontoInicial", query = "SELECT s FROM SaldoFactura s WHERE s.montoInicial = :montoInicial"),
    @NamedQuery(name = "SaldoFactura.findByHaber", query = "SELECT s FROM SaldoFactura s WHERE s.haber = :haber"),
    @NamedQuery(name = "SaldoFactura.findBySaldo", query = "SELECT s FROM SaldoFactura s WHERE s.saldo = :saldo"),
    @NamedQuery(name = "SaldoFactura.findByVence", query = "SELECT s FROM SaldoFactura s WHERE s.vence = :vence"),
    @NamedQuery(name = "SaldoFactura.findByCliente", query = "SELECT s FROM SaldoFactura s WHERE s.cliente = :cliente"),
    @NamedQuery(name = "SaldoFactura.findSaldo", query = "SELECT s.saldo FROM SaldoFactura s WHERE s.factura = :factura"),
    @NamedQuery(name = "SaldoFactura.findImporte", query = "SELECT s.importeTotal FROM SaldoFactura s WHERE s.factura = :factura")})
public class SaldoFactura implements Serializable {
    @Size(max = 20)
    @Column(name = "vendedor")
    private String vendedor;
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "factura", nullable = false, length = 10)
    private String factura;
    @Basic(optional = false)
    @NotNull
    @Column(name = "montoInicial", nullable = false)
    private double montoInicial;
    @Column(name = "saldo", precision = 53)
    private Double saldo;
    @Basic(optional = false)
    @NotNull
    @Column(name = "vence", nullable = false)
    @Temporal(TemporalType.DATE)
    private Date vence;
    @Basic(optional = false)
    @NotNull
    @Column(name = "fechaEmision", nullable = false)
    @Temporal(TemporalType.DATE)
    private Date fechaEmision;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "importeTotal")
    private Double importeTotal;
    @Column(name = "haber")
    private Double haber;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "cliente")
    private String cliente;
    @Size(max = 10)
    @Column(name = "estado")
    private String estado;

    public SaldoFactura() {
    }

    public String getFactura() {
        return factura;
    }

    public void setFactura(String factura) {
        this.factura = factura;
    }

    public double getMontoInicial() {
        return montoInicial;
    }

    public void setMontoInicial(double montoInicial) {
        this.montoInicial = montoInicial;
    }

    public Double getSaldo() {
        return saldo;
    }

    public void setSaldo(Double saldo) {
        this.saldo = saldo;
    }

    public Date getVence() {
        return vence;
    }

    public void setVence(Date vence) {
        this.vence = vence;
    }

    public Double getImporteTotal() {
        return importeTotal;
    }

    public void setImporteTotal(Double importeTotal) {
        this.importeTotal = importeTotal;
    }

    public Double getHaber() {
        return haber;
    }

    public void setHaber(Double haber) {
        this.haber = haber;
    }

    public String getCliente() {
        return cliente;
    }

    public void setCliente(String cliente) {
        this.cliente = cliente;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getVendedor() {
        return vendedor;
    }

    public void setVendedor(String vendedor) {
        this.vendedor = vendedor;
    }

    public Date getFechaEmision() {
        return fechaEmision;
    }

    public void setFechaEmision(Date fechaEmision) {
        this.fechaEmision = fechaEmision;
    }
    
}
