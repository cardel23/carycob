/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cubisoft.carycob.vistas;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * 
 * @author Carlos Delgadillo
 */
@Entity
@Table(name = "VencimientoFacturas")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Vencimiento.findAll", query = "SELECT v FROM Vencimiento v")
    })
public class Vencimiento implements Serializable {
    
    private static final long serialVersionUID = 1L;
    @Id
    @Column(name = "diferenciaDias")
    private int diferenciaDias;
    @Column(name = "contador")
    private int contador;
    @Column(name = "dia")
    private int dia;
    @Column(name = "mes")
    private int mes;
    @Column(name = "vence")
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date vence;
    
    

    public Vencimiento() {
    }

    public int getDiferenciaDias() {
        return diferenciaDias;
    }

    public void setDiferenciaDias(int diferenciaDias) {
        this.diferenciaDias = diferenciaDias;
    }

    public int getContador() {
        return contador;
    }

    public void setContador(int contador) {
        this.contador = contador;
    }

    public int getDia() {
        return dia;
    }

    public void setDia(int dia) {
        this.dia = dia;
    }

    public int getMes() {
        return mes;
    }

    public void setMes(int mes) {
        this.mes = mes;
    }

    public Date getVence() {
        return vence;
    }

    public void setVence(Date vence) {
        this.vence = vence;
    }

    
    
}
