/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cubisoft.carycob.vistas;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Carlos Delgadillo
 */
@Entity
@Table(name = "VentasClasificacion")
@XmlRootElement
public class VentasClasificacion implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Column(name = "subclasificacion")
    private String subclasificacion;
    @Column(name = "clasificacion")
    private String clasificacion;
    @Column(name = "vendido", precision = 2)
    private Double vendido;
    @Column(name = "vencido", precision = 2)
    private Double vencido;
    @Column(name = "cobrado", precision = 2)
    private Double cobrado;

    public String getSubclasificacion() {
        return subclasificacion;
    }

    public void setSubclasificacion(String subclasificacion) {
        this.subclasificacion = subclasificacion;
    }

    public String getClasificacion() {
        return clasificacion;
    }

    public void setClasificacion(String clasificacion) {
        this.clasificacion = clasificacion;
    }

    public Double getVendido() {
        return vendido;
    }

    public void setVendido(Double vendido) {
        this.vendido = vendido;
    }

    public Double getVencido() {
        return vencido;
    }

    public void setVencido(Double vencido) {
        this.vencido = vencido;
    }

    public Double getCobrado() {
        return cobrado;
    }

    public void setCobrado(Double cobrado) {
        this.cobrado = cobrado;
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (subclasificacion != null ? subclasificacion.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the subclasificacion fields are not set
        if (!(object instanceof VentasClasificacion)) {
            return false;
        }
        VentasClasificacion other = (VentasClasificacion) object;
        if ((this.subclasificacion == null && other.subclasificacion != null) || (this.subclasificacion != null && !this.subclasificacion.equals(other.subclasificacion))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cubisoft.carycob.entidades.VentasClasificacion[ id=" + subclasificacion + " ]";
    }
    
}
