/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cubisoft.carycob.vistas;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Estacion 2
 */
@Entity
@Table(name = "CuentaCliente")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CuentaCliente.findAll", query = "SELECT c FROM CuentaCliente c"),
    @NamedQuery(name = "CuentaCliente.findByIdCliente", query = "SELECT c FROM CuentaCliente c WHERE c.idCliente = :idCliente"),
    @NamedQuery(name = "CuentaCliente.findByNombre", query = "SELECT c FROM CuentaCliente c WHERE c.nombre = :nombre"),
    @NamedQuery(name = "CuentaCliente.findByDebe", query = "SELECT c FROM CuentaCliente c WHERE c.debe = :debe"),
    @NamedQuery(name = "CuentaCliente.findByHaber", query = "SELECT c FROM CuentaCliente c WHERE c.haber = :haber"),
    @NamedQuery(name = "CuentaCliente.findByTotal", query = "SELECT c FROM CuentaCliente c WHERE c.total = :total")})
public class CuentaCliente implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "idCliente", nullable = false, length = 10)
    private String idCliente;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "nombre", nullable = false, length = 50)
    private String nombre;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "debe", precision = 53)
    private Double debe;
    @Column(name = "haber", precision = 53)
    private Double haber;
    @Column(name = "total", precision = 53)
    private Double total;
    @Column(name = "saldoVencido")
    private Double saldoVencido;
    public CuentaCliente() {
    }

    public String getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(String idCliente) {
        this.idCliente = idCliente;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Double getDebe() {
        return debe;
    }

    public void setDebe(Double debe) {
        this.debe = debe;
    }

    public Double getHaber() {
        return haber;
    }

    public void setHaber(Double haber) {
        this.haber = haber;
    }

    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }

    public Double getSaldoVencido() {
        return saldoVencido;
    }

    public void setSaldoVencido(Double saldoVencido) {
        this.saldoVencido = saldoVencido;
    }
    
}
