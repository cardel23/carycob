/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cubisoft.carycob.vistas;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Carlos Delgadillo
 */
@Entity
@Table(name = "VentasCliente")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "VentasCliente.findAll", query = "SELECT v FROM VentasCliente v")
})
public class VentasCliente implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Column(name = "vendedor")
    private String vendedor;
    @Column(name = "vendido", precision = 2)
    private Double vendido;
    @Column(name = "cobrado", precision = 2)
    private Double cobrado;
    @Column(name = "vencido", precision = 2)
    private Double vencido;
    

    public String getVendedor() {
        return vendedor;
    }

    public void setVendedor(String vendedor) {
        this.vendedor = vendedor;
    }

    public Double getVendido() {
        return vendido;
    }

    public void setVendido(Double vendido) {
        this.vendido = vendido;
    }

    public Double getCobrado() {
        return cobrado;
    }

    public void setCobrado(Double cobrado) {
        this.cobrado = cobrado;
    }

    public Double getVencido() {
        return vencido;
    }

    public void setVencido(Double vencido) {
        this.vencido = vencido;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (vendedor != null ? vendedor.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof VentasCliente)) {
            return false;
        }
        VentasCliente other = (VentasCliente) object;
        if ((this.vendedor == null && other.vendedor != null) || (this.vendedor != null && !this.vendedor.equals(other.vendedor))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cubisoft.carycob.entidades.VentasCliente[ id=" + vendedor + " ]";
    }
    
}
