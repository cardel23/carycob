/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cubisoft.carycob.vistas;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Estacion 2
 */
@Entity
@Table(name = "SumaPagos")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "SumaPagos.findAll", query = "SELECT s FROM SumaPagos s"),
    @NamedQuery(name = "SumaPagos.findByFactura", query = "SELECT s FROM SumaPagos s WHERE s.factura = :factura"),
    @NamedQuery(name = "SumaPagos.findByDebito", query = "SELECT s FROM SumaPagos s WHERE s.debito = :debito"),
    @NamedQuery(name = "SumaPagos.findByPagosRealizados", query = "SELECT s FROM SumaPagos s WHERE s.pagosRealizados = :pagosRealizados")})
public class SumaPagos implements Serializable {
//    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "cliente")
    private String cliente;
    @Basic(optional = false)
    @NotNull
    @Column(name = "monto")
    private double monto;
    @Basic(optional = false)
    @NotNull
    @Column(name = "fechaVencimiento")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaVencimiento;
    @Size(max = 10)
    @Column(name = "estado")
    private String estado;
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "factura", nullable = false, length = 10)
    private String factura;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "debito", precision = 53)
    private Double debito;
    @Column(name = "pagosRealizados")
    private Integer pagosRealizados;

    public SumaPagos() {
    }

    public String getFactura() {
        return factura;
    }

    public void setFactura(String factura) {
        this.factura = factura;
    }

    public Double getDebito() {
        return debito;
    }

    public void setDebito(Double debito) {
        this.debito = debito;
    }

    public Integer getPagosRealizados() {
        return pagosRealizados;
    }

    public void setPagosRealizados(Integer pagosRealizados) {
        this.pagosRealizados = pagosRealizados;
    }

    public String getCliente() {
        return cliente;
    }

    public void setCliente(String cliente) {
        this.cliente = cliente;
    }

    public double getMonto() {
        return monto;
    }

    public void setMonto(double monto) {
        this.monto = monto;
    }

    public Date getFechaVencimiento() {
        return fechaVencimiento;
    }

    public void setFechaVencimiento(Date fechaVencimiento) {
        this.fechaVencimiento = fechaVencimiento;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }
    
}
