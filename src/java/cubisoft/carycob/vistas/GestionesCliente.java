/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cubisoft.carycob.vistas;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Vista que muestra las gestiones relacionadas a un cliente
 * @author Carlos Delgadillo
 */
@Entity
@Table(name = "GestionesCliente")
@XmlRootElement
public class GestionesCliente implements Serializable {
    
    private static final long serialVersionUID = 1L;
    @Id
    @Column(name="idGestion")
    private String idGestion;
    @Column(name="idCliente")
    private String idCliente;
    @Column(name="nombre")
    private String nombre;
    @Column(name="idFactura")
    private String idFactura;
    @Column(name="usuario")
    private String usuario;
    @Column(name="estado")
    private String estado;
    @Column(name="observaciones")
    private String observaciones;
    @Column(name="fechaGestion")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaGestion;
    @Column(name="fechaLimiteGestion")
    @Temporal(TemporalType.DATE)
    private Date fechaLimiteGestion;

    public String getIdGestion() {
        return idGestion;
    }

    public void setIdGestion(String idGestion) {
        this.idGestion = idGestion;
    }

    public String getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(String idCliente) {
        this.idCliente = idCliente;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getIdFactura() {
        return idFactura;
    }

    public void setIdFactura(String idFactura) {
        this.idFactura = idFactura;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    public Date getFechaGestion() {
        return fechaGestion;
    }

    public void setFechaGestion(Date fechaGestion) {
        this.fechaGestion = fechaGestion;
    }

    public Date getFechaLimiteGestion() {
        return fechaLimiteGestion;
    }

    public void setFechaLimiteGestion(Date fechaLimiteGestion) {
        this.fechaLimiteGestion = fechaLimiteGestion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idGestion != null ? idGestion.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the idGestion fields are not set
        if (!(object instanceof GestionesCliente)) {
            return false;
        }
        GestionesCliente other = (GestionesCliente) object;
        if ((this.idGestion == null && other.idGestion != null) || (this.idGestion != null && !this.idGestion.equals(other.idGestion))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cubisoft.carycob.vistas.GestionesCliente[ idGestion=" + idGestion + " ]";
    }
    
}
