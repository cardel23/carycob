/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cubisoft.carycob.excepciones;

/**
 * Excepción que se genera cuando falla la construcción de un pago
 * @author Carlos Delgadillo
 * @since 18/03/2014
 */
public class PagoException extends Exception {
    
    public PagoException(){
        super();
    }
    
    public PagoException(String mensaje){
        super(mensaje);
    }
    
    public PagoException(String mensaje, Throwable causa){
        super(mensaje, causa);
    }
}
