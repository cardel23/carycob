/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cubisoft.carycob.excepciones;

/**
 * Excepcion lanzada cuando falla la construción de una factura
 * @author Carlos Delgadillo
 * @since 15/03/2014
 */
public class FacturaException extends Exception{
    
    public FacturaException(){
        super("El id de la factura no puede ser nulo");
    }
    
    public FacturaException(String mensaje){
        super(mensaje);
    }
    
    public FacturaException(String mensaje, Throwable causa){
        super(mensaje, causa);
    }
    
}
