/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cubisoft.carycob.excepciones;

/**
 * Se produce cuando no existe el cliente
 * @author Carlos Delgadillo
 * @since 13/03/2014
 */
public class NoClienteException extends Exception{
    
    public NoClienteException(){
    }
    
    public NoClienteException(String mensaje){
        super(mensaje);
    }
    
    public NoClienteException(String mensaje, Throwable causa){
        super(mensaje, causa);
    }
    
}
