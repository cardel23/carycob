/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cubisoft.carycob.excepciones;

/**
 * Excepciones producidas durante la lectura de archivos de excel
 * @author Carlos Delgadillo
 */
public class ProcesadorExcelException extends Exception {

    public static String NOELEMENTOS = "La lista no contiene elementos debido a errores de lectura. Revise el archivo de excel o suba uno nuevo";
    public static String PCT = "El 10% o más de los registros contienen errores. Revise el archivo de excel o suba uno nuevo";
    /**
     * Creates a new instance of
     * <code>ProcesadorExcelException</code> without detail message.
     */
    public ProcesadorExcelException() {
    }

    /**
     * Constructs an instance of
     * <code>ProcesadorExcelException</code> with the specified detail message.
     *
     * @param msg the detail message.
     */
    public ProcesadorExcelException(String msg) {
        super(msg);
    }
    
    public ProcesadorExcelException(String msg, Throwable causa){
        super(msg,causa);
    }
}
