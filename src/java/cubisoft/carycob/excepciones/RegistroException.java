/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cubisoft.carycob.excepciones;

/**
 * Se genera esta excepción cuando una factura ha sido rechazada anteriormente
 * @author Carlos Delgadillo
 * @since 03/05/2014
 */
public class RegistroException extends Exception {
    
    public RegistroException(){
        super("Un registro con el mismo id ha sido rechazado previamente");
    }
    
    public RegistroException(String mensaje){
        super(mensaje);
    }
    
    public RegistroException(Throwable causa){
        super(causa);
    }
    
    public RegistroException(String mensaje, Throwable causa){
        super(mensaje, causa);
    }
}
