/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cubisoft.carycob.seguridad;

import cubisoft.carycob.entidades.Usuario;
import javax.faces.application.NavigationHandler;
import javax.faces.context.FacesContext;
import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;
import javax.faces.event.PhaseListener;
import javax.servlet.http.HttpSession;

/**
 * Controla la sesion del usuario en el sistema
 * @author Estacion 2
 */
public class AuthorizationListener implements PhaseListener {

    /**
     * 
     * @param event
     */
    @Override
    public void afterPhase(PhaseEvent event) {

        FacesContext facesContext = event.getFacesContext();
        String currentPage = facesContext.getViewRoot().getViewId();

        boolean isLoginPage = (currentPage.lastIndexOf("/login.xhtml") > -1);
        HttpSession session = (HttpSession) facesContext.getExternalContext().getSession(false);
        NavigationHandler nh = facesContext.getApplication().getNavigationHandler(); 
        if(session==null){   
        nh.handleNavigation(facesContext, null, "/login.xhtml");
    }
        
    else{
        Usuario currentUser = (Usuario) session.getAttribute("currentUser");
        if (!isLoginPage && (currentUser == null)) {       
            nh.handleNavigation(facesContext, null, "/login.xhtml");            
        }
         
    }
}

    /**
     *
     * @param event
     */
    @Override
    public void beforePhase(PhaseEvent event) {

    }

    /**
     *
     * @return
     */
    @Override
    public PhaseId getPhaseId() {
        return PhaseId.RESTORE_VIEW;
    }
}
