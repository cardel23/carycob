/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cubisoft.carycob.emailUtils;

/**
 *
 * @author Darlon Espinoza Lopez
 */
public class EmailSenderConfig {
    private String host ;
    private int port;
    private boolean auth;
    private String username;
    private String password ;
    private static EmailSenderConfig instance=null;

    private EmailSenderConfig() {
    }

    public static  EmailSenderConfig getInstance(){
        if(instance==null){
            instance=new EmailSenderConfig();
            instance.host= "smtp.gmail.com";
            instance.port = 465;
            instance.username = "pruebacarycob@gmail.com";
            instance.password= "prueba12345";
            instance.auth = true;
        }
        return instance;     
    }

    
    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public boolean isAuth() {
        return auth;
    }

    public void setAuth(boolean auth) {
        this.auth = auth;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    
}
